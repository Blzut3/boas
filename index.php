<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

//Magic quotes can sometimes be turned off, since most servers leave this on,
//removing them would be more of a performance hit.  So I will simulate them if
//it is off.
$start_execution = microtime(true);
$start_memusage = memory_get_usage();
if(get_magic_quotes_gpc() == 0)
{
	foreach($_POST as &$var)
	{
		if(is_string($var))
			addslashes($var);
	}
}
define('bitowl', true);
define('STANDARD_PAGE_SIZE', 20);
//load essential system files.
require_once 'includes/session.php';
require_once 'includes/config.php';
require_once 'includes/language.php';
require_once 'includes/database.php';
require_once 'includes/template_engine.php';
require_once 'includes/cp_functions.php';
require_once 'includes/global_functions.php';
require_once 'includes/bbcode.php';
$config = new BitOwl_Config('config.php');
$template_engine = BitOwl_TemplateEngine::getStandardInstance(); //init template engine
$template_engine->variables['query'] = str_replace('&', '&amp;', $_SERVER['QUERY_STRING']);

require_once 'includes/forms.php';

if(!$config->isInstalled())
{
	session_start();
	require 'install.php';
	die();
}

define('FILES_DIR', $config->setting['system']['files']['dir'].'/');

$db = BitOwl_Database::createDatabaseObject($config->setting['system']['dbhost'], $config->setting['system']['dbuser'], $config->setting['system']['dbpass'], $config->setting['system']['dbbase']); //init database
require_once 'dbupgrade.php';
//check/do login.
$session = BitOwl_Session::getSession();
$this_user = $session->getUser();
$template_engine->variables['loggingin'] = 'false';
if(isset($_POST['login_username']) && isset($_POST['login_password']))
{
	$template_engine->variables['loggingin'] = 'true';
	$this_user = $session->login($_POST['login_username'], $_POST['login_password'], isset($_POST['autologin']));
}
if($this_user === false || isset($_GET['logout']))
{
	if(isset($_GET['logout']))
	{
		$session->end();
	}
	if(isset($config->setting['system']['firstlogin']))
	{
		$template_engine->variables['loggingin'] = 'first';
		unset($config->setting['system']['firstlogin']);
		$config->save();
	}
	if($template_engine->variables['loggingin'] == 'true')
	{
		$template_engine->variables['loggingin'] = 'failed';
	}
	$template_engine->template('templates/cp/login.html');
}
else
{
	if(strpos($this_user['permissions'], 'perm_system') !== false)
	{
		$template_engine->variables['cps'][] = array('name' => language('CP_SYSTEM'), 'link' => 'system');
	}
	$template_engine->variables['cps'][] = array('name' => language('CP_USERS'), 'link' => 'users');
	$permission_list = array('perm_founder', 'perm_system', 'perm_users');
	$dir = opendir('./'); //retrieve a list of all cps
	for($i = 2;($file = readdir($dir)) !== false;$i++)
	{
		if($file != '..' && $file != '.' && $file != 'system' && $file != 'users')
		{
			if(!is_file($file) && file_exists($file.'/main.php') && file_exists($file.'/menu.php'))
			{
				$permission_list[] = 'perm_'.$file;
				// Now we need to init each module.  This is mainly for specifying extra permission sets.
				if(file_exists($file.'/init.php'))
				{
					if(isset($extra_permissions))
						unset($extra_permissions);
					include $file.'/init.php';
					if(is_array($extra_permissions))
					{
						foreach($extra_permissions as $permission)
						{
							// Possible improvement here: put the extra
							// permissions into an extra array which would
							// allow the list to be indented and remove any
							// possibility of overlap.
							$permission_list[] = $permission;
						}
					}
				}
				if(strpos($this_user['permissions'], 'perm_founder') === false && strpos($this_user['permissions'], 'perm_'.$file) === false)
				{
					continue;
				}
				$template_engine->variables['cps'][$i] = array('name' => language('CP_'.$file), 'link' => $file);
				if(strpos(strtolower($template_engine->variables['cps'][$i]['name']), 'cp_') === 0) //not in language?
				{
					$template_engine->variables['cps'][$i]['name'] = ucwords(substr($template_engine->variables['cps'][$i]['name'], 3));
				}
			}
		}
	}
	closedir($dir);
	unset($dir, $file);
	readpermissions($this_user, $permission_list); //expand permission list on user.
	$template_engine->variables['loggedinuser'] = $this_user;
	$cp = isset($_GET['cp']) && file_exists("./{$_GET['cp']}/") ? $_GET['cp'] : 'users';
	if(isset($_GET['func']) && $cp != $_GET['cp']) //we just had an invailid cp why check the func?
		$func = 'main';
	else
		$func = isset($_GET['func']) && file_exists("./$cp/{$_GET['func']}.php") ? $_GET['func'] : 'main';
	// Check the validity of these parameters.
	if(strpos($cp, '.') !== false ||
		strpos($func, '.') !== false ||
		strpos($cp, 'files') !== false)
		die();
	$template_engine->variables['cp'] = $cp;
	$template_engine->variables['func'] = $func;
	foreach($template_engine->variables['cps'] as $key => $tab)
	{
		if($tab == $cp)
		{
			$template_engine->variables['activetab'] = $key;
		}
	}
	include "./$cp/menu.php"; //should be a list of functions
	$functions = getFunctions();

	// Check for updates to the plugin
	if(function_exists('updatePlugin') &&
		(!isset($config->setting['version'][$cp]) ||
		version_compare(PLUGIN_VERSION, $config->setting['version'][$cp], '>'))
	)
	{
		// Switch to the config page, but only if one exists.
		foreach($functions as &$function)
		{
			if($function['link'] == 'config')
			{
				$template_engine->variables['func'] = 'config';
				$func = 'config';
			}
		}
		updatePlugin(!isset($config->setting['version'][$cp]) ? NULL : $config->setting['version'][$cp]);
	}

	$template_engine->variables['functions'] = $functions;
	$template_engine->template('templates/cp/header.html');
	include "./$cp/$func.php";
	$template_engine->variables['executiontime'] = round((microtime(true) - $start_execution)*1000, 2);
	$template_engine->variables['memoryusage'] = round((memory_get_peak_usage() - $start_memusage)/1024, 2);
	$template_engine->template('templates/cp/footer.html');
}
?>
