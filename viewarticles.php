<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl')) return; //do not continue if we did not init.
if(!defined('VIEWARTICLES'))
{
	define('VIEWARTICLES', true);

	// We'll store the working article here so we can fetch the attachments.
	$_bitowl['working_article'] = NULL;
	$_bitowl['articles_attachment_func'] = 'viewArticlesStandardAttachmentFunc';

	// Change $_bitowl['article_attachment_func'] to point to a custom function.
	function /* str */ viewArticlesStandardAttachmentFunc($attachment, $description, $number)
	{
		global $_bitowl;

		// PNG, GIF, or JPG should be displayed rather than linked.
		if($attachment['isimage'])
		{
			return '<img src="'.$_bitowl['config']->setting['system']['files']['url'].'/download.php?file='.$attachment['thumbnail'].'" alt="" title="'.$description.'" />';
		}
		return '<a href="'.$_bitowl['config']->setting['system']['files']['url'].'/download.php?file='.$attachment['file'].'">'.$description.'</a>';
	}

	function viewArticlesBBCodeAttachment($number, $innerValue)
	{
		global $_bitowl;
		if(!is_array($_bitowl['working_article']))
			return '';
		$attachments = unserialize($_bitowl['working_article']['attachments']);
		if($attachments[0] == '' || count($attachments) < $number)
			return '';

		return call_user_func($_bitowl['articles_attachment_func'], $attachments[$number], $innerValue, $number);
	}

	function displayArticle($article, $showComments=false)
	{
		global $_bitowl;

		$_bitowl['working_article'] = &$article;
		$article['recent'] = time()-$article['date'] < $_bitowl['config']->setting['journalist']['recenttime'];
		$article['date'] = date($_bitowl['config']->setting['system']['dateformat'], $article['date']);
		$article['story'] = parseBBCode($article['story'], 'viewArticlesBBCodeAttachment');
		if(!empty($article['thumbnail']))
			$article['thumbnail'] = $_bitowl['config']->setting['system']['files']['url'].'/download.php?file='.$article['thumbnail'];
		$_bitowl['templates']->variables['author'] = false;
		foreach($_bitowl['users'] as $user)
		{
			if($user['username'] == $article['author'])
			{
				if(!empty($user['realname']))
					$article['author'] = $user['realname'];
				$_bitowl['templates']->variables['author'] = $user;
				break;
			}
		}
		if($_bitowl['config']->setting['journalist']['allowcomments'])
		{
			$comments = new BitOwl_Comments($article['comments'], !$showComments);
			if($article['comments'] != $comments->getThreadId()) // New thread id
			{
				$_bitowl['database']->query('articles', BITOWL_DB_UPDATE, BITOWL_DB_ROW, array('id' => $article['id'], 'comments' => $comments->getThreadId()));
			}
			$article['comments'] = $comments->getCount();
		}
		else
			$article['comments'] = 0;
		$_bitowl['templates']->variables['article'] = $article;

		showTemplate('journalist_article.html');

		if($showComments && $_bitowl['config']->setting['journalist']['allowcomments'])
			$comments->display('comment.html');
	}

	function showArticleAttachment($id, $attachment)
	{
		global $_bitowl;

		$article = $_bitowl['database']->query('articles', BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('attachments'), BITOWL_DB_WHERE, array('id', $id), BITOWL_DB_WHERE, array('published', 1), BITOWL_DB_LIMIT, array(0,1));
		if(isset($article[0]))
		{
			$attachments = unserialize($article[0]['attachments']);
			if(isset($attachments[$attachment]))
				echo '<img alt="" style="display: block;margin: 0px auto 0px auto;width: '.$attachments[$attachment]['width'].'px" src="'.$_bitowl['config']->setting['system']['files']['url'].'/download.php?file='.$attachments[$attachment]['file'].'" />';
		}
	}

	function showArticles($category=-1)
	{
		global $_bitowl;

		$paginate = $_bitowl['config']->setting['journalist']['articlesperpage'] > 0;
		if($paginate)
		{
			$pagination = new BitOwl_Pagination($_bitowl['config']->setting['journalist']['articlesperpage']);

			$articles = $_bitowl['database']->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('category', $category, 'operator' => BITOWL_DB_WHERE_BITWISEAND), BITOWL_DB_WHERE, array('published', 1), BITOWL_DB_LIMIT, array($pagination->start(), $_bitowl['config']->setting['journalist']['articlesperpage']), BITOWL_DB_RCHRONOLOGICAL, BITOWL_DB_PAGINATED);
			$pagination->setTotal($_bitowl['database']->getTotalResults());
		}
		else
			$articles = $_bitowl['database']->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('category', $category, 'operator' => BITOWL_DB_WHERE_BITWISEAND), BITOWL_DB_WHERE, array('published', 1), BITOWL_DB_RCHRONOLOGICAL);

		if(count($articles) > 0)
		{
			foreach($articles as $article)
			{
				displayArticle($article);
			}
		}
		if($paginate)
			$pagination->show();
	}

	function showSpecificArticle($id)
	{
		global $_bitowl;

		$article = getSpecificArticleData($id);
		if($article != NULL)
			displayArticle($article, true);
	}

	function getSpecificArticleData($id)
	{
		global $_bitowl;

		if($_bitowl['working_article'] == NULL || $_bitowl['working_article']['id'] == $id)
		{
			$article = $_bitowl['database']->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $id), BITOWL_DB_WHERE, array('published', 1), BITOWL_DB_LIMIT, array(0,1));
			if(isset($article[0]))
				$_bitowl['working_article'] = &$article[0];
			else
				return NULL;
		}
		return $_bitowl['working_article'];
	}
}
?>
