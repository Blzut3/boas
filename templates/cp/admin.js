//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

// Automatically allow tables to be sorted.
var tables = document.getElementsByTagName("table");
var lastSorted = new Array(tables.length);

function sortTable(table, cell)
{
	// Initialize data.
	var thisTable = tables[table];
	var tdata = new Array();

	// Grab the rows from the table, we can't delete the rows yet since some
	// browsers do not like that. (IE6)
	while(thisTable.rows.length-1 > tdata.length)
		tdata.push(thisTable.rows[tdata.length+1]);

	// Sort the table based on the cell.
	function sortTableRow(a, b)
	{
		if(a.cells[cell].innerHTML < b.cells[cell].innerHTML)
			return 1;
		else if(a.cells[cell].innerHTML > b.cells[cell].innerHTML)
			return -1;
		return 0;
	}
	tdata.sort(sortTableRow);
	// If this is the column we last sorted reverse the results and unset the
	// lastSorted.
	if(lastSorted[table] == cell)
	{
		tdata.reverse();
		lastSorted[table] = -1;
	}
	else // Otherwise save the lastSorted
		lastSorted[table] = cell;

	// Insert the rows that we have sorted.
	var rowsToDelete = tdata.length;
	while(tdata.length > 0)
	{
		var thisRow = tdata.pop();
		var newRow = thisTable.insertRow(-1);
		for(var i = 0;i < thisRow.cells.length;i++)
			newRow.insertCell(-1).innerHTML = thisRow.cells[i].innerHTML;
	}

	// Dete the old rows from the table.
	while(rowsToDelete-- > 0)
		thisTable.deleteRow(1);
}

// Produces a function which calls sortTable.
function createSortTableEvent(table, cell)
{
	function makeFunction(evt)
	{
		sortTable(table, cell);
	}
	return makeFunction;
}

// Enable the sorting
for(var i = 0;i < tables.length;i++)
{
	lastSorted[i] = -1;
	if(tables[i].className == "editor")
	{
		var headerRow = tables[i].rows[0];
		for(var j = 0;j < headerRow.cells.length;j++)
		{
			var thisCell = headerRow.cells[j];
			thisCell.onclick = createSortTableEvent(i, j);
			thisCell.style.cursor = "pointer";
		}
	}
}
