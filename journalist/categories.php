<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined("bitowl") || $this_user['perm_journalist'] !== true || $this_user['perm_journalist_admin'] !== true)
{
	die();
}

function nextIdAvailable($categories)
{
	$nextid = 1;
	if(is_array($categories))
	{
		for($i = 0;$i < count($categories);$i++)
		{
			if($categories[$i]['id'] == $nextid)
			{
				$nextid++;
				$i = -1; // $i will be incremented.
			}
		}
	}
	return $nextid;
}

if(isset($_POST['delete'])) // Delete before table is updated.
{
	$db->query('categories', BITOWL_DB_DELETE, BITOWL_DB_WHERE, array('id', $_POST['delete']));
}

$categories = $db->query('categories', BITOWL_DB_SELECT);
$template_engine->variables['editing'] = false;

if(isset($_POST['category']))
{
	// Due to how loose categories are, I'm not too concerned about invalid user input.
	if(isset($_POST['permissions']))
	{
		for($i = 0;$i < count($_POST['permissions']);$i++)
		{
			if(empty($_POST['permissions'][$i]))
				unset($_POST['permissions'][$i]);
		}
	}
	else
		$_POST['permissions'] = '';

	$updated_category = array('id' => $_POST['category'], 'name' => $_POST['category_name'], 'users' => is_array($_POST['permissions']) ? implode(',', $_POST['permissions']) : '');
	$db->query('categories', BITOWL_DB_UPDATE, BITOWL_DB_ROW, $updated_category);

	$template_engine->variables['destination'] = '?cp=journalist&amp;func=categories';
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->variables['message'] = language('M_CATEGORYEDITED');
	$template_engine->template('templates/cp/message_confirm.html');
}
elseif(isset($_GET['delete']))
{
	$delete = NULL;
	foreach($categories as $category)
	{
		if($category['id'] == $_GET['delete'])
			$delete = $category;
	}
	if($delete == NULL)
		die();

	$template_engine->variables['destination'] = '?cp=journalist&amp;func=categories';
	$template_engine->variables['post_fields'] = array(array('name' => 'delete', 'value' => $_GET['delete']));
	$template_engine->variables['message_title'] = language('DELETECATEGORY');
	$template_engine->variables['message'] = language('C_DELETECATEGORY', $delete['name']);
	$template_engine->template('templates/cp/message_confirm.html');
}
else
{
	if(isset($_POST['category_name']) || isset($_GET['category']))
	{
		if(isset($_POST['category_name']))
		{
			$_GET['category'] = nextIdAvailable($categories);
			$categories[] = $category = array('id' => $_GET['category'], 'name' => $_POST['category_name']);
			$db->query('categories', BITOWL_DB_INSERT, BITOWL_DB_ROW, $category);
		}

		$editing = NULL;
		foreach($categories as $category)
		{
			if($category['id'] == $_GET['category'])
			{
				//users.(username) == true
				$category['users'] = array_flip(explode(',', $category['users']));
				if(count($category['users'] > 0))
				{
					foreach($category['users'] as $index => $value)
						$category['users'][$index] = true;
				}
				$editing = $category;
			}
		}

		// Only add users with the journalist permission to the list.  Also the founder
		// should not be on the list.
		$user_list = array(array('id' => '', 'name' => '------------'));
		$users = $db->query('users', BITOWL_DB_SELECT);
		foreach($users as &$user)
		{
			readpermissions($user, array('perm_founder', 'perm_journalist', 'perm_journalist_admin'));
			if($user['perm_founder'] || $user['perm_journalist_admin'] || !$user['perm_journalist'])
				continue;

			$user_list[] = array('id' => $user['username'], 'name' => $user['username']);
		}

		$form = new BitOwl_Form(BitOwl_Form::BTN_EDIT|BitOwl_Form::BTN_RESET);

		$group = $form->newGroup(language('EDITCATEGORY'));
		$group->newWidget(BitOwl_FormWidget::HIDDEN, NULL, 'category', $editing['id']);
		$group->newWidget(BitOwl_FormWidget::TEXT, language('NAME'), 'category_name', $editing['name']);
		$group->newWidget(BitOwl_FormWidget::SELECTION, language('USERS'), 'permissions[]', $editing['users'], $user_list);

		$form->printForm();
	}
	else
	{
		$categoriesTable = array();
		foreach($categories as $category)
		{
			$categoriesTable[] = array(
				$category['id'],
				$category['name'],
				'<a href="?cp=journalist&amp;func=categories&amp;category='.$category['id'].'">'.language('EDIT').'</a> <a href="?cp=journalist&amp;func=categories&amp;delete='.$category['id'].'">'.language('DELETE').'</a>'
			);
		}
	
		$form = new BitOwl_Form(BitOwl_Form::BTN_SUBMIT);
	
		$group = $form->newGroup(language('MANAGECATEGORIES'));
		$group->newTable(array(language('ID'), language('NAME'), language('ACTIONS')), $categoriesTable);
	
		$group = $form->newGroup(language('NEWCATEGORY'));
		$group->newWidget(BitOwl_FormWidget::TEXT, language('NAME'), 'category_name');
	
		$form->printForm();
	}
}
?>