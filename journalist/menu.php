<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

define('PLUGIN_VERSION', '3.0.1');
define('MAX_CATEGORIES', 32);

function updatePlugin($oldversion)
{
	global $config, $db;

	if($oldversion == NULL)
	{
		$db->createTable('categories', 0,
			array('name' => 'name', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'users', 'type' => BITOWL_DB_CREATETABLE_STRING)
		);
		$db->createTable('articles', BITOWL_DB_CREATETABLE_FLIPPED,
			array('name' => 'title', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'author', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'date', 'type' => BITOWL_DB_CREATETABLE_INT),
			array('name' => 'published', 'type' => BITOWL_DB_CREATETABLE_BOOL),
			array('name' => 'category', 'type' => BITOWL_DB_CREATETABLE_INT),
			array('name' => 'shortstory', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'story', 'type' => BITOWL_DB_CREATETABLE_TEXT),
			array('name' => 'attachments', 'type' => BITOWL_DB_CREATETABLE_TEXT),
			array('name' => 'thumbnail', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'comments', 'type' => BITOWL_DB_CREATETABLE_INT)
		);
		$db->createTable('feeds', 0,
			array('name' => 'title', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'link', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'description', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'file', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'categories', 'type' => BITOWL_DB_CREATETABLE_INT)
		);

		$db->query('categories', BITOWL_DB_INSERT, BITOWL_DB_ROW, array('id' => 0, 'name' => 'General'));
	}
	else if($oldversion == '3.0.0')
	{
		// Try to work around bug where comments column was not created on normal installs.
		$result = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_LIMIT, array(0, 1));
		if(count($result) == 0 || !isset($result[0]['comments']))
			$db->modifyTable('articles', BITOWL_DB_MODIFYTABLE_INSERT, 'comments', 'thumbnail', BITOWL_DB_CREATETABLE_INT, -1);
	}

	$config->setting['version']['journalist'] = PLUGIN_VERSION;
	$config->settingDefault('journalist.allowcomments', false);
	$config->settingDefault('journalist.articlesperpage', 5);
	$config->settingDefault('journalist.thumbnailwidth', 320);
	$config->settingDefault('journalist.thumbnailheight', 200);
	$config->settingDefault('journalist.recenttime', 86400);
	$config->save();
}


function getFunctions()
{
	global $this_user;

	$functions = array(
		array('name' => language('F_MAIN'), 'link' => 'main'),
		array('name' => language('F_COMPOSE'), 'link' => 'compose'),
		array('name' => language('F_MANAGEARTICLES'), 'link' => 'manage')
	);
	
	if($this_user['perm_journalist_admin'])
	{
		$functions[] = array('name' => language('F_CATEGORIES'), 'link' => 'categories');
		$functions[] = array('name' => language('F_CONFIGURE'), 'link' => 'config');
		$functions[] = array('name' => language('F_FEEDS'), 'link' => 'feeds');
		$functions[] = array('name' => language('F_PURGE'), 'link' => 'purge');
	}

	return $functions;
}

// Returns true if the article in $article belongs to $user
function checkArticleOwnership($article, $user)
{
	if($user['perm_journalist_admin'])
		return true;
	if(($article['author'] == $user['username']) ||
		($user['perm_editor'] === true && ($article['category'] & $user['category_permissions'])))
		return true;
	return false;
}

// Returns usable categories or true if all are available.
function getAvailableCategories($categories)
{
	global $this_user;

	$this_user['category_permissions'] = 1;
	if($this_user['perm_journalist_admin'] === false)
	{
		for($i = count($categories)-1;$i >= 0;$i--)
		{
			$categories[$i]['users'] = explode(',', $categories[$i]['users']);
			$available = false;
			foreach($categories[$i]['users'] as $user)
			{
				if($user == $this_user['username'])
					$available = true;
			}
			if($available)
				$this_user['category_permissions'] |= 1<<$categories[$i]['id'];
			else
				unset($categories[$i]);
		}
	}
	else
		$this_user['category_permissions'] = ~0;
	return $categories;
}

class RSSFeed
{
	private $feed;
	private static $categories = NULL;

	public static function setCategoriesTable($categories)
	{
		self::$categories = $categories;
	}

	protected static function entitiesRecode($string)
	{
		return str_replace(array('<', '>', '&'), array('&#x3C;', '&#x3E;', '&#x26;'), html_entity_decode($string, ENT_COMPAT, 'UTF-8'));
	}

	public function __construct($feed)
	{
		global $db;
		$this->feed = $feed;
		if(self::$categories === NULL)
			self::$categories = $db->query('categories', BITOWL_DB_SELECT);
	}

	public function generate()
	{
		global $db;

		$baseurl = substr($this->feed['link'], 0, strpos($this->feed['link'], '?'));
		if(empty($baseurl))
		{
			$lastToken = strpos($this->feed['link'], '{');
			$lastSlash = strrpos($this->feed['link'], '/', $lastToken-strlen($this->feed['link']));
			if($lasToken === false || $lastSlash === false)
				$baseurl = $this->feed['link'];
			else    
				$baseurl = substr($this->feed['link'], 0, $lastSlash);
		}

		$out = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<rss version=\"2.0\">\n\t<channel>\n\t\t<title>{$this->feed['title']}</title>\n\t\t<link>$baseurl</link>\n\t\t<description>{$this->feed['description']}</description>\n\t\t<generator>BitOwl Application Suit 2.0</generator>\n";
		$articles = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('published', true), BITOWL_DB_WHERE, array('category', $this->feed['categories'], 'operator' => BITOWL_DB_WHERE_BITWISEAND), BITOWL_DB_LIMIT, array(0, 10), BITOWL_DB_RCHRONOLOGICAL);
		foreach($articles as $article)
		{
			// make the URL
			$url = $this->entitiesRecode(preg_replace(array('/\\{article\\}/', '/\\{category\\}/'), array($article['id'], $article['category']), $this->feed['link']));

			$first_category = false;
			$out .= "\t\t<item>\n\t\t\t<title>".$this->entitiesRecode($article['title'])."</title>\n\t\t\t<description>".$this->entitiesRecode($article['shortstory'])."</description>\n\t\t\t<pubDate>".date('r', $article['date'])."</pubDate>\n\t\t\t<guid isPermaLink=\"false\">tag:$baseurl,{$article['id']}</guid>\n";
			for($i = 1;$i < 32;$i++)
			{
				if($article['category'] & (1<<$i))
				{
					if($first_category === false)
						$first_category = self::$categories[convertID(self::$categories, $i)]['name'];
					$out .= "\t\t\t<category>".self::$categories[convertID(self::$categories, $i)]['name']."</category>\n";
				}
			}

			// Avoid looping twice so parse more variables here.
			if($first_category === false)
				$first_category = 'unknown';
			$url = $this->entitiesRecode(preg_replace('/\\{category-name\\}/', $first_category, $url));
			$out .= "\t\t\t<link>$url</link>\n";

			$out .= "\t\t</item>\n";
		}
		$out .= "\t</channel>\n</rss>\n";
		file_put_contents($this->feed['file'], $out);
	}
}
?>
