<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if((!defined("bitowl") || $this_user['perm_journalist'] != true))
{
	die();
}

$displayTable = true;
if(isset($_GET['delete']) || isset($_POST['delete']))
{
	$article = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', isset($_GET['delete']) ? $_GET['delete'] : $_POST['delete']));
	if(checkArticleOwnership($article[0], $this_user))
	{
		if(isset($_GET['delete']))
		{
			$displayTable = false;
			$template_engine->variables['message_title'] = language('DELETEARTICLE');
			$template_engine->variables['message'] = language('C_DELETEARTICLE', $article[0]['title']);
			$template_engine->variables['post_fields'] = array(
				array('name' => 'delete', 'value' => $_GET['delete'])
			);
			$template_engine->variables['destination'] = '?cp=journalist&amp;func=manage';
			$template_engine->template('templates/cp/message_confirm.html');
		}
		else
		{
			$db->query('articles', BITOWL_DB_DELETE, BITOWL_DB_ROW, $article[0]);
		}
	}
}
elseif(isset($_GET['comments']))
{
	$article = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', (int) $_GET['comments']));
	if(is_array($article))
	{
		$displayTable = false;
		manageComments($article[0]['comments']);
	}
}

if($displayTable)
{
	$pagination = new BitOwl_Pagination(STANDARD_PAGE_SIZE);

	$categories = getAvailableCategories($db->query('categories', BITOWL_DB_SELECT));
	if(!$this_user['perm_editor'])
		$articles = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('author', $this_user['username']), BITOWL_DB_RCHRONOLOGICAL, BITOWL_DB_LIMIT, array($pagination->start(), STANDARD_PAGE_SIZE), BITOWL_DB_PAGINATED);
	elseif($this_user['perm_editor'])
		$articles = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('author', $this_user['username']), BITOWL_DB_WHERE, array('category', $this_user['category_permissions']&(~1), 'operator' => BITOWL_DB_WHERE_BITWISEAND, 'multioperator' => BITOWL_DB_WHERE_OR), BITOWL_DB_RCHRONOLOGICAL, BITOWL_DB_LIMIT, array($pagination->start(), STANDARD_PAGE_SIZE), BITOWL_DB_PAGINATED);
	else
		$articles = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_RCHRONOLOGICAL, BITOWL_DB_LIMIT, array($pagination->start(), STANDARD_PAGE_SIZE), BITOWL_DB_PAGINATED);

	$pagination->setTotal($db->getTotalResults());

	$tableData = array();
	foreach($articles as $article)
	{
		$tableData[] = array(
			$article['title'],
			$article['author'],
			date($config->setting['system']['dateformat'], $article['date']),
			'<a href="?cp=journalist&amp;func=compose&amp;edit='.$article['id'].'">'.language('EDIT').'</a> <a href="?cp=journalist&amp;func=manage&amp;comments='.$article['id'].'">'.language('COMMENTS').'</a> <a href="?cp=journalist&amp;func=manage&amp;delete='.$article['id'].'">'.language('DELETE').'</a>'
		);
	}

	$form = new BitOwl_Form(0);

	$group = $form->newGroup(language('F_MANAGEARTICLES'));
	$group->newTable(array(language('TITLE'), language('AUTHOR'), language('DATE'), language('ACTIONS')), $tableData);
	$group->newWidget(BitOwl_FormWidget::PAGINATION, NULL, NULL, $pagination);
	$group->newWidget(BitOwl_FormWidget::LABEL, '<a href="?cp=journalist&amp;func=compose">'.language('F_COMPOSE').'</a>');

	$form->printForm();
}
?>
