<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_journalist'])
{
	die();
}

$available_categories = getAvailableCategories($db->query('categories', BITOWL_DB_SELECT));
$attachments = array();

function makeCleanArticle()
{
	global $this_user;

	return array(
		'title' => '',
		'author' => $this_user['username'],
		'date' => time(),
		'shortstory' => '',
		'story' => '',
		'published' => $this_user['perm_editor'] ? 1 : 0,
		'category' => 1,
		'attachments' => '',
		'thumbnail' => '',
		'comments' => -1
	);
}

function makeArticleWithPostData()
{
	global $this_user;

	$article = makeCleanArticle();
	$article['title'] = entities_encode(stripslashes($_POST['title']));
	$article['story'] = entities_encode(stripslashes($_POST['story']));
	$article['published'] = $this_user['perm_editor'] ? isset($_POST['publish']) : 0;
	$article['attachments'] = isset($_POST['attachments']) ? stripslashes($_POST['attachments']) : '';

	$article['shortstory'] = calculateShortStory($article['story']);

	// Build the categories.
	if(isset($_POST['categories']) && is_array($_POST['categories']))
	{
		foreach($_POST['categories'] as &$category)
		{
			if($this_user['category_permissions'] & (1<<$category))
				$article['category'] |= 1<<$category;
		}
	}

	return $article;
}

function checkForAffectedFeeds($category)
{
	global $db;
	$feeds = $db->query('feeds', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('categories', $category, 'operator' => BITOWL_DB_WHERE_BITWISEAND));
	foreach($feeds as $feed)
	{
		$rss = new RSSFeed($feed);
		$rss->generate();
	}
}

// Creates the proper array for a file
function identifyAttachment($file)
{
	global $this_user;

	$img = getimagesize(FILES_DIR.$file);
	$attachment = array('file' => $file, 'isimage' => $img !== false, 'uploader' => $this_user['username']);
	if($attachment['isimage'])
	{
		$attachment['width'] = $img[0];
		$attachment['height'] = $img[1];
		$attachment['thumbnail'] = '';
		$attachment['thumbwidth'] = 0;
		$attachment['thumbheight'] = 0;
	}
	return $attachment;
}

if(isset($_POST['submit']) || isset($_POST['edit']))
{
	$article = makeArticleWithPostData();

	// Generate the thumbnails for attachments which do not have one
	$article['attachments'] = unserialize($article['attachments']);
	foreach($article['attachments'] as &$attachment)
	{
		if($attachment['isimage'] && empty($attachment['thumbnail']))
		{
			$attachment['thumbnail'] = generateImageThumbnail(FILES_DIR.$attachment['file'], $config->setting['journalist']['thumbnailwidth'], $config->setting['journalist']['thumbnailheight']);
			$attachment['thumbwidth'] = $config->setting['journalist']['thumbnailwidth'];
			$attachment['thumbheight'] = $config->setting['journalist']['thumbnailheight'];
		}
	}
	$article['attachments'] = serialize($article['attachments']);

	$template_engine->variables['post_fields'] = array(
		array('name' => 'continue', 'value' => '0')
	);
	$template_engine->variables['destination'] = '?cp=journalist&amp;func=manage';
	$template_engine->variables['message_title'] = language('MESSAGE');
	if(isset($_POST['editing']))
	{
		$articles = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $_POST['editing']));
		if(($articles[0]['id'] == $_POST['editing'] && $articles[0]['author']) && checkArticleOwnership($articles[0], $this_user))
		{
			$article['id'] = $articles[0]['id'];
			$article['date'] = $articles[0]['date'];
			$article['author'] = $articles[0]['author'];
			$article['comments'] = $articles[0]['comments'];
			// -- Actually on a second thought, edits should revert the article to unpublished for non-permissioned users.
			/*if($this_user['perm_editor'] !== true) // Make sure that the published value stays the same.
				$article['published'] = $articles[0]['published'];
			else*/if($article['published'] != $articles[0]['published']) // If we're allowed to changed if the article is published and we change it.  Also change the date.
				$article['date'] = time();
			// Check the categories
			for($i = 1;$i < 32;$i++)
			{
				if(!((1<<$i) & $this_user['category_permissions']) && ($articles[0]['category'] & (1<<$i)))
					$article['category'] |= 1<<$i;
			}
			// Now compare the attachments, if any are missing try to delete them.
			$old_attachments = unserialize($articles[0]['attachments']);
			foreach($old_attachments as $attachment)
			{
				if($attachment == '')
					break;

				if(strpos($article['attachments'], $attachment['file']) === false)
				{
					unlink(FILES_DIR.$attachment['file']);
					if(!empty($attachment['thumbnail']))
						unlink(FILES_DIR.$attachment['thumbnail']);
				}
			}

			if(!empty($article['attachments']))
			{
				$attachments = unserialize($article['attachments']);
				foreach($attachments as $attachment)
				{
					if($attachment['isimage'] && !empty($attachment['thumbnail']))
					{
						$article['thumbnail'] = $attachment['thumbnail'];
						break;
					}
				}
			}
			else
				$article['thumbnail'] = '';

			$db->query('articles', BITOWL_DB_UPDATE, BITOWL_DB_ROW, $article, BITOWL_DB_WHERE, array('id', $article['id']));
			// If the article is published or it used to be published, recalculate RSS feeds.
			if($article['published'] || $articles[0]['published'])
				checkForAffectedFeeds($article['category']);
			$template_engine->variables['message'] = language('M_ARTICLEEDITED');
		}
	}
	else
	{
		$attachments = unserialize($article['attachments']);
		foreach($attachments as $attachment)
		{
			if($attachment['isimage'] && !empty($attachment['thumbnail']))
			{
				$article['thumbnail'] = $attachment['thumbnail'];
				break;
			}
		}

		$db->query('articles', BITOWL_DB_INSERT, BITOWL_DB_ROW, $article);
		$template_engine->variables['message'] = language('M_ARTICLEADDED');
	}
	$template_engine->template('templates/cp/message_confirm.html');
}
else
{
	$article = makeCleanArticle();
	$article_id = isset($_GET['edit']) ? $_GET['edit'] : (isset($_POST['editing']) ? $_POST['editing'] : 0);
	$editing = (isset($_GET['edit']) && is_numeric($_GET['edit'])) || (isset($_POST['editing']) && is_numeric($_POST['editing']));

	$delete_later = array();
	if($editing)
	{
		$articles = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $article_id));
		if(($articles[0]['id'] == $article_id) && checkArticleOwnership($articles[0], $this_user))
		{
			$category[0] = true;
			for($i = 1;$i < MAX_CATEGORIES;$i++)
			{
				$category[$i] = $articles[0]['category'] & (1<<$i);
			}
			$articles[0]['category'] = $category;
			$delete_later = unserialize($articles[0]['attachments']);
			$article = $articles[0];
		}

		if(!isset($article))
			$editing = false;
	}
	else
	{
		$article['category'] = array();
		// If the warning level is high this is needed,
		for($i = 1;$i < MAX_CATEGORIES;$i++)
		{
			$article['category'][$i] = false;
		}
	}
	if(isset($_POST['title']))
	{
		$article = makeArticleWithPostData();

		// Split the categories
		$category[0] = true;
		for($i = 1;$i < MAX_CATEGORIES;$i++)
		{
			$category[$i] = $article['category'] & (1<<$i);
		}
		$article['category'] = $category;

		if(isset($_POST['editing'])) // Make sure we continue to edit.
		{
			$_GET['edit'] = $_POST['editing'];
			$editing = true;
		}
	}

	if(!isset($article['attachments']))
		$article['attachments'] = '';
	$old_attachments = unserialize(isset($_POST['attachments']) ? stripslashes($_POST['attachments']) : $article['attachments']);
	$article['attachments'] = array();
	if(is_array($old_attachments))
	{
		for($i = 0;$i < count($old_attachments);$i++)
		{
			if(!isset($_POST['delete_attachment'][$i]))
			{
				// If we are realigning, output a new thumbnail to the same file.
				if(isset($_POST['align_attachment'][$i]) && $_POST['alignment'][$i] != '')
				{
					generateImageThumbnail(FILES_DIR.$old_attachments[$i]['file'], $config->setting['journalist']['thumbnailwidth'], $config->setting['journalist']['thumbnailheight'], $_POST['alignment'][$i], $old_attachments[$i]['thumbnail']);
					$old_attachments[$i]['thumbwidth'] = $config->setting['journalist']['thumbnailwidth'];
					$old_attachments[$i]['thumbheight'] = $config->setting['journalist']['thumbnailheight'];
				}
				if(isset($_POST['watermark_attachment'][$i]))
				{
					watermarkImage(FILES_DIR.$old_attachments[$i]['file']);
				}
				$attachments[] = array('filename' => BitOwl_Upload::stripFilename($old_attachments[$i]['file']), 'size' => readablefilesize(FILES_DIR.$old_attachments[$i]['file']), 'url' => $config->setting['system']['files']['url'].'/download.php?file='.$old_attachments[$i]['file'], 'isimage' => $old_attachments[$i]['isimage']);
				$article['attachments'][] = $old_attachments[$i];
			}
			else // Try to delete the actual file.
			{
				$dont_delete = false;
				foreach($delete_later as $file)
				{
					if($file['file'] == $old_attachments[$i]['file'])
					{
						$dont_delete = true;
						break;
					}
				}
				if(!$dont_delete)
				{
					unlink(FILES_DIR.$old_attachments[$i]['file']);
					if(!empty($old_attachments[$i]['thumbnail']))
						unlink(FILES_DIR.$old_attachments[$i]['thumbnail']);
				}
			}
		}
	}
	if(isset($_POST['upload']))
	{
		$upload = new BitOwl_Upload('attachment');
		if($upload->isUploaded())
		{
			$upload->upload();
			$new_attachment = identifyAttachment($upload->getDestination());
			$new_attachment['thumbnail'] = generateImageThumbnail(FILES_DIR.$new_attachment['file'], $config->setting['journalist']['thumbnailwidth'], $config->setting['journalist']['thumbnailheight']);
			$new_attachment['thumbwidth'] = $config->setting['journalist']['thumbnailwidth'];
			$new_attachment['thumbheight'] = $config->setting['journalist']['thumbnailheight'];
			$attachments[] = array('filename' => $upload->getName(), 'size' => readablefilesize(FILES_DIR.$upload->getDestination()), 'url' => $config->setting['system']['files']['dir'].'/download.php?file='.$upload->getDestination(), 'isimage' => $new_attachment['isimage']);
			$article['attachments'][] = $new_attachment;
		}
	}

	// Preview
	function previewBBCodeAttachmentFunc($number, $innerValue)
	{
		global $config, $article;
		if($article['attachments'][0] == '' || count($article['attachments']) < $number)
			return '';

		$attachment = &$article['attachments'][$number];
		if($attachment['isimage'])
		{
			return '<img src="'.$config->setting['system']['files']['url'].'/download.php?file='.$attachment['thumbnail'].'" alt="" title="'.$innerValue.'" style="float: right" />';
		}
		return '<a href="'.$_bitowl['system']['files']['url'].'/download.php?file='.$attachment['file'].'">'.$innerValue.'</a>';
	}

	$preview = NULL;
	if(isset($_POST['preview']))
	{
		$preview = parseBBCode($article['story'], 'previewBBCodeAttachmentFunc');
	}

	$article['attachments'] = entities_encode(serialize($article['attachments']));

	$form = new BitOwl_Form(($editing ? BitOwl_Form::BTN_EDIT : BitOwl_Form::BTN_SUBMIT)|BitOwl_Form::BTN_PREVIEW);

	if($preview != NULL)
	{
		$group = $form->newGroup(language('PREVIEW').': '.$article['title'], 'preview');
		$group->newWidget(BitOwl_FormWidget::LABEL, $preview);
	}

	$group = $form->newGroup(language('F_COMPOSE'));
	if($editing)
		$group->newWidget(BitOwl_FormWidget::HIDDEN, NULL, 'editing', $article_id);
	$group->newWidget(BitOwl_FormWidget::LONGTEXT, language('TITLE'), 'title', $article['title']);
	$group->newWidget(BitOwl_FormWidget::STORYBOX, language('STORY'), 'story', $article['story']);
	if(count($available_categories) > 0)
	{
		array_unshift($available_categories, array('id' => 0, 'name' => '--------------'));
		$group->newWidget(BitOwl_FormWidget::SELECTION, language('CATEGORY'), 'categories[]', $article['category'], $available_categories);
	}
	if($this_user['perm_editor'])
		$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('PUBLISH'), 'publish', $article['published']);

	$form->printForm(BitOwl_Form::BTN_UPLOAD, true);

	$group = $form->newGroup(language('UPLOADATTACHMENTS'));
	$group->newWidget(BitOwl_FormWidget::HIDDEN, NULL, 'attachments', $article['attachments']);
	if(count($attachments) > 0)
	{
		$alignmentOptions = array(
			array('id' => '', 'name' => ''),
			array('id' => ALIGNMENT_TOP_LEFT, 'name' => language('ALIGNMENTTOPLEFT')),
			array('id' => ALIGNMENT_CENTER, 'name' => language('ALIGNMENTCENTER')),
			array('id' => ALIGNMENT_BOTTOM_RIGHT, 'name' => language('ALIGNMENTBOTTOMRIGHT'))
		);

		$attachmentsTable = array();
		foreach($attachments as $key => $attachment)
		{
			$attachmentsTable[] = array(
				'<a href="'.$attachment['url'].'">'.$attachment['filename'].'</a>',
				$attachment['isimage'] ? new BitOwl_FormWidget(BitOwl_FormWidget::COMBOBOX, NULL, "alignment[$key]", '', $alignmentOptions) : '',
				$attachment['size'],
				($attachment['isimage'] ?
					'<input type="submit" name="align_attachment['.$key.']" value="'.language('ALIGN').'" /> <input type="submit" name="watermark_attachment['.$key.']" value="'.language('WATERMARK').'" />' : '') .
					'<span id="insert"></span><input type="submit" name="delete_attachment['.$key.']" value="'.language('DELETE').'" />'
			);
		}
		$group->newTable(array(language('FILENAME'), language('ALIGNMENT'), language('SIZE'), language('ACTIONS')), $attachmentsTable);
	}
	$group->newWidget(BitOwl_FormWidget::UPLOAD, language('UPLOAD'), 'attachment');

	$form->printForm();

	// Some additional javascript
	$template_engine->template('templates/cp/journalist_compose.html');
}
?>
