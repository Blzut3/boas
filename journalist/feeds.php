<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_journalist_admin'])
{
	die();
}

$showTable = true;
if(isset($_POST['title']))
{
	$showTable = false;
	$feed = array(
		'title' => $_POST['title'],
		'description' => $_POST['description'],
		'link' => $_POST['link'],
		'file' => $_POST['filename'],
		'categories' => 1
	);
	if(is_array($_POST['categories']))
	{
		foreach($_POST['categories'] as &$category)
		{
			if($category == 0)
				continue;
			$feed['categories'] |= 1<<($category-1);
		}
	}
	if(!isset($_POST['edit']))
		$db->query('feeds', BITOWL_DB_INSERT, BITOWL_DB_ROW, $feed);
	else
	{
		$feed['id'] = (int)$_POST['editid'];
		$db->query('feeds', BITOWL_DB_UPDATE, BITOWL_DB_ROW, $feed);
	}
	$rss = new RSSFeed($feed);
	$rss->generate();

	$template_engine->variables['destination'] = '?cp=journalist&amp;func=feeds';
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->variables['message'] = language(isset($_POST['edit']) ? 'M_FEEDEDITED' : 'M_FEEDADDED');
	$template_engine->template('templates/cp/message_confirm.html');
}
elseif(isset($_GET['delete']) || isset($_POST['delete']))
{
	$feeds = $db->query('feeds', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', isset($_POST['delete']) ? $_POST['delete'] : $_GET['delete']));
	if(isset($_GET['delete']))
	{
		$showTable = false;
		$template_engine->variables['message_title'] = language('DELETEFEED');
		$template_engine->variables['message'] = language('C_DELETEFEED', $feeds[0]['title']);
		$template_engine->variables['post_fields'] = array(
			array('name' => 'delete', 'value' => $_GET['delete'])
		);
		$template_engine->variables['destination'] = '?cp=journalist&amp;func=feeds';
		$template_engine->template('templates/cp/message_confirm.html');
	}
	else
	{
		$db->query('feeds', BITOWL_DB_DELETE, BITOWL_DB_ROW, $feeds[0]);
	}
}
elseif(isset($_GET['resync']))
{
	$showTable = false;
	$feeds = $db->query('feeds', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $_GET['resync']));
	$rss = new RSSFeed($feeds[0]);
	$rss->generate();
	$template_engine->variables['destination'] = '?cp=journalist&amp;func=feeds';
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->variables['message'] = language('M_FEEDRESYNCED');
	$template_engine->template('templates/cp/message_confirm.html');
}

if($showTable)
{
	if(isset($_GET['edit']) || isset($_GET['create']))
	{
		$form = new BitOwl_Form((isset($_GET['edit']) ? BitOwl_Form::BTN_EDIT : BitOwl_Form::BTN_SUBMIT)|BitOwl_Form::BTN_RESET);

		$group = $form->newGroup(isset($_GET['create']) ? language('NEWFEED') : language('EDITFEED'));
		$feed = NULL;
		if(isset($_GET['edit']))
		{
			$group->newWidget(BitOwl_FormWidget::HIDDEN, NULL, 'editid', $_GET['edit']);

			$feeds = $db->query('feeds', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $_GET['edit']));
			$category = array();
			for($i = 0;$i < 32;$i++)
			{
				$category[$i+1] = ($feeds[0]['categories']&(1<<$i)) != 0;
			}
			$feeds[0]['categories'] = $category;
			$feed = $feeds[0];
		}

		$categoriesDB = $db->query('categories', BITOWL_DB_SELECT);
		$categories = array(array('id' => 0, 'name' => '--------------'));
		foreach($categoriesDB as $category)
			$categories[] = array('id' => $category['id']+1, 'name' => $category['name']);

		$group->newWidget(BitOwl_FormWidget::TEXT, language('TITLE'), 'title', is_array($feed) ? $feed['title'] : '');
		$group->newWidget(BitOwl_FormWidget::TEXT, language('DESCRIPTION'), 'description', is_array($feed) ? $feed['description'] : '');
		$group->newWidget(BitOwl_FormWidget::TEXT, language('LINK'), 'link', is_array($feed) ? $feed['link'] : '');
		$group->newWidget(BitOwl_FormWidget::LABEL, language('LINKNOTE'));
		$group->newWidget(BitOwl_FormWidget::TEXT, language('FILENAME'), 'filename', is_array($feed) ? $feed['file'] : '');
		$group->newWidget(BitOwl_FormWidget::LABEL, language('FILERELATIVENOTE', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME'])));
		$group->newWidget(BitOwl_FormWidget::SELECTION, language('CATEGORY'), 'categories[]', is_array($feed) ? $feed['categories'] : array(), $categories);

		$form->printForm();
	}
	else
	{
		$feeds = array();
		$feedsDB = $db->query('feeds', BITOWL_DB_SELECT);
		foreach($feedsDB as $feed)
		{
			$feeds[] = array(
				$feed['title'],
				'<a href="?cp=journalist&amp;func=feeds&amp;edit='.$feed['id'].'">'.language('EDIT').'</a> <a href="?cp=journalist&amp;func=feeds&amp;delete='.$feed['id'].'">'.language('DELETE').'</a> <a href="?cp=journalist&amp;func=feeds&amp;resync='.$feed['id'].'">'.language('RESYNC').'</a>'
			);
		}

		$form = new BitOwl_Form(0);

		$group = $form->newGroup(language('ACTIVEFEEDS'));
		$group->newTable(array(language('TITLE'), language('ACTIONS')), $feeds);
		$group->newWidget(BitOwl_FormWidget::LABEL, '<a href="?cp=journalist&amp;func=feeds&amp;create=true">'.language('NEWFEED').'</a>');
	
		$form->printForm();
	}
}
?>
