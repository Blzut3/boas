<?php
//*****************************************************************************
//
//	Copyright (C) 2011  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

switch($msg)
{
	case 'ACTIVEFEEDS': return 'Active Feeds';
	case 'ARTICLESPERPAGE': return 'Articles per page';
	case 'C_DELETEARTICLE': return "Are you sure you want to delete the article \"$arg1\"?"; //$arg1 is the title of the article.
	case 'C_DELETECATEGORY': return "Are you sure you want to delete the category \"$arg1\"?"; // $arg1 is the name of the category.
	case 'C_DELETEFEED': return "Are you sure you want to delete the feed \"$arg1\"? &nbsp;Note that the file will not be automatically deleted."; // $arg1 is the title of the feed.
	case 'C_PURGEARTICLES': return 'Are you sure you want to purge the articles before the specified date.';
	case 'CATEGORY': return 'Category';
	case 'DELETEARTICLE': return 'Delete Article';
	case 'DELETECATEGORY': return 'Delete Category';
	case 'DELETEFEED': return 'Delete Feed';
	case 'EDITCATEGORY': return 'Edit Category';
	case 'EDITFEED': return 'Edit Feed';
	case 'F_CATEGORIES': return 'Categories';
	case 'F_COMPOSE': return 'Compose';
	case 'F_FEEDS': return 'Feeds';
	case 'F_MANAGEARTICLES': return 'Manage Articles';
	case 'F_PURGE': return 'Purge';
	case 'LINKNOTE': return 'Link should be the page on which the articles in the feed are published. You may use \'{article}\', \'{category}\', and \'{category-name}\'.';
	case 'LINK': return 'Link';
	case 'M_ARTICLEEDITED': return 'The article has been edited successfully.';
	case 'M_ARTICLEADDED': return 'The article has been added successfully.';
	case 'M_ARTICLESPURGED': return 'The articles before the specified date have been purged.';
	case 'M_CATEGORYEDITED': return 'The category has been edited successfully.';
	case 'M_FEEDADDED': return 'The feed has been added successfully.';
	case 'M_FEEDEDITED': return 'The feed has been edited successfully.';
	case 'M_FEEDRESYNCED': return 'The feed has been resynchronized successfully.';
	case 'MANAGECATEGORIES': return 'Manage Categories';
	case 'NEWCATEGORY': return 'New Category';
	case 'NEWFEED': return 'New Feed';
	case 'PUBLISH': return 'Publish';
	case 'PURGEARTICLES': return 'Purge Articles';
	case 'RECENTTIME': return 'Recent Time';
	case 'RECENTTIMEDESCRIPTION': return 'Amount of time in seconds for which should be considered "new".';
	case 'RESYNC': return 'Resync';
	case 'STORY': return 'Story';
	case 'UPLOADATTACHMENTS': return 'Upload Attachments';
	case 'UNPUBLISHEDARTICLES': return 'Unpublished Articles';
	default: return false;
}
?>
