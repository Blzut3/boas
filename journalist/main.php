<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_journalist'])
	die();

$form = new BitOwl_Form(0);

$group = $form->newGroup(language('VERSIONINFORMATION'));
$group->newWidget(BitOwl_FormWidget::LABEL, language('INSTALLEDVERSION'), NULL, $config->setting['version']['journalist']);

if($this_user['perm_editor'] === true)
{
	if($this_user['perm_journalist_admin'] !== true)
	{
		$categories = getAvailableCategories($db->query('categories', BITOWL_DB_SELECT));
		$unpublished_articles = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('published', 0), BITOWL_DB_WHERE, array('category', $this_user['category_permissions']&~1, 'operator' => BITOWL_DB_WHERE_BITWISEAND));
	}
	else
		$unpublished_articles = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('published', 0));

	$numUnpublished = count($unpublished_articles);
	if($numUnpublished > 0)
	{
		$group = $form->newGroup(language('UNPUBLISHEDARTICLES').': '.$numUnpublished);
		foreach($unpublished_articles as $article)
		{
			$group->newWidget(BitOwl_FormWidget::LABEL, '<a href="?cp=journalist&amp;func=compose&amp;edit='.$article['id'].'">'.$article['title'].'</a>');
		}
	}
}

$form->printForm();
?>
