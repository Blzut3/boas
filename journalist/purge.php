<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_journalist_admin'])
{
	die();
}

if(isset($_POST['year']))
{
	if(is_numeric($_POST['year']) && is_numeric($_POST['month']) && is_numeric($_POST['day']))
	{
		$cutoff = mktime(0, 0, 0, $_POST['month'], $_POST['day'], $_POST['year']);
		if($cutoff !== false)
		{
			if(isset($_POST['confirm']))
			{
				$articlesToDelete = $db->query('articles', BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('attachments'), BITOWL_DB_WHERE, array('date', $cutoff, 'operator' => BITOWL_DB_WHERE_LESSTHAN));
				if(is_array($articlesToDelete))
				{
					foreach($articlesToDelete as &$article)
					{
						$attachments = unserialize($article['attachments']);
						if(is_array($attachments))
						{
							foreach($attachments as $attachment)
							{
								unlink(FILES_DIR.$attachment['file']);
								if($attachment['isimage'] && !empty($attachment['thumbnail']))
									unlink(FILES_DIR.$attachment['thumbnail']);
							}
						}
					}

					// If no articles where found there's nothing to delete, so don't bother with this query.
					$db->query('articles', BITOWL_DB_DELETE, BITOWL_DB_WHERE, array('date', $cutoff, 'operator' => BITOWL_DB_WHERE_LESSTHAN));
				}
				$template_engine->variables['message'] = language('M_ARTICLESPURGED');
			}
			else
			{
				$template_engine->variables['post_fields'] = array(
					array('name' => 'confirm', 'value' => true),
					array('name' => 'year', 'value' => $_POST['year']),
					array('name' => 'month', 'value' => $_POST['month']),
					array('name' => 'day', 'value' => $_POST['day'])
				);
				$template_engine->variables['message'] = language('M_CONFIRMPURGE');
			}

			$template_engine->variables['destination'] = '?cp=journalist&amp;func=purge';
			$template_engine->variables['message_title'] = language('MESSAGE');
			$template_engine->template('templates/cp/message_confirm.html');
		}
	}
}
else
{
	$form = new BitOwl_Form();
	if(isset($_POST['year']))
		$form->addMessage(language('PURGEARTICLES'));

	$group = $form->newGroup(language('PURGEARTICLES'));
	$group->newWidget(BitOwl_FormWidget::TEXT, language('YEAR'), 'year', isset($_POST['year']) ? $_POST['year'] : '');
	$group->newWidget(BitOwl_FormWidget::TEXT, language('MONTH'), 'month', isset($_POST['month']) ? $_POST['month'] : '');
	$group->newWidget(BitOwl_FormWidget::TEXT, language('DAY'), 'day', isset($_POST['day']) ? $_POST['day'] : '');

	$form->printForm();
}
?>
