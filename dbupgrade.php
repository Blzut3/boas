<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************
// Checks for an outdated database and performs upgrade operations if needed.

define('DATABASE_VERSION', 2);

// Assume if we don't have a db version set we have a new install.
if(!isset($config->setting['version']['db']))
{
	$config->setting['version']['db'] = DATABASE_VERSION;
	$config->save();
}

if($config->setting['version']['db'] < DATABASE_VERSION)
{
	switch($config->setting['version']['db'])
	{
		case 1:
			$founder = $db->query('users', BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('id', 'permissions'), BITOWL_DB_WHERE, array('id', 0));
			if(is_array($founder))
			{
				$founder[0]['permissions'] = 'perm_founder,'.$founder[0]['permissions'];
				$db->query('users', BITOWL_DB_UPDATE, BITOWL_DB_ROW, $founder[0]);
			}
		default:
			break;
	}
	$config->setting['version']['db'] = DATABASE_VERSION;
	$config->save();
}
?>
