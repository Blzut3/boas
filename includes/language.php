<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!isset($script_path))
{
	function language($msg, $arg1='')
	{
		global $config, $cp;
		$msg = strtoupper($msg);
		$value = require './languages/'.$config->setting['system']['language'].'.php';
		if($value === false)
		{
			if(file_exists("./$cp/lang_".$config->setting['system']['language'].'.php'))
				$value = require "./$cp/lang_".$config->setting['system']['language'].'.php';
			if($value === false)
				return ucfirst(strtolower($msg));
		}
		return $value;
	}
}
else
{
	function language($msg, $arg1='')
	{
		global $_bitowl, $config, $script_path;
		$msg = strtoupper($msg);
		$value = require "$script_path/languages/".$_bitowl['config']->setting['system']['language'].'.php';
		if($value === false)
			return ucfirst(strtolower($msg));
		return $value;
	}
}
?>