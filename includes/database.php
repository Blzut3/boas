<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

define('BITOWL_DB_SELECT',			1);
define('BITOWL_DB_INSERT',			2);
define('BITOWL_DB_UPDATE',			3);
define('BITOWL_DB_DELETE',			4);
define('BITOWL_DB_WHERE',			5);
define('BITOWL_DB_LIMIT',			6);
define('BITOWL_DB_COLUMNLIST',		7);
define('BITOWL_DB_VALUES',			8);
define('BITOWL_DB_ROW',				9);
define('BITOWL_DB_RCHRONOLOGICAL',	10);
define('BITOWL_DB_PAGINATED',		11);
define('BITOWL_DB_NESTEDSET',		12);
define('BITOWL_DB_SELECTCHILDREN',	13);
define('BITOWL_DB_SELECTPARENTS',	14);
define('BITOWL_DB_SELIMEDCHILDREN',	15);
define('BITOWL_DB_SETNSPARENT',		16);
define('BITOWL_DB_SEARCH',			255); // Note: This command is not guarenteed to be implemented and should only be used by end-users.

define('BITOWL_DB_WHERE_EQUALS',		0);
define('BITOWL_DB_WHERE_BITWISEAND',	1);
define('BITOWL_DB_WHERE_LESSTHAN',		2);
define('BITOWL_DB_WHERE_AND',			128);
define('BITOWL_DB_WHERE_OR',			129);

define('BITOWL_DB_CREATETABLE_BOOL',	0);	// 0 or 1
define('BITOWL_DB_CREATETABLE_INT',		1);	// -2^31 - 2^31
define('BITOWL_DB_CREATETABLE_STRING',	2);	// Up to 255 characters.
define('BITOWL_DB_CREATETABLE_TEXT',	3);	// Unlimited characters.

define('BITOWL_DB_CREATETABLE_NESTEDSET',	0x1);
define('BITOWL_DB_CREATETABLE_FLIPPED',		0x2); // Optimize for fetching backwards.

define('BITOWL_DB_MODIFYTABLE_INSERT',	0);
define('BITOWL_DB_MODIFYTABLE_DELETE',	1);

/* The database class eases the process of supporting multiple database types.
 * All access to the databases should go though here.
 */
abstract class BitOwl_Database
{
	//The following 5 variables deal with how to connect and access the database
	protected $dbtype;
	protected $hostname;
	protected $username;
	protected $password;
	protected $database;
	/* Connection stores connections to the database; in flatfile this would be
	 * the result of the fopen function.
	 */
	protected $connection;

	/* Stores the value from the last call with BITOWL_DB_PAGINATED.
	 */
	protected $totalResults;

	/* Constructor will detect between MySQL and flatfile.  If useing MySQL it
	 * also creates a table list for use later when we may need to get data from
	 * multiple tables.
	 */
	function __construct($host, $user=false, $pass=false, $db=false, $processorintensive=false) //host is only needed on flatfile
	{
		$this->dbtype = 'Unknown';
		$this->hostname = $host;
		$this->username = $user;
		$this->password = $pass;
		$this->database = $db;
	}

	/* Searches $argc for $section.  Returns the index if found or -1.
	 */
	protected function findQuerySection($section, $argc, $argv, $expected=0, $start=0)
	{
		if($start < 0)
			$start = 0;

		for($i = $start;$i < $argc-$expected;$i++)
		{
			if($argv[$i] === $section)
				return $i+1;
		}
		return -1;
	}

	abstract public function query($table, $action);
	/* Returns the name of the database type.
	 */
	public function getName()
	{
		return language('UNKNOWNDB');
	}
	/* Returns the size in bytes of all database files/tables
	 */
	abstract public function getSize();

	public function getTotalResults() { return $this->totalResults; }

	/* The following function should only be used during installation processes.
	 *
	 * Pass in a list of columns.
	 *	array(
	 *		'name' => 'columnName',
	 *		'type' => BITOWL_DB_CREATETABLE_BOOL
	 *	);
	 */
	abstract public function createTable($table, $flags);

	abstract public function modifyTable($table, $action, $column, $after='', $type=NULL, $default='');

	public static function createDatabaseObject($host, $user=false, $pass=false, $db=false)
	{
		if($user === false)
			$type = 'FlatFile';
		else
			$type = 'MySQL';
		if(require_once 'dbtypes/'.strtolower($type).'.php')
		{
			$classname = "BitOwl_Database_$type";
			return new $classname($host, $user, $pass, $db);
		}
		else
			throw new Exception("Database of type \"$type\" not found.");
	}
}
?>