<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

final class BitOwl_FFTable
{
	private $file;
	private $outFile;
	private $filename;
	private $autoIndex;
	private $flags;
	private $keys;
	private $tableOffset;

	private static $ESCAPE_TRANSLATIONS = array(
		'\\\\' => '\\',
		'\\n' => "\n",
		'\\r' => "\r",
		'\\t' => "\t",
		'\\"' => '"'
	);

	const DB_FLAG_FLIPPED = 0x1;

	function __construct($file, $create=false)
	{
		// Open the database file.
		$this->outFile = NULL;
		$this->autoIndex = 0;
		$this->filename = $file;
		$this->file = fopen($file, !$create ? 'r+' : 'w+');
		if($this->file === false)
			die(language('E_QUERY'));

		if(!$create)
			$this->readHeader();
	}

	function __destruct()
	{
		fclose($this->file);
		if($this->outFile !== NULL)
			fclose($this->outFile);
	}

	private static function escape(&$string)
	{
		$string = strtr($string, array_flip(self::$ESCAPE_TRANSLATIONS));
	}

	private static function unescape(&$string)
	{
		$string = strtr($string, self::$ESCAPE_TRANSLATIONS);
	}

	private function makeHeader()
	{
		return str_pad(dechex($this->autoIndex), 8, '0', STR_PAD_LEFT).','.$this->flags.','.implode(',', $this->keys)."\n";
	}

	private function startWriting()
	{
		// In order to write to the middle of a file, we need to open a new file
		// and feed the lines before and after from the old file.
		if($this->outFile !== NULL)
			fclose($this->outFile);
		$this->outFile = fopen($this->filename.'.new', 'w');
		if($this->outFile === false)
			die(language('E_QUERY'));

		// Write the header.
		$header = $this->makeHeader();
		fwrite($this->outFile, $header) or die(language('E_QUERY'));
	}
	// Writes just the autoindex.
	private function updateAutoIndex()
	{
		fseek($this->file, 0, SEEK_SET);
		fwrite($this->file, str_pad(dechex($this->autoIndex), 8, '0', STR_PAD_LEFT), 8);
	}
	// This function reads all the lines from the current position on and feeds
	// them into the new file.
	private function loopRows()
	{
		while(($line = fgets($this->file, 10240)) !== false)
			fwrite($this->outFile, $line);
	}
	private function finishWriting()
	{
		// Close the streams to both the new and old file, move the new file
		// over the old file and preceed to reopen.
		fclose($this->outFile);
		fclose($this->file);
		rename($this->filename.'.new', $this->filename);
		$this->file = fopen($this->filename, 'r+');
		if($this->file === false)
			die(language('E_QUERY'));
		$this->outFile = NULL;
		$this->readHeader();
	}
	private function readHeader()
	{
		// Read the header.
		$this->keys = fgets($this->file, 512);
		if($this->keys === false)
			die(language('E_QUERY'));
		$this->keys = explode(',', trim($this->keys));
		// The first parameter is a 8 byte long string for the auto index
		// position.
		$this->autoIndex = hexdec(array_shift($this->keys));
		// The second parameter should be integer so we'll shift it out of the
		// keys array.
		$this->flags = (int) array_shift($this->keys);

		// Store the offset so we can return to it later.
		$this->tableOffset = ftell($this->file);
	}

	public function addColumn($name, $after='', $default=NULL)
	{
		$columnNumber = -1;

		if(empty($after))
		{
			$columnNumber = count($this->keys);
			$this->keys[] = $name;
		}
		else
		{
			for($i = 0;$i < count($this->keys);$i++)
			{
				if($this->keys[$i] == $after)
				{
					$columnNumber = $i+1;
					array_splice($this->keys, $columnNumber, 0, array($name));
					break;
				}
			}
		}

		// Update all of the rows.
		if($columnNumber != -1)
		{
			$this->startWriting();
			while(($nextRow = $this->getRow()) !== false)
			{
				array_splice($nextRow, $columnNumber, 0, $default);
				fputcsv($this->outFile, $nextRow) or die(language('E_QUERY'));
			}
			$this->finishWriting();
		}
	}

	public function deleteColumn($column)
	{
		$colNum = -1;
		for($i = 0;$i < count($this->keys);$i++)
		{
			if($this->keys[$i] == $column)
			{
				$colNum = $i;
				unset($this->keys[$i]);
				break;
			}
		}

		if($colNum != -1)
		{
			$this->startWriting();
			while(($nextRow = $this->getRow()) !== false)
			{
				unset($nextRow[$colNum]);
				fputcsv($this->outFile, $nextRow) or die(language('E_QUERY'));
			}
			$this->finishWriting();
		}
	}

	public function createTable($flags)
	{
		$this->flags = $flags;
		$this->startWriting();
		$this->finishWriting();
	}

	public function getFlags()
	{
		return $this->flags;
	}

	// Gets a row in the database.  In order to use it associatively call useRow().
	public function getRow()
	{
		$row = fgetcsv($this->file, 10240, ',');
		if($row === false)
			return false;
		return $row;
	}

	public function useRow(&$row)
	{
		$row = array_combine($this->keys, $row);
		array_walk($row, array('BitOwl_FFTable', 'unescape'));
	}

	public function insertRow($row, $parentRow=NULL)
	{
		// Lets make sure we have the correct number of indexes
		$newRow = array_fill_keys($this->keys, '');
		foreach($this->keys as $key)
		{
			if(isset($row[$key]))
				$newRow[$key] = $row[$key];
		}
		$row = $newRow;

		$row['id'] = $this->autoIndex++;
		array_walk($row, array('BitOwl_FFTable', 'escape'));
		if($parentRow === NULL)
		{
			if($this->flags & self::DB_FLAG_FLIPPED)
			{
				$this->reset();
				$this->startWriting();
				fputcsv($this->outFile, $row) or die(language('E_QUERY'));
				$this->loopRows();
				$this->finishWriting();
			}
			else
			{
				$this->updateAutoIndex();
				// If the database is top-to-bottom we can simply seek to the end
				// and append the line.
				fseek($this->file, 0, SEEK_END);
				fputcsv($this->file, $row) or die(language('E_QUERY'));
			}
		}
		else
		{
			// If we are working with nested sets, we could potentually need to
			// update every row in the table.
			$this->reset();
			$this->startWriting();
			if($this->flags & self::DB_FLAG_FLIPPED)
				fputcsv($this->outFile, $row) or die(language('E_QUERY'));
			while(($nextRow = $this->getRow()) !== false)
			{
				$this->useRow($nextRow);
				if($nextRow['lft'] >= $parentRow['rgt'])
					$nextRow['lft'] += 2;
				if($nextRow['rgt'] >= $parentRow['rgt'])
					$nextRow['rgt'] += 2;
				fputcsv($this->outFile, $nextRow) or die(language('E_QUERY'));
			}
			if(!($this->flags & self::DB_FLAG_FLIPPED))
				fputcsv($this->outFile, $row) or die(language('E_QUERY'));
			$this->finishWriting();
		}
	}

	public function updateRow($row, $nestedset, $nodeSize, $moveLeft, $moveRight, $newOffset)
	{
		array_walk($row, array('BitOwl_FFTable', 'escape'));
		$newRow = array_fill_keys($this->keys, '');
		foreach($this->keys as $key)
		{
			if(isset($row[$key]))
				$newRow[$key] = $row[$key];
		}
		$row = $newRow;
		$this->reset();
		$this->startWriting();
		// Scan for the location of the row we're modifying by comparing the
		// unique ID.
		while(($nextRow = $this->getRow()) !== false)
		{
			$raw = $nextRow;
			$this->useRow($nextRow);
			if(!$nestedset)
			{
				if($nextRow['id'] == $row['id'])
				{
					fputcsv($this->outFile, $row) or die(language('E_QUERY'));
					$this->loopRows();
					break;
				}
				else
					fputcsv($this->outFile, $raw) or die(language('E_QUERY'));
			}
			else
			{
				if($nextRow['id'] == $row['id'])
					fputcsv($this->outFile, $row) or die(language('E_QUERY'));
				else
				{
					if(!($nextRow['lft'] >= $row['lft'] && $nextRow['rgt'] <= $row['rgt']))
					{
						if($nextRow['lft'] > $row['lft'])
							$nextRow['lft'] -= $nodeSize;
						if($nextRow['rgt'] > $row['rgt'])
							$nextRow['rgt'] -= $nodeSize;
						if($nextRow['lft'] > $moveLeft)
							$nextRow['lft'] += $nodeSize;
						if($nextRow['rgt'] >= $moveRight)
							$nextRow['rgt'] += $nodeSize;
					}
					fputcsv($this->outFile, $nextRow) or die(language('E_QUERY'));
				}
			}
		}
		$this->finishWriting();
	}

	public function deleteRow($row, $nestedset)
	{
		$this->reset();
		$this->startWriting();
		// Look for the row and delete it.
		while(($nextRow = $this->getRow()) !== false)
		{
			$raw = $nextRow;
			$this->useRow($nextRow);
			if($nextRow['id'] == $row['id'])
			{
				if(!$nestedset)
				{
					$this->loopRows();
					break;
				}
			}
			else
			{
				if($nestedset)
				{
					if($nextRow['lft'] > $row['lft'] && $nextRow['lft'] < $row['rgt'])
					{
						$nextRow['lft']--;
						$nextRow['rgt']--;
					}
					if($nextRow['rgt'] > $row['rgt'])
						$nextRow['rgt'] -= 2;
					if($nextRow['lft'] > $row['lft'])
						$nextRow['lft'] -= 2;
				}
				fputcsv($this->outFile, $raw);
			}
		}
		$this->finishWriting();
	}

	public function reset()
	{
		fseek($this->file, $this->tableOffset, SEEK_SET);
	}
}

final class BitOwl_Database_FlatFile extends BitOwl_Database
{
	function __construct($host, $user=false, $pass=false, $db=false)
	{
		parent::__construct($host, $user, $pass, $db);
		$this->dbtype = 'flatfile';

		if(!is_dir($this->hostname))
			mkdir($this->hostname);
	}

	// Removes all the array indexes of which are not included by the
	// COLUMNSLIST section.
	private function cleanRow($row, $argc, $argv)
	{
		$section = 0;
		if(($section = $this->findQuerySection(BITOWL_DB_COLUMNLIST, $argc, $argv, 1)) != -1)
			return array_intersect_key($row, array_flip($argv[$section]));
		return $row;
	}
	// Checks the input against the WHERE clauses.
	private function qualifyRow($row, $argc, $argv, $nsSelection, $parent, $parentRow=NULL)
	{
		$result = true;
		$section = 0;

		if($nsSelection != 0 && $parentRow != NULL)
		{
			switch($nsSelection)
			{
				default:
					break;
				case BITOWL_DB_SELECTCHILDREN:
					$result = $row['lft'] > $parentRow['lft'] && $row['rgt'] < $parentRow['rgt'];
					break;
				case BITOWL_DB_SELECTPARENTS:
					$result = $row['lft'] <= $parentRow['lft'] && $row['rgt'] >= $parentRow['rgt'];
					break;
				case BITOWL_DB_SELIMEDCHILDREN:
					$result = $row['lft'] > $parentRow['lft'] && $row['rgt'] < $parentRow['rgt'] && $row['depth'] == $parentRow['depth']+1;
					break;
			}
		}

		while(($section = $this->findQuerySection(BITOWL_DB_WHERE, $argc, $argv, 1, $section)) != -1)
		{
			$nextResult = false;
			$operator = isset($argv[$section]['operator']) ? $argv[$section]['operator'] : BITOWL_DB_WHERE_EQUALS;

			if(isset($argv[$section]['parent']) && $parent != $argv[$section]['parent'])
				continue;

			switch($operator)
			{
				case BITOWL_DB_WHERE_EQUALS:
					$nextResult = $row[$argv[$section][0]] == $argv[$section][1];
					break;
				case BITOWL_DB_WHERE_LESSTHAN:
					$nextResult = $row[$argv[$section][0]] < $argv[$section][1];
					break;
				case BITOWL_DB_WHERE_BITWISEAND:
					$nextResult = ($row[$argv[$section][0]] & $argv[$section][1]) != 0;
					break;
			}

			$multiop = isset($argv[$section]['multioperator']) ? $argv[$section]['multioperator'] : BITOWL_DB_WHERE_AND;
			switch($multiop)
			{
				default:
				case BITOWL_DB_WHERE_AND:
					$result = $result && $nextResult;
					break;
				case BITOWL_DB_WHERE_OR:
					$result = $result || $nextResult;
					break;
			}
		}

		return $result;
	}

	public function query($table, $action)
	{
		$queries = array();
		$argc = func_num_args();
		$argv = func_get_args();
		if($argc < 1)
			return;

		// First split BITOWL_DB_ROW
		$section = 0;
		while(($section = $this->findQuerySection(BITOWL_DB_ROW, $argc, $argv, 1, $section)) != -1)
		{
			$argv[] = BITOWL_DB_COLUMNLIST;
			$argv[] = array_keys($argv[$section]);
			$argv[] = BITOWL_DB_VALUES;
			$argv[] = array_values($argv[$section]);
			$argc += 4;
		}

		// Open database file
		$file = new BitOwl_FFTable($this->hostname.'/'.$table.'.db');

		// Collect meta data (such as limits)
		$rowLimit = 512;
		$rowStart = 0;
		$rChronological = $this->findQuerySection(BITOWL_DB_RCHRONOLOGICAL, $argc, $argv, 0) != -1;
		if($file->getFlags() & BitOwl_FFTable::DB_FLAG_FLIPPED)
			$rChronological = !$rChronological;
		$paginated = $this->findQuerySection(BITOWL_DB_PAGINATED, $argc, $argv, 0) != -1;
		$nestedset = $this->findQuerySection(BITOWL_DB_NESTEDSET, $argc, $argv, 0) != -1;
		if(($section = $this->findQuerySection(BITOWL_DB_LIMIT, $argc, $argv, 1)) != -1)
		{
			$rowStart = $argv[$section][0];
			$rowLimit = $argv[$section][1];
		}

		$rows = array();
		$numRows = !$rChronological ? -$rowStart : 0;
		// The first argument has special requirements
		switch($action)
		{
			case BITOWL_DB_SELECT:
				$nsSelection = 0;
				if($this->findQuerySection(BITOWL_DB_SELECTCHILDREN, $argc, $argv, 1) != -1)
					$nsSelection = BITOWL_DB_SELECTCHILDREN;
				elseif($this->findQuerySection(BITOWL_DB_SELECTPARENTS, $argc, $argv, 1) != -1)
					$nsSelection = BITOWL_DB_SELECTPARENTS;
				elseif($this->findQuerySection(BITOWL_DB_SELIMEDCHILDREN, $argc, $argv, 1) != -1)
					$nsSelection = BITOWL_DB_SELIMEDCHILDREN;

				$parentRow = NULL;
				if($nestedset)
				{
					while(($row = $file->getRow()) !== false)
					{
						if($row[0] !== NULL)
						{
							$file->useRow($row);
							if($this->qualifyRow($row, $argc, $argv, 0, true))
							{
								$parentRow = $row;
								break;
							}
						}
					}
					$file->reset();
				}

				while(($row = $file->getRow()) !== false)
				{
					if(!$paginated && !$rChronological && $numRows >= $rowLimit)
						break;

					if($row[0] !== NULL)
					{
						$file->useRow($row);
						if($this->qualifyRow($row, $argc, $argv, $nsSelection, false, $parentRow))
						{
							if($numRows++ >= 0 && ($rChronological || $numRows <= $rowLimit))
							{
								$rows[] = $this->cleanRow($row, $argc, $argv);
								if($rChronological && $numRows > $rowLimit+$rowStart)
									array_shift($rows);
							}
						}
					}
				}
				break;
			case BITOWL_DB_INSERT:
				$parentRow = NULL;
				$column_section = $this->findQuerySection(BITOWL_DB_COLUMNLIST, $argc, $argv, 1);
				$value_section = $this->findQuerySection(BITOWL_DB_VALUES, $argc, $argv, 1);

				$row = array_combine($argv[$column_section], $argv[$value_section]);
				if($nestedset)
				{
					$parent = 0;
					if(($section = $this->findQuerySection(BITOWL_DB_SETNSPARENT, $argc, $argv, 1)) != -1)
						$parent = $argv[$section][0];

					$parentRow = $this->query($table, BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $parent), BITOWL_DB_COLUMNLIST, array('lft', 'rgt', 'depth'));
					if(isset($parentRow[0]))
					{
						$parentRow = $parentRow[0];
						$row['depth'] = $parentRow['depth']+1;
						$row['lft'] = $parentRow['rgt'];
						$row['rgt'] = $parentRow['rgt']+1;
					}
					else
					{
						$parentRow = NULL;
						$row['depth'] = 0;
						$row['lft'] = 0;
						$row['rgt'] = 1;
					}
				}
				$file->insertRow($row, $parentRow);
				break;
			case BITOWL_DB_UPDATE:
				$column_section = $this->findQuerySection(BITOWL_DB_COLUMNLIST, $argc, $argv, 1);
				$value_section = $this->findQuerySection(BITOWL_DB_VALUES, $argc, $argv, 1);

				$row = array_combine($argv[$column_section], $argv[$value_section]);
				$nodeSize = $moveLeft = $moveRight = $newOffset = 0;
				if($nestedset && ($section = $this->findQuerySection(BITOWL_DB_SETNSPARENT, $argc, $argv, 1)) != -1)
				{
					$parent = $argv[$section][0];

					$parentRow = $this->query($table, BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id' => $parent), BITOWL_DB_COLUMNLIST, array('lft', 'rgt', 'depth'));
					$nodeSize = $nodeInfo['rgt'] - $nodeInfo['lft'] + 1;
					$moveLeft = $parentRow['lft'] - ($parentRow['lft'] > $nodeInfo['lft'] ? $nodeSize : 0);
					$moveRight = $parentRow['rgt'] - ($parentRow['rgt'] > $nodeInfo['rgt'] ? $nodeSize : 0);
					$newOffset = $parentRow['rgt'] - $nodeInfo['rgt'] - 1 + ($parentRow['rgt'] > $nodeInfo['rgt'] ? 0 : $nodeSize);
				}
				$file->updateRow($row, $nestedset, $nodeSize, $moveLeft, $moveRight, $newOffset);
				break;
			case BITOWL_DB_DELETE:
				// Might need to move some code into the super class in order
				// to prevent too much copy & paste.
				if($this->findQuerySection(BITOWL_DB_WHERE, $argc, $argv, 1) == -1)
				{
					// For delete we need to add a where clause if the row is passed in.
					$column_section = $this->findQuerySection(BITOWL_DB_COLUMNLIST, $argc, $argv, 1);
					$value_section = $this->findQuerySection(BITOWL_DB_VALUES, $argc, $argv, 1);
					if($column_section == -1 || $value_section == -1)
						die(language('E_MISSINGQUERYSECTION', 'BITOWL_DB_COLUMNLIST, BITOWL_DB_VALUES, BITOWL_DB_WHERE'));
					for($i = 0;$i < count($argv[$column_section]);$i++)
					{
						$argv[] = BITOWL_DB_WHERE;
						$argv[] = array($argv[$column_section][$i], $argv[$value_section][$i]);
						$argc += 2;
					}
				}

				// Due to the implementation, this may loop many more times than
				// the number of rows in the database.
				while(($row = $file->getRow()) !== false)
				{
					if($row[0] !== NULL)
					{
						$file->useRow($row);
						if($this->qualifyRow($row, $argc, $argv, 0, false))
							$file->deleteRow($row, $nestedset);
					}
				}
				break;
		}

		if($paginated)
			$this->totalResults = $numRows + (!$rChronological ? $rowStart : 0);

		return $rChronological ? array_reverse(array_slice($rows, 0, $rowLimit)) : $rows;
	}
	public function getName()
	{
		return language('FLATFILE');
	}
	public function getSize()
	{
		$size = 0;
		if($dir = opendir($this->hostname))
		{
			while(($file = readdir($dir)) !== false)
			{
				if(is_file($this->hostname.'/'.$file) && substr($file, -3) == '.db')
					$size += filesize($this->hostname.'/'.$file);
			}
		}
		return $size;
	}
	public function createTable($table, $flags)
	{
		$argc = func_num_args();
		$argv = func_get_args();
		if($argc < 1)
			return;

		$file = new BitOwl_FFTable($this->hostname.'/'.$table.'.db', true);
		$file->addColumn('id');

		for($i = 2;$i < $argc;$i++)
			$file->addColumn($argv[$i]['name']);

		if($flags & BITOWL_DB_CREATETABLE_NESTEDSET)
		{
			$file->addColumn('lft');
			$file->addColumn('rgt');
			$file->addColumn('depth');
		}

		$ffTableFlags = 0;
		if($flags & BITOWL_DB_CREATETABLE_FLIPPED)
			$ffTableFlags |= BitOwl_FFTable::DB_FLAG_FLIPPED;

		$file->createTable($ffTableFlags);
	}

	public function modifyTable($table, $action, $column, $after='', $type=NULL, $default='')
	{
		// Open database file
		$file = new BitOwl_FFTable($this->hostname.'/'.$table.'.db');

		switch($action)
		{
			default:
				$file->addColumn($column, $after, $default);
				break;
			case BITOWL_DB_MODIFYTABLE_DELETE:
				$file->deleteColumn($column);
				break;
		}
	}
}
?>
