<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

define('DATABASE_DEBUG_MODE', false);

// Special flag for use only by the MySQL class.
// This also must be the last parameter for query().
define('BITOWL_DB_RETURNQUERY', 0x7FFFFFFF);

final class BitOwl_Database_MySQL extends BitOwl_Database
{
	//private $tablelist=NULL;
	private $prefix='';

	function __construct($host, $user=false, $pass=false, $db=false)
	{
		$te = BitOwl_TemplateEngine::getStandardInstance();
		parent::__construct($host, $user, $pass, $db, false);
		$te->variables['mysql']['queries'] = 0;
		$this->dbtype = 'MySQL';
		$this->connection = new mysqli($this->hostname, $this->username, $this->password, $this->database) or die(language('E_CONNECT'));
	}
	function __destruct()
	{
		$this->connection->close();
	}

	/* This function allows us to count our queries.
	 */
	private function mysql_query($query)
	{
		if(DATABASE_DEBUG_MODE && isset($_GET['cp']))
			var_dump($query);

		$te = BitOwl_TemplateEngine::getStandardInstance();
		if(!isset($te->variables['mysql']))
			$te->variables['mysql']['queries'] = 0;
		$te->variables['mysql']['queries']++;
		return $this->connection->query($query);
	}

	/*private function checkTablelist($table)
	{
		if(!is_array($this->tablelist))
		{
			$this->tablelist = array();
			$tablelist = $this->mysql_query('SHOW TABLES LIKE \'%\\_%\'');
			while($table = $tablelist->fetch_row())
			{
				$this->tablelist[$table[0]] = true;
			}
			$tablelist->free();
		}

		return isset($this->tablelist[$table]);
	}*/

	private function getValue($value)
	{
		if(is_bool($value))
			return $value ? 1 : 0;
		elseif(is_numeric($value))
			return $value;
		elseif(is_string($value))
			return '"'.addslashes($value).'"';
		return 'NULL';
	}

	public function query($table, $action)
	{
		$postqueries = array();
		$queries = array();
		$argc = func_num_args();
		$argv = func_get_args();
		if($argc < 1)
			return;

		// First split BITOWL_DB_ROW
		$section = 0;
		while(($section = $this->findQuerySection(BITOWL_DB_ROW, $argc, $argv, 1, $section)) != -1)
		{
			$argv[] = BITOWL_DB_COLUMNLIST;
			$argv[] = array_keys($argv[$section]);
			$argv[] = BITOWL_DB_VALUES;
			$argv[] = array_values($argv[$section]);
			$argc += 4;
		}

		// Perform nested set operations?
		$nestedset = ($this->findQuerySection(BITOWL_DB_NESTEDSET, $argc, $argv, 0, $section)) != -1;
		$groupby = false;
		$selectImediateChildren = !$nestedset ? -1 : $this->findQuerySection(BITOWL_DB_SELIMEDCHILDREN, $argc, $argv, 1);

		$order = 0;
		$table = $this->prefix.$table;
		$query = '';
		$count_strip = false;
		$where = false;
		// The first argument has special requirements
		switch($action)
		{
			case BITOWL_DB_SELECT:
				$query = 'SELECT ';
				if(($section = $this->findQuerySection(BITOWL_DB_COLUMNLIST, $argc, $argv, 1)) != -1)
					$query .= 't1.`'.implode($argv[$section], '`,t1.`').'`';
				else
					$query .= 't1.*';
				if($nestedset)
				{
					$groupby = true;
					$query .= ', count(t2.id) AS depth';
				}
				if(($section = $this->findQuerySection(BITOWL_DB_PAGINATED, $argc, $argv, 0) != -1))
					$count_strip = strlen($query);
				$query .= " FROM `$table` AS t1";
				if($nestedset)
					$query .= ", `$table` AS t2";
				$order = $this->findQuerySection(BITOWL_DB_RCHRONOLOGICAL, $argc, $argv, 0) != -1 ? 2 : 1;
				break;
			case BITOWL_DB_INSERT:
				$query = "INSERT INTO `$table`";
				$column_section = $this->findQuerySection(BITOWL_DB_COLUMNLIST, $argc, $argv, 1);
				$value_section = $this->findQuerySection(BITOWL_DB_VALUES, $argc, $argv, 1);
				$setsID = false;

				// For nested sets
				if($nestedset)
				{
					$parent = 0;
					if(($section = $this->findQuerySection(BITOWL_DB_SETNSPARENT, $argc, $argv, 1)) != -1)
						$parent = $argv[$section][0];

					$parentInfo = $this->query($table, BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('rgt'), BITOWL_DB_WHERE, array('id', $parent));
					if(is_array($parentInfo))
						$parentInfo = $parentInfo[0];

					$queries[] = "UPDATE `$table` SET rgt = rgt+2 WHERE rgt >= {$parentInfo['rgt']}";
					$queries[] = "UPDATE `$table` SET lft = lft+2 WHERE lft >= {$parentInfo['rgt']}";

					$argv[$column_section][] = 'lft';
					$argv[$column_section][] = 'rgt';
					$argv[$value_section][] = $parentInfo['rgt'];
					$argv[$value_section][] = $parentInfo['rgt']+1;
				}
				if($column_section != -1)
				{
					$addcomma = false;
					for($i = 0;$i < count($argv[$column_section]);$i++)
					{
						if(!is_array($argv[$value_section][$i]))
						{
							if($addcomma)
								$query .= ',';
							else
							{
								$query .= ' (';
								$addcomma = true;
							}

							$query .= '`'.$argv[$column_section][$i].'`';
							if($argv[$column_section][$i] == 'id')
								$setsID = $i;
						}
					}
					if($addcomma)
						$query .= ')';
				}
				$query .= ' VALUES (';
				if($value_section != -1)
				{
					$addcomma = false;
					for($i = 0;$i < count($argv[$value_section]);$i++)
					{
						if($addcomma) // Besides the first add a comma.
							$query .= ',';
						else
							$addcomma = true;

						if(is_array($argv[$value_section][$i]))
						{
							$status = $this->mysql_query('SHOW TABLE STATUS LIKE \''.$table."_{$argv[$column_section][$i]}'");
							$query .= $status['Auto_increment']; // Should be the right value.

							$queries[] = $this->query($table."_{$argv[$column_section][$i]}", BITOWL_DB_INSERT, BITOWL_DB_ROW, $argv[$value_section][$i], BITOWL_DB_RETURNQUERY);
						}
						else
						{
							$query .= $this->getValue($argv[$value_section][$i]);
						}

						if($setsID !== false && $i == $setsID)
						{
							$postqueries[] = "UPDATE `$table` SET id = ".$this->getValue($argv[$value_section][$i]).' WHERE id = LAST_INSERT_ID()';
						}
					}
				}
				else
					die(language('E_MISSINGQUERYSECTION', 'BITOWL_DB_VALUES'));
				$query .= ')';
				break;
			case BITOWL_DB_UPDATE:
				$query = "UPDATE `$table` AS t1 SET ";
				$column_section = $this->findQuerySection(BITOWL_DB_COLUMNLIST, $argc, $argv, 1);
				$value_section = $this->findQuerySection(BITOWL_DB_VALUES, $argc, $argv, 1);
				// Add a where clause for the ID
				$id = -1;
				for($i = 0;$i < count($argv[$column_section]);$i++)
				{
					if($argv[$column_section][$i] == 'id')
					{
						$argv[] = BITOWL_DB_WHERE;
						$argv[] = array($argv[$column_section][$i], $argv[$value_section][$i]);
						$id = (int) $argv[$value_section][$i];
						$argc += 2;
						break;
					}
				}
				// For nested sets who are changing their position in the tree
				if($nestedset && ($section = $this->findQuerySection(BITOWL_DB_SETNSPARENT, $argc, $argv, 1)) != -1)
				{
					$parent = $argv[$section][0];

					$parentInfo = $this->query($table, BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('lft', 'rgt'), BITOWL_DB_WHERE, array('id', $parent), BITOWL_DB_WHERE, array('id', $id, 'multioperator' => BITOWL_DB_WHERE_OR));
					// Figure out which is which.
					if($parentInfo[0]['id'] == $id)
					{
						$nodeInfo = $parentInfo[0];
						$parentInfo = $parentInfo[1];
					}
					else
					{
						$nodeInfo = $parentInfo[1];
						$parentInfo = $parentInfo[0];
					}
					$nodeSize = $nodeInfo['rgt'] - $nodeInfo['lft'] + 1;
					$moveLeft = $parentInfo['lft'] - ($parentInfo['lft'] > $nodeInfo['lft'] ? $nodeSize : 0);
					$moveRight = $parentInfo['rgt'] - ($parentInfo['rgt'] > $nodeInfo['rgt'] ? $nodeSize : 0);
					$newOffset = $parentInfo['rgt'] - $nodeInfo['rgt'] - 1 + ($parentInfo['rgt'] > $nodeInfo['rgt'] ? 0 : $nodeSize);

					$queries[] = "UPDATE `$table` SET lft = -lft, rgt = -rgt WHERE lft >= {$nodeInfo['lft']} AND  rgt <= {$nodeInfo['rgt']}";
					$queries[] = "UPDATE `$table` SET lft = lft-$nodeSize WHERE lft > {$nodeInfo['lft']}";
					$queries[] = "UPDATE `$table` SET rgt = rgt-$nodeSize WHERE rgt > {$nodeInfo['rgt']}";
					$queries[] = "UPDATE `$table` SET lft = lft+$nodeSize WHERE lft > $moveLeft";
					$queries[] = "UPDATE `$table` SET rgt = rgt+$nodeSize WHERE rgt >= $moveRight";
					$queries[] = "UPDATE `$table` SET lft = -lft+($newOffset), rgt = -rgt+($newOffset) WHERE lft <= -{$nodeInfo['lft']} AND rgt >= -{$nodeInfo['rgt']}";
				}
				for($i = 0;$i < count($argv[$column_section]);$i++)
				{
					if($i != 0)
						$query .= ',';

					if(!is_array($argv[$value_section][$i]))
					{
						$query .= "t1.`{$argv[$column_section][$i]}`=".$this->getValue($argv[$value_section][$i]);
					}
					else
						$queries[] = $this->query($table."_{$argv[$column_section][$i]}", BITOWL_DB_UPDATE, BITOWL_DB_ROW, $argv[$value_section][$i], BITOWL_DB_RETURNQUERY);
				}
				break;
			case BITOWL_DB_DELETE:
				$query = "DELETE t1 FROM `$table` AS t1";

				if($this->findQuerySection(BITOWL_DB_WHERE, $argc, $argv, 1) == -1)
				{
					// For delete we need to add a where clause if the row is passed in.
					$column_section = $this->findQuerySection(BITOWL_DB_COLUMNLIST, $argc, $argv, 1);
					$value_section = $this->findQuerySection(BITOWL_DB_VALUES, $argc, $argv, 1);
					if($column_section == -1 || $value_section == -1)
						die(language('E_MISSINGQUERYSECTION', 'BITOWL_DB_COLUMNLIST, BITOWL_DB_VALUES, BITOWL_DB_WHERE'));
					for($i = 0;$i < count($argv[$column_section]);$i++)
					{
						if(!is_array($argv[$value_section][$i]))
						{
							$argv[] = BITOWL_DB_WHERE;
							$argv[] = array($argv[$column_section][$i], $argv[$value_section][$i]);
							$argc += 2;
						}
						else
							$queries[] = $this->query($table."_{$argv[$column_section][$i]}", BITOWL_DB_DELETE, BITOWL_DB_WHERE, array('id', $argv[$value_section][$i]));
					}
				}

				if($nestedset)
				{
					$id = -1;
					// Check if a row was provided
					for($i = 0;$i < count($argv[$column_section]);$i++)
					{
						if($argv[$column_section][$i] == 'id')
						{
							$id = (int) $argv[$value_section][$i];
							break;
						}
					}
					if($id == -1)
					{
						// Also check in the WHERE clauses.
						while(($section = $this->findQuerySection(BITOWL_DB_WHERE, $argc, $argv, 1, $section)) != -1)
						{
							if($argv[$section][0] == 'id')
							{
								$id = (int) $argv[$section][1];
								break;
							}
						}
					}
					if($id != -1)
					{
						$rowInfo = $this->query($table, BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('lft', 'rgt'), BITOWL_DB_WHERE, array('id', $id));
						$queries[] = "UPDATE `$table` SET rgt = rgt-1, lft = lft-1 WHERE lft BETWEEN {$rowInfo[0]['lft']} AND {$rowInfo[0]['rgt']}";
						$queries[] = "UPDATE `$table` SET rgt = rgt-2 WHERE rgt > {$rowInfo[0]['rgt']}";
						$queries[] = "UPDATE `$table` SET lft = lft-2 WHERE lft > {$rowInfo[0]['rgt']}";
					}
				}
				break;
			default:
				die(language('E_INVALIDACTION'));
		}

		if($selectImediateChildren != -1)
		{
			$query .= ", (SELECT t1.lft, t1.rgt FROM `$table` AS t1";
		}

		$section = 0;
		while(($section = $this->findQuerySection(BITOWL_DB_WHERE, $argc, $argv, 1, $section)) != -1)
		{
			$operator = '=';
			if(isset($argv[$section]['operator']))
			{
				if($argv[$section]['operator'] == BITOWL_DB_WHERE_BITWISEAND)
					$operator = '&';
				elseif($argv[$section]['operator'] == BITOWL_DB_WHERE_LESSTHAN)
					$operator = '<';
			}

			$query .= (!$where ? ' WHERE ' : (!isset($argv[$section]['multioperator']) || $argv[$section]['multioperator'] != BITOWL_DB_WHERE_OR ? ' AND ' : ' OR ')).(isset($argv[$section]['parent']) && $argv[$section]['parent'] && $selectImediateChildren == -1 ? 't2.`' : 't1.`');
			$query .= $argv[$section][0].'`'.$operator;

			$value = $argv[$section][1];
			$query .= $this->getValue($value);

			$where = true;
		}
		while(($section = $this->findQuerySection(BITOWL_DB_SEARCH, $argc, $argv, 1, $section)) != -1)
		{
			$query .= (!$where ? ' WHERE MATCH(t1.`' : ' AND MATCH(t1.`').implode('`,`', $argv[$section][0]).'`) AGAINST ("'.addslashes($argv[$section][1]).'" IN BOOLEAN MODE)';
			$where = true;
		}
		if($nestedset && ($section = $this->findQuerySection(BITOWL_DB_SELECTCHILDREN, $argc, $argv, 1, $section)) != -1)
		{
			$query .= (!$where ? ' WHERE ' : ' AND ').'t1.lft > t2.lft AND t1.rgt < t2.rgt';
			$where = true;
		}
		elseif($nestedset && ($section = $this->findQuerySection(BITOWL_DB_SELECTPARENTS, $argc, $argv, 1, $section)) != -1)
		{
			$query .= (!$where ? ' WHERE ' : ' AND ').'t1.lft <= t2.lft AND t1.rgt >= t2.rgt';
			$where = true;
		}

		if($nestedset && $groupby)
		{
			$query .= ' GROUP BY t1.id';
		}
		if($order)
		{
			$query .= ' ORDER BY '.($nestedset ? 't1.lft ' : 't1.id ').($order == 1 ? 'ASC' : 'DESC');
		}

		if($selectImediateChildren != -1)
		{
			$query .= ') AS t3 WHERE t1.lft BETWEEN t2.lft AND t2.rgt AND t2.lft  > t3.lft AND t2.rgt < t3.rgt GROUP BY t1.id HAVING count(t2.id)=1';
		}

		if($count_strip !== false)
		{
			$count_query = 'SELECT count(t1.id)'.substr($query, $count_strip);
			$result = $this->mysql_query($count_query);
			if($result === false)
			{
				if(DATABASE_DEBUG_MODE)
				{
					echo 'Errno: '.$this->connection->errno.'<br />Error: '.$this->connection->error.'<br />';
				}
				die(language('E_QUERY'));
			}
			$data = $result->fetch_row();
			$this->totalResults = $data[0];
			$result->free();
		}

		if(($section = $this->findQuerySection(BITOWL_DB_LIMIT, $argc, $argv, 1)) != -1)
		{
			if(!is_numeric($argv[$section][0]) || !is_numeric($argv[$section][1]))
				die(language('E_QUERY'));
			$query .= " LIMIT {$argv[$section][0]},{$argv[$section][1]}";
		}

		$queries[] = $query;
		$queries = array_merge($queries, $postqueries);
		// Return the query if requested
		if($argv[$argc-1] === BITOWL_DB_RETURNQUERY)
			return implode($queries, ';');

		// Otherwise execute it.
		$return = array();
		foreach($queries as $query)
		{
			$result = $this->mysql_query($query);
			if($result === false) // Bad query
			{
				die(language('E_QUERY'));
			}
			elseif($result !== true) // Now we only need to do this if a resource was returned.
			{
				while($data = $result->fetch_assoc())
				{
					/*foreach($data as $key => $value)
					{
						if($this->checkTablelist($table.'_'.$key))
						{
							$data2 = $this->query($table.'_'.$key, BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $value));
							$data[$key] = $data2[0];
						}
					}*/
					$return[] = $data;
				}
				$result->free();
			}
		}
		return $return;
	}
	public function getName()
	{
		return language('MYSQL');
	}
	public function getSize()
	{
		$size = 0;
		$query = 'SHOW TABLE STATUS';
		$result = $this->mysql_query($query) or die(language('E_QUERY'));
		while($data = $result->fetch_assoc())
		{
			$size += $data['Data_length'];
		}
		$result->free();
		return $size;
	}
	private function getColumnDefinition($type)
	{
		$def = '';
		switch($type)
		{
			default:
			case BITOWL_DB_CREATETABLE_BOOL:
				$def .= 'tinyint(1)';
				break;
			case BITOWL_DB_CREATETABLE_INT:
				$def .= 'int(10)';
				break;
			case BITOWL_DB_CREATETABLE_STRING:
				$def .= 'varchar(255)';
				break;
			case BITOWL_DB_CREATETABLE_TEXT:
				$def .= 'text';
				break;
		} 
		return $def.' NOT NULL';
	}
	public function createTable($table, $flags)
	{
		$argc = func_num_args();
		$argv = func_get_args();
		if($argc < 2)
			return;

		$query = 'CREATE TABLE `'.$this->prefix.$table.'` (`id` int(10) unsigned NOT NULL auto_increment';
		for($i = 2;$i < $argc;$i++)
		{
			$query .= ', `'.$argv[$i]['name'].'` '.$this->getColumnDefinition($argv[$i]['type']);
		}
		if($flags & BITOWL_DB_CREATETABLE_NESTEDSET)
		{
			$query .= ', `lft` int(10), `rgt` int(10)';
		}
		$query .= ', PRIMARY KEY(`id`))';
		$this->mysql_query($query) or die(language('E_QUERY'));
	}

	public function modifyTable($table, $action, $column, $after='', $type=NULL, $default='')
	{
		$query = 'ALTER TABLE `'.$this->prefix.$table.'` ';
		switch($action)
		{
			default:
				if(is_null($type) || empty($after))
					die(language('E_QUERY'));
				$query .= 'ADD COLUMN `'.$column.'` '.$this->getColumnDefinition($type).
					' DEFAULT '.$this->getValue($default).' AFTER `'.$after.'`';
				break;
			case BITOWL_DB_MODIFYTABLE_DELETE:
				$query .= 'DROP COLUMN `'.$column.'`';
				break;
		}
		$this->mysql_query($query) or die(language('E_QUERY'));
	}
}
?>
