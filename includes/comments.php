<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

// This file contains functions to view threads of comments on the user's
// website.

class BitOwl_Comments
{
	private $thread;
	private $root = NULL;
	private $count = 0;

	public function __construct($thread, $countOnly, $replysubjet='', $handlePost=true)
	{
		global $_bitowl;

		if(get_magic_quotes_gpc() != 0) // Remove magic quotes from comment fields
		{
			foreach($_POST as $key => &$var)
			{
				if(strpos($key, 'comment_') !== 0)
					continue;
				if(is_string($var))
					$var = stripslashes($var);
			}
		}

		$this->thread = $thread;

		removeFromQuery('reply');
		// We'll use the date of the new post to give it a new anchor.
		$markPost = 0;
		$_bitowl['templates']->variables['replysubject'] = $replysubject;
		if($handlePost)
		{
			if(isset($_POST['comment_subject']))
			{
				$_bitowl['templates']->variables['commenterror'] = NULL;
				$markPost = $this->postComment(
					$_POST['comment_reply'],
					$_POST['comment_commenter'],
					$_POST['comment_email'],
					$_POST['comment_subject'],
					$_POST['comment_comment'],
					$_bitowl['templates']->variables['commenterror']
				);
			}
		}

		$posts = array();
		if(!$countOnly)
			$posts = $_bitowl['database']->query('comments', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('thread', $this->thread), BITOWL_DB_PAGINATED);
		else
			$posts = $_bitowl['database']->query('comments', BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('id', 'thread', 'parent'), BITOWL_DB_WHERE, array('thread', $this->thread), BITOWL_DB_PAGINATED);
		$this->count = $_bitowl['database']->getTotalResults();
		if(!$countOnly && count($posts) > 0)
		{
			foreach($posts as &$post)
				$post['comment'] = parseBBCode($post['comment']);
			$this->root = $this->sortPosts($posts, 0, $markPost);
		}
	}

	public function display($template)
	{
		global $_bitowl, $loggedinuser;

		$_bitowl['templates']->variables['captcha'] = $loggedinuser == false ?
			captcha_get_html() : '';

		$_bitowl['templates']->variables['comments'] = &$this->root;
		showTemplate($template);
	}

	public function getCount() { return $this->count; }
	public function getThreadId() { return $this->thread; }

	// Sort posts into a linked list
	private function sortPosts(&$posts, $parent, $markPost)
	{
		global $_bitowl;

		// Dummy nodes so that I don't need to do...while in the template engine.
		$root = array('next' => NULL);
		$node = &$root;
		for($i = 0;$i < count($posts);$i++)
		{
			if($posts[$i]['parent'] == $parent)
			{
				$node['next'] = $posts[$i];
				$node = &$node['next'];

				// Remove this element from the array so that it doesn't get
				// scanned.
				array_splice($posts, $i--, 1);

				// Check if this is a new post
				if($markPost == $node['date'])
					$node['new'] = true;

				// Convert the date to string.
				$node['date'] = date($_bitowl['config']->setting['system']['dateformat'], $node['date']);

				$node['next'] = NULL;
				$node['sub'] = $this->sortPosts($posts, $node['id'], $markPost);
				// Check for array('next' => NULL) and change it to just NULL
				if(is_null($node['sub']['next']))
					$node['sub'] = NULL;

				// Clean up a little
				unset($node['parent'], $node['thread']);
			}
		}
		return $root;
	}

	public function postComment($parent, $name, $email, $subject, $comment, &$error)
	{
		global $_bitowl, $loggedinuser;

		if($this->thread == -1) // Get a thread number
		{
			$this->thread = $_bitowl['config']->setting['system']['nextthreadid']++;
			$_bitowl['config']->save();
		}

		$error = NULL;
		if($loggedinuser)
		{
			$name = $loggedinuser['username'];
			$email = $loggedinuser['email'];
			$captcha_check = true;
		}
		else
			$captcha_check = captcha_check_answer();

		if(empty($name) || empty($email) || empty($subject) || empty($comment))
			$error = language('E_REQUIRED');
		elseif($captcha_check !== true)
			$error = language('E_INVALIDCAPTCHA');
		elseif(!$loggedinuser && user_exists($name))
			$error = language('E_USERNAMETAKEN');
		elseif(!isValidEmailAddress($email))
			$error = language('E_INVALIDEMAIL');

		if($error == NULL)
		{
			$comment = array(
				'thread' => $this->thread, 
				'parent' => (int) $parent,
				'date' => time(),
				'ip' => $_SERVER['REMOTE_ADDR'],
				'author' => $name,
				'email' => $email,
				'subject' => $subject,
				'comment' => $comment
			);
			$_bitowl['database']->query('comments', BITOWL_DB_INSERT, BITOWL_DB_ROW, $comment);
			return $comment['date'];
		}
		return 0;
	}
}
?>
