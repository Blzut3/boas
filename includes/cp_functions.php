<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

//functions related to admin cp operations
function readpermissions(&$user, $permission_list)
{
	$permissions = explode(',', $user['permissions']);
	foreach($permissions as $permission)
	{
		$user[$permission] = true;
	}

	foreach($permission_list as $permission)
	{
		// Founders have full permissions
		if(isset($user['perm_founder']) && $user['perm_founder'])
		{
			$user[$permission] = true;
		}
		elseif(!isset($user[$permission]))
		{
			$user[$permission] = false;
		}
	}
}

function readablesize($size)
{
	if($size >= 1073741824)
		return round($size/1073741824, 2).' GiB';
	if($size >= 1048576)
		return round($size/1048576, 2).' MiB';
	elseif($size >= 1024)
		return round($size/1024, 2).' KiB';
	return $size.' B';
}

function readablefilesize($file)
{
	return readablesize(filesize($file));
}

// Shorten a string.
function calculateShortStory($story, $length=255, $parseBBCode=true)
{
	$out = '';
	if($parseBBCode)
		$paragraphs = explode("\n", trim(parseBBCode($story, false, true)));
	else
		$paragraphs = explode("\n", trim($story));
	foreach($paragraphs as $paragraph)
	{
		$out .= $paragraph;
		if(strlen($out) > $length)
			break;
	}

	$targetLength = $length - ($parseBBCode ? 12 : 8);
	if(strlen($out) > $targetLength && strlen($out) != $length)
	{
		while(($pos = strrpos($out, ' ')) !== false && strlen($out) > $targetLength)
		{
			$out = substr($out, 0, $pos);
		}
		$lastChar = $out[strlen($out)-1];
		if($lastChar != '.' && $lastChar != '!' && $lastChar != '.')
			$out .= '&hellip;';
		if($parseBBCode)
			$out .= '</p>';
	}

	return $out;
}

function manageComments($thread)
{
	global $db, $config;

	if(isset($_POST['comment_delete']))
	{
		// Delete all selected comments and their children.
		$params = array('comments', BITOWL_DB_DELETE);
		$params_children = array('comments', BITOWL_DB_DELETE);
		foreach($_POST['comment_delete'] as $key => $delete)
		{
			$params[] = BITOWL_DB_WHERE;
			$params[] = array('id', $key, 'multioperator' => BITOWL_DB_WHERE_OR);
			$params_children[] = BITOWL_DB_WHERE;
			$params_children[] = array('parent', $key, 'multioperator' => BITOWL_DB_WHERE_OR);
		}
		call_user_func_array(array($db, 'query'), $params);
		call_user_func_array(array($db, 'query'), $params_children);
		unset($_GET['page']); // Reset pagination.
	}

	$form = new BitOwl_Form();

	$pagination = new BitOwl_Pagination(STANDARD_PAGE_SIZE);
	if($thread == -1)
	{
		$comments = NULL;
		$pagination->setTotal(0);
	}
	else
	{
		$comments = $db->query('comments', BITOWL_DB_SELECT, BITOWL_DB_PAGINATED,
			BITOWL_DB_COLUMNLIST, array('id', 'date', 'author', 'subject'),
			BITOWL_DB_WHERE, array('thread', (int) $thread),
			BITOWL_DB_LIMIT, array($pagination->start(), STANDARD_PAGE_SIZE)
		);
		$pagination->setTotal($db->getTotalResults());
	}

	$tableData = array();
	if(is_array($comments))
	{
		foreach($comments as $comment)
		{
			$tableData[] = array(
				new BitOwl_FormWidget(BitOwl_FormWidget::CHECKBOX, '', 'comment_delete['.$comment['id'].']'),
				$comment['subject'],
				$comment['author'],
				date($config->setting['system']['dateformat'], $comment['date']),
				$comment['ip']
			);
		}
	}

	$group = $form->newGroup(language('MANAGECOMMENTS'));
	$group->newTable(array(language('DELETE'), language('SUBJECT'), language('AUTHOR'), language('DATE'), language('IP')), $tableData);
	$group->newWidget(BitOwl_FormWidget::PAGINATION, NULL, NULL, $pagination);

	$form->printForm();
}

// Generates a thumbnail for an image.  This will try to fill the thumbnail
// dimensions while keeping the aspect ratio from the original.  That is the
// original image will be centered in the window and resized so one dimension
// fills the image.
//
// Returns the filename of the new image.
define('ALIGNMENT_TOP_LEFT', 0);
define('ALIGNMENT_CENTER', 1);
define('ALIGNMENT_BOTTOM_RIGHT', 2);
function generateImageThumbnail($original, $width, $height, $alignment=ALIGNMENT_CENTER, $outfile=NULL, $orig_uploaded=true)
{
	$img = imagecreatefromstring(file_get_contents($original));
	$dest = imagecreatetruecolor($width, $height);

	$x = 0;
	$y = 0;
	$w = imagesx($img);
	$h = imagesy($img);
	$orig_aspect = $w/$h;
	$new_aspect = $width/$height;
	if($orig_aspect > $new_aspect) // Original wider than new.
	{
		switch($alignment)
		{
			default:
				$x = $w/2 - ($h*$new_aspect)/2;
				break;
			case ALIGNMENT_TOP_LEFT:
				$x = 0;
				break;
			case ALIGNMENT_BOTTOM_RIGHT:
				$x = $w-($h*$new_aspect);
				break;
		}
		$w = $h*$new_aspect;
	}
	elseif($orig_aspect < $new_aspect) // New wider than original.
	{
		switch($alignment)
		{
			default:
				$y = $h/2 - ($w/$new_aspect)/2;
				break;
			case ALIGNMENT_TOP_LEFT:
				$y = 0;
				break;
			case ALIGNMENT_BOTTOM_RIGHT:
				$y = $h-($w/$new_aspect);
				break;
		}
		$h = $w/$new_aspect;
	}

	$out = $outfile !== NULL ? $outfile : BitOwl_Upload::makeFilename('thumb-'.($orig_uploaded ? BitOwl_Upload::stripFilename($original) : $original));
	imagecopyresampled($dest, $img, 0, 0, $x, $y, $width, $height, $w, $h);
	imagepng($dest, FILES_DIR.$out, 9, PNG_ALL_FILTERS);
	imagedestroy($dest);
	imagedestroy($img);
	return $out;
}

function watermarkImage($file)
{
	global $config;
	if(!$config->setting['system']['watermark']['enabled'] || empty($config->setting['system']['watermark']['image']))
		return;

	$ispng = false;
	if(($img = @imagecreatefrompng($file)) !== false)
		$ispng = true;
	else
		$img = imagecreatefromstring(file_get_contents($file));

	$wm = imagecreatefromstring(file_get_contents(FILES_DIR.$config->setting['system']['watermark']['image']));
	$imgDimensions = array(imagesx($img), imagesy($img));
	$wmDimensions = array(imagesx($wm), imagesy($wm));

	// Create output buffer which is initialized to a transparent canvas.
	$out = imagecreatetruecolor($imgDimensions[0], $imgDimensions[1]);
	imagefill($out, 0, 0, imagecolorallocatealpha($out, 255, 255, 255, 127));
	imagecopy($out, $img, 0, 0, 0, 0, $imgDimensions[0], $imgDimensions[1]);

	// In order to determine if we need to scale the watermark we will be using the length of the diagonals.
	$imgSize = sqrt($imgDimensions[0]*$imgDimensions[0] + $imgDimensions[1]*$imgDimensions[1]);
	$wmSize = sqrt($wmDimensions[0]*$wmDimensions[0] + $wmDimensions[1]*$wmDimensions[1]);
	$maxSize = $config->setting['system']['watermark']['size']/100;
	$scale = 1;
	if($wmSize > $maxSize*$imgSize)
		$scale = ($maxSize*$imgSize)/$wmSize;

	$x = 0;
	$y = 0;
	$w = $scale*$wmDimensions[0];
	$h = $scale*$wmDimensions[1];
	// Adjust X coordinate
	switch($config->setting['system']['watermark']['position'])
	{
		default:
		case 0: // Top Left
		case 3: // Left
		case 6: // Bottom Left
			break;
		case 1: // Top Center
		case 4: // Center
		case 7: // Bottom Center
			$x = $imgDimensions[0]/2 - $w;
			break;
		case 2: // Top Right
		case 5: // Right
		case 8: // Bottom Right
			$x = $imgDimensions[0] - $w;
			break;
	}
	// Adjust Y coordinate
	switch($config->setting['system']['watermark']['position'])
	{
		default:
		case 0: // Top Left
		case 1: // Top
		case 2: // Top Right:
			break;
		case 3: // Left Center
		case 4: // Center
		case 5: // Right Center
			$y = $imgDimensions[1]/2 - $h;
			break;
		case 6: // Bottom Left
		case 7: // Bottom Center
		case 8: // Bottom Right
			$y = $imgDimensions[1] - $h;
			break;
	}

	imagecopyresampled($out, $wm, $x, $y, 0, 0, $w, $h, $wmDimensions[0], $wmDimensions[1]);
	if($ispng)
		imagepng($out, $file, 9, PNG_ALL_FILTERS);
	else
		imagejpeg($out, $file, 85);
}
?>
