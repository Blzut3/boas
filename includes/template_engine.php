<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

/* TemplateEngine class contains all functions related to compiling and
 * displaying templates.  The filename of a compiled template is the same as
 * the source only with a .php appended to the filename.
 */
class BitOwl_TemplateEngine
{
	const VAR_PATTERN = '[a-zA-Z0-9_.\\(\\)]';
	const TEMPLATE_PATTERN = '[a-zA-Z0-9_.\\(\\)\\/\\-]';
	const BOOLEAN_PATTERN = '([a-zA-Z0-9&_.|\\(\\)!=><+\\-*\\/%" ]*)';
	
	/* Variable that holds all of the variables accessable by the template.
	 */
	public $variables = array('ROOT_COMMENT_THREAD' => -1, 'temp' => array());

	/* Compiles a template file for later use, the filename should be the same
	 * as the input, only having ".php" appended to the end of the filename.
	 */
	private function compile($template_file) //converts the template to php
	{
		$temp = file_get_contents($template_file);
		$output = fopen($template_file.'.php', 'w'); //Create/Erase file
		fwrite($output, "");
		fclose($output);
		$output = fopen($template_file.'.php', 'a');
		while(strpos($temp, '<!--') !== false) //go though each block, writing any html located before it.
		{
			if(strpos($temp, '-->') === false)
			{
				die(language('T_UNCLOSED'));
			}
			$html = substr($temp, 0, strpos($temp, '<!--')); //write any html before this block
			$html = $this->parseHTML($html); //find any variables in the html parts
			fwrite($output, $html);
			$code = substr($temp, strpos($temp, '<!--')+4, strpos($temp, '-->')-strpos($temp, '<!--')-4);
			if($code[0] != '-') // Ignore this section if there is an extra -
			{
				//$code to php
				$code = $this->covertCode($code);
				fwrite($output, '<?php '.$code.'?>');
			}
			else
				fwrite($output, '<!--'.$code.'-->');
			$temp = substr($temp, strpos($temp, '-->')+3); //cut the input for next loop
		}
		fwrite($output, $this->parseHTML($temp)); //write the rest of the file
		fclose($output);
	}
	/* Converts template variables stored in the HTML sections of the template.
	 * These are encloded in braces.  Parentheses are used to get the value of
	 * another variable.
	 */
	private function parseHTML($html)
	{
		/*while(preg_match("/\{(".self::VAR_PATTERN."+?)\}/", $html, &$arg))
		{
			$html = preg_replace("/\{(".self::VAR_PATTERN."+?)\}/", '<?php echo '.$this->getVariable($arg[1]).'; ?>', $html, 1);
		}*/
		preg_match_all('/\\{('.self::VAR_PATTERN.'+?)\\}/', $html, $arg, PREG_OFFSET_CAPTURE);
		$replace_shift = 0;
		foreach($arg[1] as &$match)
		{
			$var = $this->getVariable($match[0]);
			if($var[0] == '$')
			{
				$html = substr_replace($html, '<?php if(isset('.$var.')) echo '.$var.'; ?>', $match[1] + $replace_shift - 1, strlen($match[0])+2);
				$replace_shift += strlen('<?php if(isset('.$var.')) echo '.$var.'; ?>') - (strlen($match[0]) + 2);
			}
			else
			{
				$html = substr_replace($html, '<?php echo '.$var.'; ?>', $match[1] + $replace_shift - 1, strlen($match[0])+2);
				$replace_shift += strlen('<?php echo '.$var.'; ?>') - (strlen($match[0]) + 2);
			}
		}
		return $html;
	}
	/* Coverts code enclosed within HTML comment tags to PHP.
	 */
	private function covertCode($code)
	{
		$code = str_replace(';', "\n", $code); //semicolons allow for multiple statements per line
		$code = explode("\n", $code);
		foreach($code as &$line)
		{
			if(strpos($line, '//') !== false) //comments
			{
				$line = substr($line, 0, strpos($line, '//'));
			}
			if(strpos($line, 'if(') === false && strpos($line, 'for(') === false)
			{
				$line = str_replace(array(' ', '	', "\n", "\r"), '', $line);
			}
			else
			{
				$line = trim($line);
				$line = str_replace(array(' ', '	', "\n", "\r"), '', substr($line, 0, strpos($line, '('))).substr($line, strpos($line, '('));
			}
			if(empty($line))
			{
				continue;
			}
			if($line === 'end')
			{
				$line = '}';
				continue;
			}
			elseif($line === 'break')
			{
				$line = 'break;';
				continue;
			}
			elseif($line === 'continue')
			{
				$line = 'continue;';
				continue;
			}
			elseif($line === 'else')
			{
				$line = 'else{';
				continue;
			}
			if(preg_match("/elseif\(".self::BOOLEAN_PATTERN."\)/i", $line, $results)) //elseif statement
			{
				$bool_expression = $this->validateIf(trim($results[1]));
				$line = "elseif($bool_expression){";
			}
			elseif(preg_match("/if\(".self::BOOLEAN_PATTERN."\)/i", $line, $results)) //if statement, contains security checks
			{
				$bool_expression = $this->validateIf(trim($results[1]));
				$line = "if($bool_expression){";
			}
			elseif(preg_match("/foreach\((".self::VAR_PATTERN."+?),(".self::VAR_PATTERN."+?)\)/i", $line)) //foreach loop
			{
				$args = explode(",", preg_replace("/foreach\((".self::VAR_PATTERN."+?),(".self::VAR_PATTERN."+?)\)/i", "$1,$2", $line));
				$args[0] = $this->getVariable($args[0]);
				$args[1] = $this->getVariable($args[1]);
				$line = 'if(isset('.$args[0].') && count('.$args[0].') > 0)foreach('.$args[0].' as &'.$args[1].'){';
			}
			elseif(preg_match("/for\((".self::VAR_PATTERN."+?),".self::BOOLEAN_PATTERN.",([0-9\-]+?)\)/i", $line)) //foreach loop with counter
			{
				$args = explode(';', preg_replace("/for\((".self::VAR_PATTERN."+?),".self::BOOLEAN_PATTERN.",([0-9\-]+?)\)/i", "$1;$2;$3", $line));
				$args[0] = $this->getVariable($args[0]);
				$args[1] = $this->validateIf(trim($args[1]));
				if(!ctype_digit($args[2]))
				{
					die(language('T_ARG3NOTNUMBERIC', $arg[2]).': '.$line);
				}
				$line = 'for('.$args[0].' = 0;'.$args[1].';'.$args[0].' += '.(int) $args[2].'){';
			}
			elseif(preg_match("/include\(\"(".self::TEMPLATE_PATTERN."+)\"\)/i", $line)) //include another template
			{
				$temp = preg_replace("/include\(\"(".self::TEMPLATE_PATTERN."+)\"\)/i", "$1", $line);
				$line = 'showTemplate("'.$temp.'", true);';
			}
			//set the value of a variable, unfortunately, this isn't vary fancy. (You can set the value to an integer)
			elseif(preg_match("/set\((".self::VAR_PATTERN."+?),([0-9.\-]+?)\)/i", $line))
			{
				$temp = explode(',', preg_replace("/set\((".self::VAR_PATTERN."+?),([0-9.\-]+?)\)/i", "$1,$2", $line));
				$temp[0] = $this->getVariable($temp[0]);
				$line = $temp[0].' = '.$temp[1].';';
			}
			elseif(preg_match("/if([n]?)def\((".self::VAR_PATTERN."+?)\)/i", $line))
			{
				$temp = explode(',', preg_replace("/if([n]?)def\((".self::VAR_PATTERN."+?)\)/i", "$1,$2", $line));
				$line = 'if(';
				if($temp[0] === 'n')
					$line .= '!';
				$line .= 'isset('.$this->getVariable($temp[1]).')){';
			}
			elseif(preg_match('/print\\(('.self::VAR_PATTERN.'+)\\)/i', $line, $matches))
			{
				$var = $this->getVariable($matches[1]);
				if($var[0] == '$')
					$line = 'if(isset('.$this->getVariable($matches[1]).')) echo '.$this->getVariable($matches[1]).';';
				else
					$line = 'echo '.$this->getVariable($matches[1]).';';
			}
			elseif(preg_match('/showPagination\\(('.self::VAR_PATTERN.'+?)\\)/i', $line, $matches))
			{
				$line = $this->getVariable($matches[1]).'->show();';
			}
			elseif(preg_match('/forlinkedlist\(('.self::VAR_PATTERN.'+?),('.self::VAR_PATTERN.'+?)\)/i', $line))
			{
				$temp = explode(';', preg_replace('/forlinkedlist\(('.self::VAR_PATTERN.'+?),('.self::VAR_PATTERN.'+?)\)/i', "$1;$2;$3", $line));
				$line = $this->getVariable($temp[1]).'=&'.$this->getVariable($temp[0]).';'.
					'while(!is_null('.$this->getVariable($temp[1]).'=&'.$this->getVariable($temp[1]).'[\'next\'])) {';
			}
			elseif(preg_match('/recurse\(('.self::VAR_PATTERN.'+?),('.self::VAR_PATTERN.'+?),('.self::VAR_PATTERN.'+?)\)/i', $line))
			{
				$temp = explode(',', preg_replace('/recurse\(('.self::VAR_PATTERN.'+?),('.self::VAR_PATTERN.'+?),('.self::VAR_PATTERN.'+?)\)/i', "$1,$2,$3", $line));
				$line = '$this->variables[\'temp\'][]='.$this->getVariable($temp[0]).';'.
					$this->getVariable($temp[1]).'=&'.$this->getVariable($temp[2]).';include __FILE__;'.
					$this->getVariable($temp[0]).'=array_pop($this->variables[\'temp\']);';
			}
			else
			{
				die(language('T_UNKOWN').': '.$line);
			}
		}
		return implode('', $code);
	}
	/* Checks the If statements for anything not allowed.
	 */
	private function validateIf($bool_expression)
	{
		$parts = explode(' ', $bool_expression);
		foreach($parts as &$part)
		{
			if(strpos('(', $part) === 0) //temporary variable to test with out any parentheses
			{
				$thispart = substr($part, 1);
			}
			elseif(strpos(')', $part) === strlen($part)-1)
			{
				$thispart = substr($part, 0, -1);
			}
			else
			{
				$thispart = $part;
			}
			if(($thispart === '>' || $thispart === '<' || $thispart === '/' || $thispart === '*' || $thispart === '+' || $thispart === '-' || $thispart === '%' || $thispart == '|' || $thispart == '&') && strlen($thispart) == 1)
			{
				continue;
			}
			elseif(($thispart === '!=' || $thispart === '==' || $thispart === '<=' || $thispart === '>=' || $thispart === 'or' || $thispart === '||' || $thispart === '&&') && strlen($thispart) == 2)
			{
				continue;
			}
			elseif(($thispart === 'and' || $thispart === 'xor' || $thispart === '===' || $thispart === '!==') && strlen($thispart) == 3)
			{
				continue;
			}
			elseif(preg_match("/([0-9.\-]+)/", $thispart, $matches) && strlen($matches[1]) == strlen($part)) //numberic
			{
				continue;
			}
			elseif(preg_match("/\"(".substr(self::VAR_PATTERN, 0, -1)." ]+)\"/", $thispart, $matches) && strlen($matches[1]) == strlen($part)-2) //string
			{
				continue;
			}
			elseif(preg_match("/([!]?)((isset\\(|empty\\(|is_numeric\\(|is_string\\(|is_array\\(|is_null\\(|count\\()?)(".self::VAR_PATTERN."+)/i", $thispart, $variable))
			{
				// Now see if we matched one of our allowed functions
				if(!empty($variable[2]))
				{
					// Make sure we end with a ')'
					if($variable[4][strlen($variable[4])-1] != ')')
						die(language('T_ILLEGALBOOL')." ($thispart): $boolean_expression");
					$part = preg_replace("/([!]?)((isset\\(|empty\\(|is_numeric\\(|is_string\\(|is_array\\(|is_null\\(|count\\()?)(".self::VAR_PATTERN."+)/i", "$1".$variable[2].$this->getVariable(substr($variable[4],0,-1)).')', $part, 1);
				}
				else
					$part = preg_replace("/([!]?)(".self::VAR_PATTERN."+)/i", "$1".$this->getVariable($variable[4]), $part, 1);
				continue;
			}
			else
			{
				die(language('T_ILLEGALBOOL')." ($thispart): $boolean_expression");
			}
		}
		return implode(' ', $parts);
	}
	/* Converts a variable name used within templates to one used by the
	 * template engine.  For example "i" would turn into $this->variables["i"]
	 * and tabs.system would turn into $this->variables["tabs"]["system"].
	 * enclosing one of the names in parenteses means to get the value of that
	 * variable, for example tabs.(i) would output $this->variables["tabs"][$this->variables["i"]].
	 */
	private function getVariable($var)
	{
		global $config;

		$var = preg_split('/(\\((?:.*?)\\)+)|(?:\\.{1,1})/', $var, -1, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY);
		if(strtolower($var[0]) == 'system') //System variable is designed to reach to config values.
		{
			$result = '$_bitowl[\'config\']->setting';
			unset($var[0]);
		}
		elseif(strtolower($var[0]) == 'get')
		{
			$result = '$_GET';
			unset($var[0]);
		}
		elseif(strtolower($var[0]) == 'post')
		{
			$result = '$_POST';
			unset($var[0]);
		}
		elseif(strtolower($var[0]) == 'language' && count($var) == 2) //language is vary special it requires 1 sub variable
		{
			if($config->setting['system']['performance']['dynamiclanguage'] || (isset($_bitowl) && $_bitowl['config']->setting['system']['performance']['dynamiclanguage']))
				$result = 'language(\''.$var[1].'\')';
			else
				$result = '\''.str_replace('\'', '\\\'', language($var[1])).'\'';
			return $result;
		}
		else
		{
			$result = '$this->variables';
		}
		foreach($var as &$key)
		{
			if(preg_match("/\((".self::VAR_PATTERN."+?)\)/", $key, $variable))
			{
				$key = preg_replace("/\((".self::VAR_PATTERN."+?)\)/", $this->getVariable($variable[1]), $key);
				$result .= "[$key]";
			}
			else
			{
				$result .= "['$key']";
			}
		}
		return $result;
	}
	/* Includes a template, it will check to see if it needs to be (re)compiled.
	 */
	public function template($template_file) //compiles if needed
	{
		global $config, $_bitowl;
		if((isset($config) && $config->setting['system']['performance']['checkcache']) || (isset($_bitowl) && ($_bitowl['config']->setting['system']['performance']['checkcache'])))
		{
			if(!file_exists($template_file.'.php') || filemtime($template_file) > filemtime($template_file.'.php'))
			{
				$this->compile($template_file);
			}
		}
		else
		{ // If we are not checking the cache all the time we still need to make sure the file exists
			if(!file_exists($template_file.'.php'))
			{
				$this->compile($template_file);
			}
		}
		if(isset($config))
		{
			$_bitowl['config'] = $config;
		}
		include $template_file.'.php';
	}
	/* Removes all compiled template files.  (These all end in .php)
	 * This does not require a instance of the template engine to be created.
	 */
	public static function clearCache($template_dir)
	{
		if($dir = opendir($template_dir))
		{
			while(($file = readdir($dir)) !== false)
			{
				if(strpos($file, '.php') !== false)
					unlink($template_dir.'/'.$file);
			}
			closedir($dir);
		}
	}
	/* Adds a variable to be used by templates.
	 */
	public function addVariable($var)
	{
		$keys = array_keys($var);
		foreach($keys as $key)
		{
			$this->variables[$key] = $var[$key];
		}
	}

	/* Returns the currently initialized instance of the template engine.
	 */
	protected static $stdInstance = NULL;
	public static function getStandardInstance()
	{
		if(self::$stdInstance == NULL)
			self::$stdInstance = new BitOwl_TemplateEngine();
		return self::$stdInstance;
	}
}
?>