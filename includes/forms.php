<?php
//*****************************************************************************
//
//	Copyright (C) 2010  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

class BitOwl_Form
{
	const BTN_SUBMIT=0x1;
	const BTN_EDIT=0x2;
	const BTN_UPLOAD=0x4;
	const BTN_RESET=0x8;
	const BTN_DEFAULT=0x9; // BTN_SUBMIT|BTN_RESET
	const BTN_PREVIEW=0x10;

	protected $formTarget;
	protected $groups = array();
	protected $buttons = 0;
	protected $messages = array();

	public function __construct($buttons=self::BTN_DEFAULT, $location=NULL)
	{
		// By default submit data to the page we are on.
		if(!is_string($location))
			$this->formTarget = '?'.$_SERVER['QUERY_STRING'];
		else
			$this->formTarget = $location;

		$this->buttons = $buttons;
	}

	public function addMessage($message)
	{
		if($message != NULL)
			$this->messages[] = $message;
	}

	protected function endForm($newButtons, $reset)
	{
		$te = BitOwl_TemplateEngine::getStandardInstance();
		$te->variables['endform'] = !$reset;
		$te->variables['buttons'] = $this->buttons;
		$te->template('templates/cp/form_end.html');

		$this->buttons = $newButtons;
	}

	public function &newGroup($name, $css=NULL)
	{
		$group = new BitOwl_FormGroup($name, $css);
		$this->groups[] = &$group;
		return $group;
	}

	public function printForm($newButtons=self::BTN_DEFAULT, $reset=false)
	{
		$te = BitOwl_TemplateEngine::getStandardInstance();
		$te->variables['action'] = $this->formTarget;
		$te->variables['messages'] = &$this->messages;
		$te->template('templates/cp/form_start.html');

		foreach($this->groups as &$group)
			$group->printGroup();

		$this->groups = array();
		$this->endForm($newButtons, $reset);
	}
}

class BitOwl_FormGroup
{
	protected $css;
	protected $name;
	protected $widgets = array();

	public function __construct($name, $css=NULL)
	{
		$this->css = $css;
		$this->name = $name;
	}

	public function &newTable($header, $rows=NULL)
	{
		$widget = new BitOwl_FormTable($header, $rows);
		$this->widgets[] = &$widget;
		return $widget;
	}

	public function &newWidget($type, $label, $name=NULL, $value=NULL, $options=array())
	{
		$widget = new BitOwl_FormWidget($type, $label, $name, $value, $options);
		$this->widgets[] = &$widget;
		return $widget;
	}

	public function printGroup()
	{
		global $template_engine;
		$template_engine->variables['group']['css'] = $this->css;
		$template_engine->variables['group']['name'] = $this->name;
		$template_engine->template('templates/cp/group_start.html');

		foreach($this->widgets as &$widget)
			$widget->printWidget();

		$template_engine->template('templates/cp/group_end.html');
	}
}

class BitOwl_FormWidget
{
	protected static $id=0;

	const LABEL=0;
	const TEXT=1;
	const MULTILINE=2;
	const STORYBOX=3;
	const COMBOBOX=4;
	const CHECKBOX=5;
	const UPLOAD=6;
	const TABLE=7;
	const PAGINATION=8;
	const PASSWORD=9;
	const HIDDEN=10;
	const SELECTION=11;
	const LONGTEXT=12;

	protected $type;
	protected $label;
	protected $name;
	protected $value;
	protected $options;

	public function __construct($type, $label, $name=NULL, $value=NULL, $options=array())
	{
		$this->type = $type;
		$this->label = $label;
		$this->name = $name;
		$this->value = $value;
		$this->options = $options;
	}

	protected function getWidgetArray()
	{
		return array(
			'id' => self::$id++,
			'type' => $this->type,
			'label' => $this->label,
			'name' => $this->name,
			'value' => $this->value,
			'options' => $this->options
		);
	}

	public function printWidget()
	{
		global $template_engine;
		$template_engine->variables['inTable'] = false;
		$template_engine->variables['widget'] = $this->getWidgetArray();
		$template_engine->template('templates/cp/widget.html');
	}
}

class BitOwl_FormTable extends BitOwl_FormWidget
{
	public function __construct($header, $rows=NULL)
	{
		$this->value = $header;
		$this->options = $rows;
	}

	public function addRow($row)
	{
		$this->options[] = $row;
	}

	public function printWidget()
	{
		global $template_engine;
		$template_engine->variables['inTable'] = true;
		$tableCells = array();
		foreach($this->options as $option)
		{
			$row = array();
			foreach($option as $cell)
			{
				if(!is_string($cell) && get_class($cell) == 'BitOwl_FormWidget')
					$row[] = $cell->getWidgetArray();
				else
					$row[] = (string) $cell;
			}
			$tableCells[] = $row;
		}
		$template_engine->variables['tableWidget'] = array(
			'header' => $this->value,
			'rows' => &$tableCells
		);
		$template_engine->template('templates/cp/table.html');
	}
}

class BitOwl_Upload
{
	const UPLOAD_BAD=0;
	const UPLOAD_OK=1;
	const UPLOAD_TOOBIG=2;

	protected $name;
	protected $type;
	protected $size;
	protected $tmp_name;
	protected $error;
	protected $status=self::UPLOAD_OK;
	protected $destination=NULL;

	public function __construct($file, $index=-1)
	{
		if($index == -1)
		{
			$this->name = $_FILES[$file]['name'];
			$this->type = $_FILES[$file]['type'];
			$this->size = $_FILES[$file]['size'];
			$this->tmp_name = $_FILES[$file]['tmp_name'];
			$this->error = $_FILES[$file]['error'];
		}
		else
		{
			$this->name = $_FILES[$file]['name'][$index];
			$this->type = $_FILES[$file]['type'][$index];
			$this->size = $_FILES[$file]['size'][$index];
			$this->tmp_name = $_FILES[$file]['tmp_name'][$index];
			$this->error = $_FILES[$file]['error'][$index];
		}
	}

	public function delete()
	{
		unlink(FILES_DIR.$this->getDestination());
	}

	public function getDestination() { return $this->destination; }
	public function getName() { return is_null($this->destination) ? $this->name : self::stripFilename($this->destination); }
	public function getStatus() { return $this->status; }
	public function getType() { return $this->type; }
	public function isUploaded() { return $this->error == UPLOAD_ERR_OK && $this->status == self::UPLOAD_OK; }

	public static function stripFilename($file)
	{
		return substr($file, strpos($file, '-')+1, -4);
	}

	public static function makeFilename($file)
	{
		$this_user = BitOwl_Session::getSession()->getUser();
		$file = str_replace(array(' ', '\\', '/', '\'', '"'), array('_', '', '', '', ''), $file);
		$filename = time()."-$file.upl";
		$path = $this_user['username'];
		if(is_dir(FILES_DIR.$this_user['username']))
		{
			if(is_writable(FILES_DIR.$this_user['username']))
				$path .= '/';
		}
		elseif(mkdir(FILES_DIR.$this_user['username']))
			$path .= '/';
		else
			$path .= '_';
		return $path.$filename; // upl extension is to prevent code injections
	}

	public function upload($destination=NULL, $maxsize=0)
	{
		if(is_null($destination))
			$destination = $this->name;

		$destination = self::makeFilename(stripslashes($destination));
		if($maxsize != 0 && $this->size > $maxsize)
			$this->status = self::UPLOAD_TOOBIG;
		elseif(move_uploaded_file($this->tmp_name, FILES_DIR.$destination))
			$this->status = self::UPLOAD_OK;
		$this->status = self::UPLOAD_BAD;

		$this->destination = $destination;
		return $destination;
	}
}
?>
