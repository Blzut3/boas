<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

function parseBBCode($string, $attachment_callback=false, $strip=false)
{
	if(!$strip)
	{
		$codes = array(
			//array(pattern, replace)
			array('/(?:\\[b\\](.*?)\\[\\/b\\])/', '<b>$1</b>'),
			array('/(?:\\[i\\](.*?)\\[\\/i\\])/', '<i>$1</i>'),
			array('/(?:\\[u\\](.*?)\\[\\/u\\])/', '<ins>$1</ins>'),
			array('/(?:\\[s\\](.*?)\\[\\/s\\])/', '<del>$1</del>'),
			array('/(?:\\[img\\](.*?)\\[\\/img\\])/', '<img src="$1" />'),
			array('/(?:\\[url\\](.*?)\\[\\/url\\])/', '<a href="$1">$1</a>'),
			array('/(?:\\[url=(.*?)\\](.*?)\\[\\/url\\])/', '<a href="$1">$2</a>'),
			array('/(?:\\[youtube\\](.*?)\\[\\/youtube\\])/', '<object width="480" height="385"><param name="movie" value="http://www.youtube.com/v/$1&amp;hl=en_US&amp;fs=1?rel=0&amp;color1=0x3a3a3a&amp;color2=0x999999"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/$1&amp;hl=en_US&amp;fs=1?rel=0&amp;color1=0x3a3a3a&amp;color2=0x999999" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="480" height="385"></embed></object>'),
			array('/(?:\\[break\\])/', '&nbsp;')
		);
	}
	else
	{
		$codes = array(
			//array(pattern, replace)
			array('/(?:\\[b\\](.*?)\\[\\/b\\])/', '$1'),
			array('/(?:\\[i\\](.*?)\\[\\/i\\])/', '$1'),
			array('/(?:\\[u\\](.*?)\\[\\/u\\])/', '$1'),
			array('/(?:\\[s\\](.*?)\\[\\/s\\])/', '$1'),
			array('/(?:\\[img\\](.*?)\\[\\/img\\])/', ''),
			array('/(?:\\[url\\](.*?)\\[\\/url\\])/', '$1'),
			array('/(?:\\[url=(.*?)\\](.*?)\\[\\/url\\])/', '$2'),
			array('/(?:\\[youtube\\](.*?)\\[\\/youtube\\])/', ''),
			array('/(?:\\[attachment=([0-9]*)\\](.*?)\\[\\/attachment\\])/', ''),
			array('/(?:\\[break\\])/', '')
		);
	}

	foreach($codes as $code)
		$string = preg_replace($code[0], $code[1], $string);

	// Handle lists
	$list_pattern = '/(?:\\[list((?:=1){0,1})\\](.*?)\\[\\/list\\])/s';
	if(preg_match_all($list_pattern, $string, $matches, PREG_SET_ORDER) > 0)
	{
		foreach($matches as $match)
		{
			$tag = $match[1] == '=1' ? 'ol' : 'ul';
			$items = explode('[*]', $match[2]);
			unset($items[0]); // Invalid index.
			$replace = "<$tag><li>".implode('</li><li>',$items)."</li></$tag>";
			$string = preg_replace($list_pattern, $replace, $string, 1);
		}
	}

	$paragraphs = explode("\n", $string);
	if(!is_array($paragraphs))
		$paragraphs = array($string);
	if($attachment_callback !== false)
	{
		$attachment_pattern = '/(?:\\[attachment=([0-9]*)\\](.*?)\\[\\/attachment\\])/';
		foreach($paragraphs as &$paragraph)
		{
			// Clear the output variable.
			$attachments = '';
			$matches = array();
			if(preg_match_all($attachment_pattern, $paragraph, $matches, PREG_SET_ORDER) > 0)
			{
				$paragraph = preg_replace($attachment_pattern, '', $paragraph);
	
				foreach($matches as $match)
					$attachments .= call_user_func($attachment_callback, (int) $match[1], $match[2]);
			}
			$paragraph = trim($paragraph);
			if(strpos($paragraph, '<ul>') === 0 || strpos($paragraph, '<li>') === 0)
				$paragraph = $attachments.$paragraph;
			else
				$paragraph = $attachments.'<p>'.$paragraph.'</p>';
		}
	}
	$string = str_replace('<p></p>', '', implode("\n", $paragraphs));

	return $string;
}
?>
