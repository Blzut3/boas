<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

//functions that can be used anywhere in the script.
function convertID($table, $id) //coverts a static ID to a dynamic id.
{
	$id = (int) $id; //Be sure we are using an integer value.
	foreach($table as $row => $tablerow)
	{
		if($tablerow['id'] == $id)
		{
			return $row;
		}
	}
	return false;
}

function initCache($path, $clear=false) //creates a list of view(something).php files to be included.
{
	if($clear)
	{
		$cache = fopen($path.'/initcache.php', 'w');
		fclose($cache);
	}
	if($dir = opendir($path))
	{
		$cache = fopen($path.'/initcache.php', 'a');
		fwrite($cache, "<?php\n");
		while(($file = readdir($dir)) !== false)
		{
			if(strpos($file, 'view') === 0 && strpos($file, '.php') !== false)
			{
				fwrite($cache, 'include $script_path.\'/'.$file."';\n");
			}
		}
		fwrite($cache, '?>');
		fclose($cache);
	}
}

// Returns true if the string is a vailid E-Mail address.
function isValidEMailAddress($address)
{
	// Check for a "normal" address first
	// ex. username@example.com
	if(strcmp(preg_replace("/([a-zA-Z0-9\.!#\$%\/?|^()`~&'+\\\\=_\-]+)@([a-zA-Z0-9\\\\.\-]+)/", "", $address), "") != 0)
	{
		// Now quoted names are allowed but discouraged.  We should check anyways:
		// ex. "User Name"@example.com
		if(strcmp(preg_replace("/\"(.+?)\"@([a-zA-Z0-9\\\\.\-]+)/", "", $address), "") != 0)
			return false; //No match
	}
	return true;
}

// Hopefully this will allow for uniform usage of htmlentities.
function entities_encode($str)
{
	return htmlentities($str, ENT_COMPAT, 'UTF-8');
}

//functions below this comment are for the end user's web site.
////////////////////////////////////////////////////////////////////////////////
function showTemplate($template, $fullpath=false) //show a template based on the config.
{
	global $_bitowl, $config, $script_path;
	$te = BitOwl_TemplateEngine::getStandardInstance();
	$prefix = isset($config) ? '' : $script_path.'/';
	if(!$fullpath)
		$prefix .= 'templates/' . (isset($config) ? 'cp/' : $_bitowl['config']->setting['system']['template'].'/');
	$te->template($prefix.$template);
}

function removeFromQuery($string) //removes string=blah from query string
{
	$te = BitOwl_TemplateEngine::getStandardInstance();
	$str = explode('&amp;', $te->variables['query']);
	$str = array_filter($str);
	for($i = 0;$i < count($str);$i++)
	{
		if(strpos($str[$i], $string) !== false)
		{
			unset($str[$i]);
		}
	}
	$str = implode('&amp;', $str);
	if(strlen($str) != 0)
	{
		$str .= '&amp;';
	}
	$te->variables['query'] = $str;
}

function user_exists($username, &$db) //checks if a username is in use.
{
	global $_bitowl;
	$userlist = $db->query('users', BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('username'));
	foreach($userlist as $user)
	{
		if(strtolower($user['username']) == strtolower($username))
		{
			return true;
		}
	}
	return false;
}

// Pagination functions
class BitOwl_Pagination
{
	private $id;
	private $itemsperpage;
	private $total;
	private $position;

	public function __construct($itemsperpage, $id='')
	{
		$this->id = "page$id";
		$this->itemsperpage = $itemsperpage;
		$this->total = 0;
		$this->position = isset($_GET[$this->id]) ? $_GET[$this->id] : 0;
	}

	public function setTotal($total)
	{
		$this->total = $total;
	}

	public function start()
	{
		return $this->position;
	}

	public function show()
	{
		$template = BitOwl_TemplateEngine::getStandardInstance();

		removeFromQuery($this->id);
		// What page are we on, how many pages are there, and where how far in
		// does our end loop start.
		$currentPage = (int) round($this->position/$this->itemsperpage);
		$totalPages = (int) ceil($this->total/$this->itemsperpage);
		$end_start = max($currentPage, $totalPages-2);

		// Set some variables.  Next and previous will be -1 if it should not
		// exist.
		$template->variables['paginationid'] = $this->id;
		$template->variables['paginationnext'] = $this->position + $this->itemsperpage;
		if($template->variables['paginationnext'] >= $this->total)
			$template->variables['paginationnext'] = -1;
		$template->variables['paginationprevious'] = max(-1, $this->position - $this->itemsperpage);
		$template->variables['paginationpages'] = array();

		// Mid count says how big the array should be after the mid loop.
		$mid_count = 7;

		// Begin loop.  We'll check for the need for a elipse.
		for($i = 0;$i < 2;$i++)
		{
			if($i*$this->itemsperpage >= $this->total)
				break;
			$template->variables['paginationpages'][] = array('start' => $i*$this->itemsperpage, 'number' => $i+1, 'active' => $i == $currentPage);
		}
		if($currentPage > 4 && $totalPages-6 > 2)
		{
			$template->variables['paginationpages'][] = 0;
			$mid_count += 1;
		}

		// mid loop
		for($i = min($totalPages-6, $currentPage-1);$i <= $currentPage+2 || count($template->variables['paginationpages']) < $mid_count;$i++)
		{
			if($i <= 2)
				continue;
			else if($i > $end_start)
				break;
			$template->variables['paginationpages'][] = array('start' => ($i-1)*$this->itemsperpage, 'number' => $i, 'active' => $i - 1 == $currentPage);
		}

		// Check for an end elipse and do the end loop.
		if($currentPage <= $totalPages-6 && $totalPages-6 > 2)
			$template->variables['paginationpages'][] = 0;
		for($i = $end_start;$i < $totalPages;$i++)
		{
			if($i < 2)
				continue;
			$template->variables['paginationpages'][] = array('start' => $i*$this->itemsperpage, 'number' => $i+1, 'active' => $i == $currentPage);
		}

		// Show the paginate template.
		showTemplate('paginate.html');
	}
}
?>
