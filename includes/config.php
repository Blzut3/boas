<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

/* config class creates a nice organised place for all functions related to 
 * the config.
 */
class BitOwl_Config
{
	public $setting;
	private $file;
	private $installed = true;

	function __construct($file)
	{
		$this->file = $file;
		if(file_exists($file))
		{
			include $file;
			$this->setting = $_bitowl;
		}
		else
			$this->installed = false;
	}

	public function isInstalled()
	{
		return $this->installed;
	}

	public function settingDefault($path, $value)
	{
		$exists = true;
		$var = &$this->setting;
		$path = explode('.', $path);
		foreach($path as $part)
		{
			if(!isset($var[$part]))
				$exists = false;
			$var = &$var[$part];
		}
		if(!$exists)
			$var = $value;
	}

	/* Converts the config into strings and then stores it into the config file.
	 */
	public function save()
	{
		$config[0] = "<?php\n";
		$this->write_settings($this->setting, $config);
		$config[] = '?>';
		$write = fopen($this->file, 'w') or die(language('E_OPEN'));
		fwrite($write, implode('', $config));
		fclose($write);
	}
	/* Processes the array of settings to convert them to strings and then send 
	 * back to save(), not to be called alone.
	 */
	private function write_settings($config, &$results, $start_name=array())
	{
		foreach($config as $key => $value)
		{
			$name = $start_name;
			$name[] = $key;
			if(is_array($value))
			{
				$this->write_settings($value, $results, $name);
			}
			else
			{
				$results[] = $this->getSettingString($name, $value);
			}
		}
	}
	/* Takes the name supplied in $name and the value in $setting and creates 
	 * the string, again, not to be called alone.
	 */
	private function getSettingString($name, $setting)
	{
		$return = '$_bitowl';
		foreach($name as $part)
		{
			$return .= '[\''.$part.'\']';
		}
		$return .= ' = ';
		if(is_bool($setting))
		{
			if($setting)
			{
				$return .= 'true';
			}
			else
			{
				$return .= 'false';
			}
		}
		elseif(is_int($setting))
		{
			$return .= $setting;
		}
		else //probably a string
		{
			$return .= '\''.$setting.'\'';
		}
		return $return.";\n";
	}
}
?>