<?php
//*****************************************************************************
//
//	Copyright (C) 2011  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

class BitOwl_Session
{
	const AUTOLOGIN_COOKIE = 'BOAS_ADMIN_AUTOLOGIN';
	const AUTOLOGIN_DURATION = 2678400; //31 days

	protected function __construct()
	{
		session_start();
		if(!isset($_SESSION['user']) || !isset($_SESSION['duration']) ||
			!isset($_SESSION['auth']) || $_SESSION['auth'] != $this->authKey())
		{
			if(isset($_COOKIE['BOAS_ADMIN_AUTOLOGIN']))
			{
				// Login will create a new session
				$info = unserialize(stripslashes($_COOKIE[self::AUTOLOGIN_COOKIE]));
				$this->login($info['username'], $info['password'], true, true);
			}
			else
				$this->end();
		}

		if(time() > $_SESSION['duration'])
			$this->end();
	}

	private function authKey()
	{
		return md5(
			$_SERVER['REMOTE_ADDR'].
			$_SERVER['HTTP_USER_AGENT']
		);
	}

	public function end()
	{
		session_unset();
		session_regenerate_id();
		if(isset($_COOKIE[self::AUTOLOGIN_COOKIE]))
			setcookie(self::AUTOLOGIN_COOKIE, '', time()-3600);
		$_SESSION['user'] = false;
		$_SESSION['duration'] = time()+self::AUTOLOGIN_DURATION;
		$_SESSION['auth'] = $this->authKey();
	}

	public function getUser()
	{
		return $_SESSION['user'];
	}

	public function login($username, $password, $autologin, $hashed=false)
	{
		global $db;
		$this->end();
		if(!empty($username) && !empty($password))
		{
			if(!$hashed)
				$password = md5($password);
			$users = $db->query('users', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('password', $password));
			if(!is_array($users))
				return false;
			foreach($users as $user)
			{
				if(strtolower($user['username']) == strtolower($username))
				{
					$_SESSION['user'] = $user;
					break;
				}
			}

			// Auto login cookie
			if($autologin)
			{
				$info = array('username' => strtolower($username), 'password' => $password);
				setcookie(self::AUTOLOGIN_COOKIE, serialize($info), $_SESSION['duration']);
			}
		}
		return $_SESSION['user'];
	}

	static private $instance = NULL;
	public static function getSession()
	{
		if(is_null(self::$instance))
			self::$instance = new BitOwl_Session();
		return self::$instance;
	}
}

?>
