<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

// Captcha
if($_bitowl['config']->setting['system']['captcha']['enabled'])
{
	require_once $script_path.'/includes/recaptchalib.php';

	function captcha_get_html()
	{
		global $_bitowl;

		return recaptcha_get_html($_bitowl['config']->setting['system']['captcha']['publickey']);
	}
	function captcha_check_answer()
	{
		global $_bitowl;

		$resp = recaptcha_check_answer($_bitowl['config']->setting['system']['captcha']['privatekey'],
			$_SERVER['REMOTE_ADDR'],
			$_POST['recaptcha_challenge_field'],
			$_POST['recaptcha_response_field']
		);

		return $resp->is_valid ? true : $resp->error;
	}
}
else
{
	function captcha_get_html() { return ''; }
	function captcha_check_answer() { return true; }
}
?>
