<?php
if(!defined('bitowl'))
	die();

define('UPLOAD_PREFIX', 'upgrade_'.time().'-');

$config->setting = array(
	'version' => array(
		'main' => '2.0.0',
		'system' => '3.0.0',
		'users' => '2.0.0'
	),
	'system' => array(
		'scripturl' => 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'],
		'template' => 'BitOwl',
		'language' => 'en_us',
		'allownamechange' => false,
		'enablebios' => false,
		'avatarupload' => false,
		'remoteavatar' => false,
		'guestregistration' => false,
		'performance' => array(
			'checkcache' => false,
			'processorintensiveflatfile' => false,
			'dynamiclanguage' => false,
			'enablesitelogin' => false
		),
		'dateformat' => 'F j<\\s\\up>S</\\s\\up> Y H:i',
		'firstlogin' => true,
		'watermark' => array(
			'enabled' => false,
			'position' => 0,
			'size' => 50
		),
		'files' => array(
			'url' => 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'files/',
			'dir' => 'files/'
		)
	)
);

define('FILES_DIR', $config->setting['system']['files']['dir'].'/');

// Start the session.  We'll be using this to store the user's input throughout
// the installation.
function checkDBVariable($type, $var, $default)
{
	if(isset($_SESSION['database']) && $_SESSION['database']['dbtype'] == $type)
		return isset($_SESSION['database'][$var]) ? $_SESSION['database'][$var] : $default;
	return $default;
}

$license = <<<EOGPL
		    GNU GENERAL PUBLIC LICENSE
		       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

			    Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Lesser General Public License instead.)  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

  To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights.
These restrictions translate to certain responsibilities for you if you
distribute copies of the software, or if you modify it.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

  We protect your rights with two steps: (1) copyright the software, and
(2) offer you this license which gives you legal permission to copy,
distribute and/or modify the software.

  Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on, we
want its recipients to know that what they have is not the original, so
that any problems introduced by others will not reflect on the original
authors' reputations.

  Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at all.

  The precise terms and conditions for copying, distribution and
modification follow.

		    GNU GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License applies to any program or other work which contains
a notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law:
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".)  Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the
Program (independent of having been made by running the Program).
Whether that is true depends on what the Program does.

  1. You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a fee.

  2. You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception: if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

  4. You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

  5. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

  6. Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties to
this License.

  7. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  8. If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  9. The Free Software Foundation may publish revised and/or new versions
of the General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and "any
later version", you have the option of following the terms and conditions
either of that version or of any later version published by the Free
Software Foundation.  If the Program does not specify a version number of
this License, you may choose any version ever published by the Free Software
Foundation.

  10. If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the author
to ask for permission.  For software which is copyrighted by the Free
Software Foundation, write to the Free Software Foundation; we sometimes
make exceptions for this.  Our decision will be guided by the two goals
of preserving the free status of all derivatives of our free software and
of promoting the sharing and reuse of software generally.

			    NO WARRANTY

  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

		     END OF TERMS AND CONDITIONS
EOGPL;

$error = NULL;
$username = '';
$email = '';

// Validate the administrator form input.
if(isset($_POST['username']))
{
	if(!empty($_POST['password']) && $_POST['password'] == $_POST['confirm_password'] && !empty($_POST['username']) && !empty($_POST['email']))
	{
		// success, go on to the database page.
		$_SESSION['username'] = $_POST['username'];
		$_SESSION['password'] = md5($_POST['password']);
		$_SESSION['email'] = $_POST['email'];
		$_GET['func'] = 'database';
	}
	else
	{
		$username = $_POST['username'];
		$email = $_POST['email'];
		$error = language('I_INVALIDADMINFORM');
	}
}
elseif(isset($_SESSION['username']))
{
	$username = $_SESSION['username'];
	$email = $_SESSION['email'];
}

$dbtypes = array(array('id' => 0, 'name' => language('FLATFILE')));
$dbforms = array(
	array(
		'name' => language('FLATFILE'),
		'fields' => array(array('name' => 'flatfile_directory', 'value' => checkDBVariable(0, 'dbhost', 'db/'), 'password' => false, 'description' => language('DIRECTORY')))
	)
);

if(extension_loaded('mysqli'))
{
	$dbtypes[] = array('id' => 1, 'name' => language('MYSQL'));
	$dbforms[] = array(
		'name' => language('MYSQL'),
		'fields' => array(
			array('name' => 'mysql_hostname', 'value' => checkDBVariable(1, 'dbhost', ''), 'password' => false, 'description' => language('HOSTNAME')),
			array('name' => 'mysql_port', 'value' => checkDBVariable(1, 'dbport', '3306'), 'password' => false, 'description' => language('PORT')),
			array('name' => 'mysql_username', 'value' => checkDBVariable(1, 'dbuser', ''), 'password' => false, 'description' => language('USERNAME')),
			array('name' => 'mysql_password', 'value' => '', 'password' => true, 'description' => language('PASSWORD')),
			array('name' => 'mysql_database', 'value' => checkDBVariable(1, 'dbbase', ''), 'password' => false, 'description' => language('DATABASE'))
		)
	);
}

$doInstall = false;
$upgrade = false;
// Check the database form.
if(isset($_POST['dbtype']))
{
	$database_valid = false;
	if($_POST['dbtype'] == 0 && !empty($_POST['flatfile_directory']))
	{
		$_SESSION['database']['dbtype'] = 0;
		$_SESSION['database']['dbhost'] = $_POST['flatfile_directory'];
		$_SESSION['database']['dbport'] = false;
		$_SESSION['database']['dbuser'] = false;
		$_SESSION['database']['dbpass'] = false;
		$_SESSION['database']['dbbase'] = false;
		$database_valid = true;
	}
	elseif($_POST['dbtype'] == 1 && !empty($_POST['mysql_hostname']) && !empty($_POST['mysql_username']) && !empty($_POST['mysql_database']))
	{
		$_SESSION['database']['dbtype'] = 1;
		$_SESSION['database']['dbhost'] = $_POST['mysql_hostname'];
		$_SESSION['database']['dbport'] = $_POST['mysql_port'];
		$_SESSION['database']['dbuser'] = $_POST['mysql_username'];
		$_SESSION['database']['dbpass'] = $_POST['mysql_password'];
		$_SESSION['database']['dbbase'] = $_POST['mysql_database'];
		$database_valid = true;
	}
	else
		$error = language('I_INVALIDDATABASE');

	// If the user form wasn't filled out go back to it.
	if(!isset($_SESSION['username']) && !isset($_POST['BOAS_dir']))
		$_GET['func'] = 'main';
	elseif($database_valid)
	{
		$config->setting['system']['dbhost'] = $_SESSION['database']['dbhost'];
		$config->setting['system']['dbport'] = $_SESSION['database']['dbport'];
		$config->setting['system']['dbuser'] = $_SESSION['database']['dbuser'];
		$config->setting['system']['dbpass'] = $_SESSION['database']['dbpass'];
		$config->setting['system']['dbbase'] = $_SESSION['database']['dbbase'];
		$_GET['cp'] = 'progress';
		$_GET['func'] = 'progress';
		$doInstall = true;
	}

	if(isset($_POST['BOAS_dir']) && $database_valid)
	{
		if(is_dir($_POST['BOAS_dir']) && file_exists($_POST['BOAS_dir'].'/config.php') && is_dir($_POST['BOAS_dir'].'/db'))
		{
			$upgrade = $_POST['BOAS_dir'];
			$doInstall = true;
		}
		else
			$error = language('I_NOINSTALL');
	}
}

$dbtype = isset($_SESSION['database']['dbtype']) ? $_SESSION['database']['dbtype'] : 0;

$cp = isset($_GET['cp']) ? $_GET['cp'] : 'license';
$template_engine->variables['cp'] = $cp;
$template_engine->variables['cps'] = array(
	array('name' => language('LICENSE'), 'link' => 'license'),
	array('name' => language('INSTALL'), 'link' => 'install'),
	array('name' => language('UPGRADE'), 'link' => 'upgrade')
);

if($doInstall)
{
		$template_engine->variables['cps'][] = array('name' => language('PROGRESS'), 'link' => 'progress');
}
elseif($cp == 'progress')
{
	// Not installing force back to the install tab.
	$template_engine->variables['cp'] = $cp = 'install';
	$_GET['func'] = 'main';
}

$func = isset($_GET['func']) ? $_GET['func'] : 'main';
$template_engine->variables['func'] = $func;
if($cp == 'install')
	$template_engine->variables['functions'] = array(
		array('name' => language('I_INITSETUP'), 'link' => 'main'),
		array('name' => language('I_DATABASE'), 'link' => 'database')
	);
elseif($cp == 'upgrade')
	$template_engine->variables['functions'] = array(array('name' => language('UPGRADE'), 'link' => 'main'));
elseif($cp == 'progress')
	$template_engine->variables['functions'] = array(array('name' => language('PROGRESS'), 'link' => 'main'));
else
	$template_engine->variables['functions'] = array(array('name' => language('LICENSE'), 'link' => 'main'));

if(!$doInstall)
{
	$template_engine->template('templates/cp/header.html');
	function makeDBForm(&$form)
	{
		global $dbtypes, $dbforms, $template_engine;

		$group = $form->newGroup(language('DATABASE'));
		$group->newWidget(BitOwl_FormWidget::COMBOBOX, language('DATABASETYPE'), 'dbtype', 0, $dbtypes);
		$group->newWidget(BitOwl_FormWidget::LABEL, language('I_DATABASEINSTALLNOTICE'));

		foreach($dbforms as $dbform)
		{
			$group = $form->newGroup($dbform['name']);
			foreach($dbform['fields'] as $field)
				$group->newWidget($field['password'] ? BitOwl_FormWidget::PASSWORD : BitOwl_FormWidget::TEXT, $field['description'], $field['name'], $field['value']);
		}
	}

	$form = new BitOwl_Form($cp != 'license' ? BitOwl_Form::BTN_DEFAULT : 0);
	$form->addMessage($error);
	switch($cp)
	{
		default:
			die();
			break;
		case 'license':
			$group = $form->newGroup(language('LICENSE'));
			$group->newWidget(BitOwl_FormWidget::LABEL, '<textarea disabled="disabled" rows="20" class="wide">'.$license.'</textarea>');
			break;
		case 'install':
			switch($func)
			{
				default:
					die();
					break;
				case 'main':
					$group = $form->newGroup(language('ADMINISTRATOR'));
					$group->newWidget(BitOwl_FormWidget::TEXT, language('USERNAME'), 'username', $username);
					$group->newWidget(BitOwl_FormWidget::PASSWORD, language('PASSWORD'), 'password');
					$group->newWidget(BitOwl_FormWidget::PASSWORD, language('CONFIRMPASSWORD'), 'confirm_password');
					$group->newWidget(BitOwl_FormWidget::TEXT, language('EMAIL'), 'email', $email);
					break;
				case 'database':
					makeDBForm($form);
					break;
			}
			break;
		case 'upgrade':
			$group = $form->newGroup(language('UPGRADE'));
			$group->newWidget(BitOwl_FormWidget::LABEL, language('I_UPGRADE'));
			$group->newWidget(BitOwl_FormWidget::TEXT, language('DIRECTORY'), 'BOAS_dir');
			makeDBForm($form);
			break;
	}
	$form->printForm();
}
else
{
	$form = new BitOwl_Form(0);
	$form->addMessage(language('I_DATABASESETUP'));

	$db = BitOwl_Database::createDatabaseObject($config->setting['system']['dbhost'], $config->setting['system']['dbuser'], $config->setting['system']['dbpass'], $config->setting['system']['dbbase']);

	$db->createTable('users', 0,
		array('name' => 'username', 'type' => BITOWL_DB_CREATETABLE_STRING),
		array('name' => 'realname', 'type' => BITOWL_DB_CREATETABLE_STRING),
		array('name' => 'password', 'type' => BITOWL_DB_CREATETABLE_STRING),
		array('name' => 'email', 'type' => BITOWL_DB_CREATETABLE_STRING),
		array('name' => 'avatar', 'type' => BITOWL_DB_CREATETABLE_STRING),
		array('name' => 'bio', 'type' => BITOWL_DB_CREATETABLE_TEXT),
		array('name' => 'publicemail', 'type' => BITOWL_DB_CREATETABLE_BOOL),
		array('name' => 'permissions', 'type' => BITOWL_DB_CREATETABLE_STRING)
	);

	$db->createTable('comments', 0,
		array('name' => 'thread', 'type' => BITOWL_DB_CREATETABLE_INT),
		array('name' => 'parent', 'type' => BITOWL_DB_CREATETABLE_INT),
		array('name' => 'date', 'type' => BITOWL_DB_CREATETABLE_INT),
		array('name' => 'ip', 'type' => BITOWL_DB_CREATETABLE_STRING),
		array('name' => 'author', 'type' => BITOWL_DB_CREATETABLE_STRING),
		array('name' => 'email', 'type' => BITOWL_DB_CREATETABLE_STRING),
		array('name' => 'subject', 'type' => BITOWL_DB_CREATETABLE_STRING),
		array('name' => 'comment', 'type' => BITOWL_DB_CREATETABLE_TEXT)
	);

	if($upgrade !== false)
	{
		require $upgrade.'/config.php'; 
		$config->setting['system']['scripturl'] = 'http://'.$_bitowl['master']['domain'];
		$config->setting['system']['dateformat'] = $_bitowl['news']['date'];
		$config->setting['journalist']['articlesperpage'] = $_bitowl['news']['itemperpage'];
		$config->setting['gallery']['thumbnailwidth'] = $_bitowl['gallery']['maxthumbwidth'] > 0 ? $_bitowl['gallery']['maxthumbwidth'] : 260;
		$config->setting['gallery']['thumbnailheight'] = $_bitowl['gallery']['maxthumbheight'] > 0 ? $_bitowl['gallery']['maxthumbheight'] : 100;

		$form->addMessage(language('I_UPGRADEDB', 'users.php'));
		require $upgrade.'/db/users.php';
		if(is_array($user))
		{
			$firstUser = true;
			foreach($user as $thisUser)
			{
				$newUser = array(
					'username' => $thisUser[0],
					'password' => $thisUser[1],
					'permissions' => array()
				);
				if($firstUser)
				{
					$newUser['permissions'][] = 'perm_founder';
					$firstUser = false;
				}
				if($thisUser[2][0])
					$newUser['permissions'][] = 'perm_system';
				if($thisUser[2][1])
					$newUser['permissions'][] = 'perm_users';
				if($thisUser[2][2])
					$newUser['permissions'][] = 'perm_journalist_admin,perm_journalist';
				if($thisUser[2][4])
					$newUser['permissions'][] = 'perm_gallery';
				$newUser['permissions'] = implode(',', $newUser['permissions']);
				$db->query('users', BITOWL_DB_INSERT, BITOWL_DB_ROW, $newUser);
			}
		}
		unset($user);

		$form->addMessage(language('I_UPGRADEDB', 'news.php'));
		$db->createTable('categories', 0,
			array('name' => 'name', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'users', 'type' => BITOWL_DB_CREATETABLE_STRING)
		);
		$db->createTable('articles', BITOWL_DB_CREATETABLE_FLIPPED,
			array('name' => 'title', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'author', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'date', 'type' => BITOWL_DB_CREATETABLE_INT),
			array('name' => 'published', 'type' => BITOWL_DB_CREATETABLE_BOOL),
			array('name' => 'category', 'type' => BITOWL_DB_CREATETABLE_INT),
			array('name' => 'shortstory', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'story', 'type' => BITOWL_DB_CREATETABLE_TEXT),
			array('name' => 'attachments', 'type' => BITOWL_DB_CREATETABLE_TEXT),
			array('name' => 'thumbnail', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'comments', 'type' => BITOWL_DB_CREATETABLE_INT)
		);
		require $upgrade.'/db/news.php';
		if(is_array($categories))
		{
			foreach($categories as $category)
			{
				$newCategory = array(
					'id' => $category['id']+1,
					'name' => $category['name']
				);
				$db->query('categories', BITOWL_DB_INSERT, BITOWL_DB_ROW, $newCategory);
			}
		}
		if(is_array($news))
		{
			foreach($news as $article)
			{
				$newsArticle = array(
					'title' => $article['title'],
					'story' => str_replace(array('<br />', '\\\'', '\\"'), array("\n", '\'', '"'), $article['message']),
					'shortstory' => calculateShortStory(str_replace(array('<br />', '\\\'', '\\"'), array("\n", '\'', '"'), $article['message'])),
					'author' => $article['author'],
					'date' => $article['date'],
					'published' => true,
					'category' => 0
				);
				foreach($article['categories'] as $id)
					$newsArticle['category'] |= 1<<($id+1);
				if(!empty($article['attachment']))
				{
					copy($upgrade.'/'.$article['attachment'], 'files/'.UPLOAD_PREFIX.$article['attachment'].'.upl');
					$article['attachments'] = serialize(array('file' => UPLOAD_PREFIX.$article['attachment'].'.upl', 'isimage' => false, 'uploader' => $article['author']));
				}
				$db->query('articles', BITOWL_DB_INSERT, BITOWL_DB_ROW, $newsArticle);
			}
		}
		unset($categories, $news);

		$form->addMessage(language('I_UPGRADEDB', 'feeds.php'));
		require $upgrade.'/feeds/feeds.php';
		$db->createTable('feeds', 0,
			array('name' => 'title', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'link', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'description', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'file', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'categories', 'type' => BITOWL_DB_CREATETABLE_INT)
		);
		if(is_array($feed))
		{
			foreach($feed as $thisFeed)
			{
				$rssFeed = array(
					'title' => $thisFeed['name'],
					'link' => $thisFeed['url'],
					'description' => $thisFeed['description'],
					'file' => $thisFeed['file'],
					'categories' => 0
				);
				foreach($thisFeed['categories'] as $id)
					$rssFeed['categories'] |= 1<<($id+1);
				$db->query('feeds', BITOWL_DB_INSERT, BITOWL_DB_ROW, $rssFeed);
			}
		}
		unset($feed);

		$form->addMessage(language('I_UPGRADEDB', 'albums.php'));
		$db->createTable('albums', BITOWL_DB_CREATETABLE_NESTEDSET,
			array('name' => 'date', 'type' => BITOWL_DB_CREATETABLE_INT),
			array('name' => 'name', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'description', 'type' => BITOWL_DB_CREATETABLE_TEXT),
			array('name' => 'thumbnail', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'images', 'type' => BITOWL_DB_CREATETABLE_TEXT)
		);
		$rootAlbum = array('id' => 0, 'name' => 'Root', 'lft' => 0, 'rgt' => 1, 'depth' => 0);
		$db->query('albums', BITOWL_DB_INSERT, BITOWL_DB_ROW, $rootAlbum);
		require $upgrade.'/db/albums.php';
		if(is_array($album))
		{
			unset($album[0]);
			foreach($album as $thisAlbum)
			{
				$newAlbum = array(
					'id' => $thisAlbum['id'],
					'date' => time(),
					'name' => $thisAlbum['name'],
					'description' => $thisAlbum['description'],
					'images' => array()
				);
				for($i = 0;$i < count($thisAlbum['image']);$i++)
				{
					$file = $thisAlbum['image'][$i][0];
					copy($upgrade.'/images/album'.$thisAlbum['id'].'/'.$file, 'files/'.UPLOAD_PREFIX.$file.'.upl');
					$img = getimagesize('files/'.UPLOAD_PREFIX.$file.'.upl');

					$newAlbum['images'][] = array(
						'file' => UPLOAD_PREFIX.$thisAlbum['image'][$i][0].'.upl',
						'description' => $thisAlbum['image'][$i][1],
						'width' => $img[0],
						'height' => $img[1],
						'title' => calculateShortStory($thisAlbum['image'][$i][1], 64, false),
						'thumbnail' => generateImageThumbnail('files/'.UPLOAD_PREFIX.$file.'.upl', $config->setting['gallery']['thumbnailwidth'], $config->setting['gallery']['thumbnailheight']),
						'thumbwidth' => $config->setting['gallery']['thumbnailwidth'],
						'thumbheight' => $config->setting['gallery']['thumbnailheight']
					);

					if($i == 0)
						$newAlbum['thumbnail'] = $newAlbum['images'][0]['thumbnail'];
				}
				$newAlbum['images'] = serialize($newAlbum['images']);

				$db->query('albums', BITOWL_DB_INSERT, BITOWL_DB_NESTEDSET, BITOWL_DB_ROW, $newAlbum, BITOWL_DB_SETNSPARENT, array($thisAlbum['parent']));
			}
		}
	}
	else
	{
		$form->addMessage(language('I_ADDADMIN'));
		$adminRow = array('username' => $_SESSION['username'], 'password' => $_SESSION['password'], 'email' => $_SESSION['email'], 'permissions' => 'perm_founder,perm_system,perm_users');
		$db->query('users', BITOWL_DB_INSERT, BITOWL_DB_ROW, $adminRow);
	}

	$form->addMessage(language('I_SAVECONFIG'));
	$config->save();

	// We can kill the session now.
	$_SESSION = array();
	if(isset($_COOKIE[session_name()]))
		setcookie(session_name(), '', time()-3600, '/');
	session_destroy();

	$form->addMessage(language('I_SUCCESS'));

	$template_engine->template('templates/cp/header.html');
	$form->printForm();
}
$template_engine->variables['executiontime'] = microtime() - $start_execution;
$template_engine->variables['memoryusage'] = round((memory_get_usage() - $start_memusage)/1024, 2);
$template_engine->template('templates/cp/footer.html');
?>
