<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

define('PLUGIN_VERSION', '2.0.1');

function updatePlugin($oldversion)
{
	global $config, $db;

	if($oldversion == NULL)
	{
		$db->createTable('albums', BITOWL_DB_CREATETABLE_NESTEDSET,
			array('name' => 'date', 'type' => BITOWL_DB_CREATETABLE_INT),
			array('name' => 'name', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'description', 'type' => BITOWL_DB_CREATETABLE_TEXT),
			array('name' => 'thumbnail', 'type' => BITOWL_DB_CREATETABLE_STRING),
			array('name' => 'images', 'type' => BITOWL_DB_CREATETABLE_TEXT)
		);

		$db->query('albums', BITOWL_DB_INSERT, BITOWL_DB_ROW, array('id' => 0, 'date' => time(), 'name' => 'Root', 'description' => 'Root album', 'thumbnail' => '', 'images' => '', 'lft' => 0, 'rgt' => 1));
	}

	$config->setting['version']['gallery'] = PLUGIN_VERSION;
	$config->settingDefault('gallery.thumbnailwidth', 320);
	$config->settingDefault('gallery.thumbnailheight', 200);
	$config->settingDefault('gallery.imagesperpage', 16);
	$config->save();
}

function getFunctions()
{
	return array(
		array('name' => language('F_MAIN'), 'link' => 'main'),
		array('name' => language('F_NEWALBUM'), 'link' => 'newalbum'),
		array('name' => language('F_MANAGEALBUMS'), 'link' => 'albums'),
		array('name' => language('F_CONFIGURE'), 'link' => 'config')
	);
}
?>
