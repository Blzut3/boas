<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_gallery'])
	die();

if(isset($_POST['name']))
{
	$parent = -1;
	if(is_numeric($_POST['parent']) && $_POST['parent'] >= 0)
	{
		$parent = $db->query('albums', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $_POST['parent']));
		if(count($parent) > 0)
			$parent = $_POST['parent'];
	}

	$newalbum = array(
		'date' => time(), // This is modified time so update it every time.
		'name' => entities_encode(stripslashes($_POST['name'])),
		'description' => entities_encode(stripslashes($_POST['description'])),
		'images' => '',
		'thumbnail' => ''
	);

	if(isset($_POST['editing']))
	{
		$editedalbum = $db->query('albums', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $_POST['editing']));
		if(count($editedalbum) > 0)
		{
			$newalbum['id'] = $editedalbum[0]['id'];
			$newalbum['images'] = $editedalbum[0]['images'];
			$newalbum['thumbnail'] = $editedalbum[0]['thumbnail'];
			$db->query('albums', BITOWL_DB_UPDATE, BITOWL_DB_NESTEDSET, BITOWL_DB_SETNSPARENT, array($parent), BITOWL_DB_ROW, $newalbum);
			$template_engine->variables['message'] = language('M_ALBUMEDITED');
		}
	}
	else
	{
		$db->query('albums', BITOWL_DB_INSERT, BITOWL_DB_NESTEDSET, BITOWL_DB_SETNSPARENT, array($parent), BITOWL_DB_ROW, $newalbum);
		$template_engine->variables['message'] = language('M_ALBUMCREATED');
	}

	$template_engine->variables['destination'] = $parent == -1 ? '?cp=gallery&amp;func=albums' : '?cp=gallery&amp;func=albums&amp;parent='.$parent;
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->template('templates/cp/message_confirm.html');
}
else
{
	$editalbum = NULL;
	$parentalbum = NULL;
	if(isset($_GET['edit']) && is_numeric($_GET['edit']))
	{
		$editalbum = $db->query('albums', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $_GET['edit']));
		$parentalbum = $db->query('albums', BITOWL_DB_SELECT, BITOWL_DB_NESTEDSET, BITOWL_DB_SELECTPARENTS, BITOWL_DB_WHERE, array('id', $_GET['edit'], 'parent' => true));
		if(count($editalbum) > 0)
			$editalbum = $editalbum[0];
		else
			$editalbum = NULL;

		if(count($parentalbum) > 1)
			$parentalbum = $parentalbum[count($parentalbum)-2];
		else
			$parentalbum = NULL;
	}

	$albums = $db->query('albums', BITOWL_DB_SELECT, BITOWL_DB_NESTEDSET, BITOWL_DB_SELECTCHILDREN, BITOWL_DB_COLUMNLIST, array('id', 'name'));

	$list = array(array('id' => 0, 'name' => '----------'));
	if(count($albums) > 0)
	{
		$disable = 0xFFFF;
		foreach($albums as &$album)
		{
			$disabled = false;
			if(!$disabled && $editalbum != NULL)
				$disabled = $album['id'] == $editalbum['id'];
			if($album['depth'] > $disable)
				$disabled = true;
			else
				$disable = $disabled ? $album['depth'] : 0xFFFF;

			$list[] = array('id' => $album['id'], 'name' => str_repeat('&nbsp;', $album['depth']*2).$album['name'], 'disabled' => $disabled);
		}
	}

	$form = new BitOwl_Form((is_array($editalbum) ? BitOwl_Form::BTN_EDIT : BitOwl_Form::BTN_SUBMIT)|BitOwl_Form::BTN_RESET);

	$group = $form->newGroup(language('F_NEWALBUM'));
	if(is_array($editalbum))
		$group->newWidget(BitOwl_FormWidget::HIDDEN, NULL, 'editing', $editalbum['id']);
	$group->newWidget(BitOwl_FormWidget::LONGTEXT, language('NAME'), 'name', is_array($editalbum) ? $editalbum['name'] : '');
	$group->newWidget(BitOwl_FormWidget::STORYBOX, language('DESCRIPTION'), 'description', is_array($editalbum) ? $editalbum['description'] : '');
	$group->newWidget(BitOwl_FormWidget::COMBOBOX, language('PARENT'), 'parent', is_array($parentalbum) ? $parentalbum['id'] : 0, $list);

	$form->printForm();
}
?>
