<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_gallery'])
	die();

if(isset($_POST['thumb_width']))
{
	if(is_numeric($_POST['thumb_width']) && is_numeric($_POST['thumb_height']))
	{
		$config->setting['gallery']['thumbnailwidth'] = (int) $_POST['thumb_width'];
		$config->setting['gallery']['thumbnailheight'] = (int) $_POST['thumb_height'];
	}
	$config->setting['gallery']['imagesperpage'] = (int) $_POST['imagesperpage'];
	$config->save();

	$template_engine->variables['destination'] = '?cp=gallery&amp;func=config';
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->variables['message'] = language('M_SETTINGSAPPLIED');
	$template_engine->template('templates/cp/message_confirm.html');
}
else
{
	$form = new BitOwl_Form();

	$group = $form->newGroup(language('DISPLAY'));
	$group->newWidget(BitOwl_FormWidget::TEXT, language('IMAGESPERPAGE'), 'imagesperpage', $config->setting['gallery']['imagesperpage']);
	$group->newWidget(BitOwl_FormWidget::TEXT, language('DEFAULTTHUMBNAILSIZE'), 'thumb_width', $config->setting['gallery']['thumbnailwidth']);
	$group->newWidget(BitOwl_FormWidget::TEXT, '', 'thumb_height', $config->setting['gallery']['thumbnailheight']);

	$form->printForm();
}
?>
