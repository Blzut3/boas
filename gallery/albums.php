<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_gallery'])
	die();

if(isset($_GET['images']) && is_numeric($_GET['images']))
{
	$album = $db->query('albums', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', $_GET['images']));
	if(count($album) > 0)
	{
		// To make life easy
		$album = $album[0];
		$album['images'] = unserialize($album['images']);

		if(isset($_POST['upload']))
		{
			foreach($_POST['upload_description'] as $key => $description)
			{
				$upload = new BitOwl_Upload('upload_file', $key);
				if($upload->isUploaded())
				{
					$upload = new BitOwl_Upload('upload_file', $key);
					$upload->upload();
					$img = getimagesize(FILES_DIR.$upload->getDestination());
					if($img === false) // Not an image so delete it
						$upload->delete();
					else
					{
						$uploaded_file = array('file' => $upload->getDestination(), 'width' => $img[0], 'height' => $img[1], 'description' => $description, 'title' => calculateShortStory($description, 64, false));
						$uploaded_file['thumbnail'] = generateImageThumbnail(FILES_DIR.$uploaded_file['file'], $config->setting['gallery']['thumbnailwidth'], $config->setting['gallery']['thumbnailheight']);
						$uploaded_file['thumbwidth'] = $config->setting['gallery']['thumbnailwidth'];
						$uploaded_file['thumbheight'] = $config->setting['gallery']['thumbnailheight'];
						watermarkImage(FILES_DIR.$uploaded_file['file']);
						$album['images'][] = $uploaded_file;
						if(empty($album['thumbnail']))
							$album['thumbnail'] = $uploaded_file['thumbnail'];
						$album['date'] = time();
					}
				}
			}
			$album['images'] = serialize($album['images']);
			$db->query('albums', BITOWL_DB_UPDATE, BITOWL_DB_ROW, $album);

			$template_engine->variables['destination'] = '?cp=gallery&amp;func=albums&amp;images='.$_GET['images'];
			$template_engine->variables['message_title'] = language('MESSAGE');
			$template_engine->variables['message'] = language('M_IMAGESUPLOADED');
			$template_engine->template('templates/cp/message_confirm.html');
		}
		elseif(isset($_POST['delete']) && is_numeric($_POST['delete']))
		{
			$image = $album['images'][$_POST['delete']];
			if(is_array($image))
			{
				unlink(FILES_DIR.$image['file']);
				unlink(FILES_DIR.$image['thumbnail']);
				array_splice($album['images'], $_POST['delete'], 1);
				// Check if we need to get a new thumbnail.
				if($image['thumbnail'] == $album['thumbnail'])
				{
					if(count($album['images']) == 0)
						$album['thumbnail'] = '';
					else
						$album['thumbnail'] = $album['images'][0]['thumbnail'];
				}
				$album['images'] = serialize($album['images']);
				$db->query('albums', BITOWL_DB_UPDATE, BITOWL_DB_ROW, $album);
			}

			$template_engine->variables['destination'] = '?cp=gallery&amp;func=albums&amp;images='.$_GET['images'];
			$template_engine->variables['message_title'] = language('MESSAGE');
			$template_engine->variables['message'] = language('M_IMAGEDELETED');
			$template_engine->template('templates/cp/message_confirm.html');
		}
		elseif(isset($_GET['delete']) && is_numeric($_GET['delete']))
		{
			$template_engine->variables['post_fields'] = array(
				array('name' => 'delete', 'value' => $_GET['delete'])
			);
			$template_engine->variables['destination'] = '?cp=gallery&amp;func=albums&amp;images='.$_GET['images'];
			$template_engine->variables['message_title'] = language('MESSAGE');
			$template_engine->variables['message'] = language('C_DELETEIMAGE');
			$template_engine->template('templates/cp/message_confirm.html');
		}
		else if(isset($_POST['description']))
		{
			foreach($_POST['description'] as $key => $value)
			{
				if(isset($album['images'][$key]))
				{
					$album['images'][$key]['description'] = $value;
					$album['images'][$key]['title'] = calculateShortStory($value, 64, false);
				}
			}
			$album['images'] = serialize($album['images']);
			$db->query('albums', BITOWL_DB_UPDATE, BITOWL_DB_ROW, $album);
			$template_engine->variables['destination'] = '?cp=gallery&amp;func=albums&amp;images='.$_GET['images'];
			$template_engine->variables['message_title'] = language('MESSAGE');
			$template_engine->variables['message'] = language('M_ALBUMEDITED');
			$template_engine->template('templates/cp/message_confirm.html');
		}
		else
		{
			if(isset($_GET['upload']))
			{
				$form = new BitOwl_Form(BitOwl_Form::BTN_UPLOAD);

				$group = $form->newGroup(language('UPLOADIMAGES'));
				for($i = 0;$i < 5;$i++)
				{
					if($i != 0)
						$group->newWidget(BitOwl_FormWidget::LABEL, '&nbsp;');
					$group->newWidget(BitOwl_FormWidget::UPLOAD, language('UPLOAD'), 'upload_file[]');
					$group->newWidget(BitOwl_FormWidget::TEXT, language('DESCRIPTION'), 'upload_description[]');
				}

				$form->printForm();
			}
			else
			{
				$imageTable = array();
				if(is_array($album['images']))
				{ // Get user friendly file names
					foreach($album['images'] as $id => &$image)
					{
						$imageTable[] = array(
							BitOwl_Upload::stripFilename($image['file']),
							new BitOwl_FormWidget(BitOwl_FormWidget::TEXT, NULL, 'description[]', $image['description']),
							'<a href="?'.$template_engine->variables['query'].'&amp;delete='.$id.'">'.language('DELETE').'</a>'
						);
					}
				}
	
				$form = new BitOwl_Form(BitOwl_Form::BTN_EDIT|BitOwl_Form::BTN_RESET);
	
				$group = $form->newGroup(language('MANAGEALBUMIMAGES'));
				$group->newTable(array(language('NAME'), language('DESCRIPTION'), language('ACTIONS')), $imageTable);
				$group->newWidget(BitOwl_FormWidget::LABEL, '<a href="?'.$template_engine->variables['query'].'&amp;upload=true">'.language('UPLOADIMAGES').'</a>');
	
				$form->printForm();
			}
		}
	}
}
elseif(isset($_POST['delete']) && is_numeric($_POST['delete']))
{
	$db->query('albums', BITOWL_DB_DELETE, BITOWL_DB_NESTEDSET, BITOWL_DB_WHERE, array('id', $_POST['delete']));
	$template_engine->variables['destination'] = '?cp=gallery&amp;func=albums';
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->variables['message'] = language('M_ALBUMDELETED');
	$template_engine->template('templates/cp/message_confirm.html');
}
elseif(isset($_GET['delete']) && is_numeric($_GET['delete']))
{
	$template_engine->variables['post_fields'] = array(
		array('name' => 'delete', 'value' => $_GET['delete'])
	);
	$template_engine->variables['destination'] = '?cp=gallery&amp;func=albums';
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->variables['message'] = language('C_DELETEALBUM');
	$template_engine->template('templates/cp/message_confirm.html');
}
else
{
	$pagination = new BitOwl_Pagination(STANDARD_PAGE_SIZE);
	$parent = isset($_GET['parent']) && is_numeric($_GET['parent']) && $_GET['parent'] >= 0 ? $_GET['parent'] : 0;
	$trace = $db->query('albums', BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('id', 'name'), BITOWL_DB_SELECTPARENTS, BITOWL_DB_NESTEDSET, BITOWL_DB_WHERE, array('id', $parent, 'parent' => true));
	$trace[count($trace)-1]['last'] = true;
	$albums = $db->query('albums', BITOWL_DB_SELECT, BITOWL_DB_SELIMEDCHILDREN, BITOWL_DB_NESTEDSET, BITOWL_DB_RCHRONOLOGICAL, BITOWL_DB_WHERE, array('id', $parent, 'parent' => true), BITOWL_DB_LIMIT, array($pagination->start(), STANDARD_PAGE_SIZE), BITOWL_DB_PAGINATED);

	$traceLabel = '';
	foreach($trace as &$traceStep)
	{
		if(!isset($traceStep['last']))
			$traceLabel .= '<a href="?cp=gallery&amp;func=albums&amp;parent='.$traceStep['id'].'">';
		$traceLabel .= $traceStep['name'];
		if(!isset($traceStep['last']))
			$traceLabel .= '</a> &raquo; ';
	}

	$albumsTable = array();
	if(is_array($albums))
	{
		foreach($albums as &$album)
		{
			$albumsTable[] = array(
				'<a href="?cp=gallery&amp;func=albums&amp;parent='.$album['id'].'">'.$album['name'].'</a>',
				'<a href="?cp=gallery&amp;func=newalbum&amp;edit='.$album['id'].'">'.language('EDIT').'</a> <a href="?cp=gallery&amp;func=albums&amp;images='.$album['id'].'">'.language('IMAGES').'</a> <a href="?cp=gallery&amp;func=albums&amp;delete='.$album['id'].'">'.language('DELETE').'</a>'
			);
		}
	}

	$form = new BitOwl_Form(0);

	$group = $form->newGroup(language('F_MANAGEALBUMS'));
	$group->newWidget(BitOwl_FormWidget::LABEL, $traceLabel);
	$group->newTable(array(language('NAME'), language('ACTIONS')), $albumsTable);
	$group->newWidget(BitOwl_FormWidget::PAGINATION, NULL, NULL, $pagination);
	$group->newWidget(BitOwl_FormWidget::LABEL,
		'<a href="?cp=gallery&amp;func=newalbum">'.language('F_NEWALBUM').'</a>'.
		($parent > 0
			? ' <a href="?cp=gallery&amp;func=newalbum&amp;edit='.$parent.'">'.language('EDIT').'</a> <a href="?cp=gallery&func=albums&images='.$parent.'">'.language('IMAGES').'</a>'
			: ''));

	$form->printForm();
}
?>
