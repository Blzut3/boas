<?php
//*****************************************************************************
//
//	Copyright (C) 2011  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

switch($msg)
{
	case 'C_DELETEIMAGE': return 'Are you sure you want to delete this image?';
	case 'C_DELETEALBUM': return 'Are you sure you want to delete this album? &nbsp;All images will be deleted. &nbsp;Sub-albums will be moved up one level, and will not be automatically deleted.';
	case 'F_MANAGEALBUMS': return 'Manage Albums';
	case 'F_NEWALBUM': return 'New Album';
	case 'IMAGES': return 'Images';
	case 'IMAGESPERPAGE': return 'Images per page';
	case 'M_ALBUMCREATED': return 'The album has been created successfully.';
	case 'M_ALBUMDELETED': return 'The specified album has been deleted successfully.';
	case 'M_ALBUMEDITED': return 'The album has been edited successfully.';
	case 'M_IMAGESUPLOADED': return 'The provided images have been uploaded successfully.';
	case 'M_IMAGEDELETED': return 'The specified image has been successfully deleted.';
	case 'MANAGEALBUMIMAGES': return 'Manage Album Images';
	case 'UPLOADIMAGES': return 'Upload Images';
	default: return false;
}
?>
