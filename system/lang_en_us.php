<?php
//*****************************************************************************
//
//	Copyright (C) 2011  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

switch($msg)
{
	case 'ALLOWGUESTREGISTRATIONS': return 'Enable guest registrations.';
	case 'AVATARSALLOWED': return 'Avatars Allowed';
	case 'C_DELETEFILE': return "Are you sure you want to delete the file at \"$arg1\"? &nbsp;It may still be in use."; //$arg1 is the path to the file.
	case 'CHECKCACHE': return 'Always check template cache.';
	case 'CLEARTEMPLATECACHE': return 'Clear Template Cache';
	case 'DATABASEVERSION': return 'Database Version';
	case 'DATEFORMAT': return 'Date Format';
	case 'DEFAULTTEMPLATE': return 'Default Template';
	case 'DELETEFILE': return 'Delete File';
	case 'DIRECTORY': return 'Directory';
	case 'DYNAMICLANGUAGE': return 'Dynamic Language';
	case 'ENABLESITELOGIN': return 'Enable Site Login';
	case 'F_FILES': return 'Files';
	case 'FILEDELETED': return 'File deleted!';
	case 'FILEMANAGER': return 'File Manager';
	case 'FILESURL': return 'Files URL';
	case 'FILESDIR': return 'Files Directory';
	case 'M_TEMPLATECACHECLEARED': return 'The template cache has been cleared.';
	case 'M_INITREFRESHED': return 'The init cache has been recalculated.';
	case 'PERFORMANCE': return 'Performance';
	case 'PERFORMANCENOTE': return 'These features should be turned off for maximum speed.';
	case 'PROCESSORINTENSIVEFLATFILE': return 'Use more processor when reading database.';
	case 'RECAPTCHAPRIVATE': return 'Recaptcha Private Key';
	case 'RECAPTCHAPUBLIC': return 'Recaptcha Public Key';
	case 'REFRESHINIT': return 'Refresh Init Cache';
	case 'SYSTEMCONFIG': return 'System Configuration';
	case 'UPLOADWATERMARK': return 'Upload Watermark';
	case 'USERNAMECHANGEALLOWED': return 'Username Changes Allowed';
	default: return false;
}
?>
