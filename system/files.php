<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_system'])
{
	die();
}
if(isset($_GET['delete']))
{
	if(strpos($_GET['delete'], '..') !== false || $_GET['delete'] == 'download.php') //security
	{
		die();
	}
	$template_engine->variables['message_title'] = language('DELETEFILE');
	$template_engine->variables['message'] = language('C_DELETEFILE', $_GET['delete']);
	$template_engine->variables['post_fields'] = array(
		array('name' => 'delete', 'value' => $_GET['delete'])
	);
	$template_engine->variables['destination'] = '?cp=system&amp;func=files&amp;directory='.$_GET['directory'];
	$template_engine->template('templates/cp/message_confirm.html');
}
else
{
	if(!isset($_GET['directory']))
		$_GET['directory'] = '';

	$template_engine->variables['message'] = '';
	if(isset($_POST['delete']))
	{
		if(strpos($_GET['delete'], '..') !== false || $_GET['delete'] == 'download.php') //should not be possible though cp
		{
			die();
		}
		if(unlink(FILES_DIR.$_POST['delete']))
		{
			$template_engine->variables['message'] = language('FILEDELETED');
		}
		else
		{
			$template_engine->variables['message'] = language('E_COULDNOTDELETEFILE');
		}
	}
	$directory = $_GET['directory'];
	if(strpos($directory, '..') !== false) //can't go back
	{
		$directory = '';
	}
	if($directory != '' && $directory[strlen($directory)-1] != '/')
		$directory .= '/';
	$list = array();
	if(($dir = opendir(FILES_DIR.$directory)) !== false)
	{
		while($file = readdir($dir))
		{
			if($file == '.') //no need for this
			{
				continue;
			}
			elseif($directory == '' && ($file == '..' || $file == 'download.php' || $file == '.htaccess')) //root doesn't need this
			{
				continue;
			}
			$type = is_file(FILES_DIR.$directory.'/'.$file) ? 'file' : 'directory';
			if($type == 'directory')
			{
				$size = 0;
				if($file != '..') //special handeling for going back a directory
				{
					$url = "?cp=system&amp;func=files&amp;directory=$directory$file";
				}
				else
				{
					$sub_directory = substr($directory, 0, strrpos($directory, '/', -2));
					$url = "?cp=system&amp;func=files&amp;directory=$sub_directory";
				}
			}
			else
			{
				$size = readablefilesize(FILES_DIR.$directory.$file);
				$url = $config->setting['system']['files']['url']."/download.php?file=$directory$file";
			}

			$linkColumn = '<a href="'.$url.'"><img src="templates/cp/images/'.(($type == 'directory') ? 'folder' : 'file').'.gif" alt="" />'.$file.'</a>';
			$list[] = array($linkColumn, $type == 'directory' ? '&nbsp;' : $size, $type != 'directory' ? '<a href="?cp=system&amp;func=files&amp;directory='.htmlentities($_GET['directory']).'&amp;delete='.$directory.$file.'">'.language('DELETE').'</a>' : '&nbsp;');
		}
	}

	$form = new BitOwl_Form(0);

	$group = $form->newGroup(language('FILEMANAGER'));
	$group->newTable(array(language('FILENAME'), language('SIZE'), language('ACTIONS')), $list);

	$form->printForm();
}
?>
