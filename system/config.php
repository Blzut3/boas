<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_system'])
{
	die();
}
if(isset($_POST['language'])) //write config
{
	// Handle file upload.
	$watermark = new BitOwl_Upload('watermark');
	if($watermark->isUploaded())
	{
		$watermark->upload();
		if(getimagesize(FILES_DIR.$watermark->getDestination()) !== false)
			$config->setting['system']['watermark']['image'] = $watermark->getDestination();
		else
			$watermark->delete();
	}

	$config->setting['system']['performance']['checkcache'] = $_POST['checktemplates'] == 'on' ? true : false;
	$config->setting['system']['performance']['dynamiclanguage'] = $_POST['dynamiclanguage'] == 'on' ? true : false;
	$config->setting['system']['performance']['enablesitelogin'] = $_POST['enablesitelogin'] == 'on' ? true : false;
	$config->setting['system']['allownamechange'] = $_POST['allownamechange'] == 'on' ? true : false;
	$config->setting['system']['avatarupload'] = $_POST['avatarupload'] == 'on' ? true : false;
	$config->setting['system']['remoteavatar'] = $_POST['remoteavatar'] == 'on' ? true : false;
	$config->setting['system']['guestregistration'] = $_POST['guestregistration'] == 'on' ? true : false;
	$config->setting['system']['scripturl'] = $_POST['scripturl'];
	$config->setting['system']['language'] = $_POST['language'];
	$config->setting['system']['template'] = $_POST['template'];
	$config->setting['system']['dateformat'] = $_POST['dateformat'];
	$config->setting['system']['enablebios'] = $_POST['enablebios'] == 'on' ? true : false;
	$config->setting['system']['watermark']['enabled'] = $_POST['enablewatermark'] == 'on' ? true : false;
	$config->setting['system']['watermark']['position'] = max(0, min(8, (int) $_POST['watermarkposition']));
	$config->setting['system']['watermark']['size'] = max(1, min(100, (int) $_POST['watermarksize']));
	$config->setting['system']['files']['url'] = $_POST['filesurl'];
	$config->setting['system']['files']['dir'] = $_POST['filesdir'];
	if(file_exists('./includes/recaptchalib.php'))
	{
		$config->setting['system']['captcha']['enabled'] = $_POST['enablecaptcha'] == 'on' ? true : false;
		$config->setting['system']['captcha']['privatekey'] = $_POST['recaptchaprivate'];
		$config->setting['system']['captcha']['publickey'] = $_POST['recaptchapublic'];
	}
	else
	{
		$config->setting['system']['captcha']['enabled'] = false;
		$config->setting['system']['captcha']['privatekey'] = '';
		$config->setting['system']['captcha']['publickey'] = '';
	}
	$config->save();

	$template_engine->variables['destination'] = '?cp=system&amp;func=config';
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->variables['message'] = language('M_SETTINGSAPPLIED');
	$template_engine->template('templates/cp/message_confirm.html');
}
else
{
	$language_dir = opendir('./languages/'); //get a list of languages
	$languages = array();
	while(($file = readdir($language_dir)) !== false)
	{
		if(is_file('./languages/'.$file))
		{
			$language = substr($file, 0, strpos($file, '.'));
			$languages[] = array('id' => $language, 'name' => $language);
		}
	}
	closedir($language_dir);
	$templates_dir = opendir('./templates/'); //list of available templates
	$templates = array();
	while(($file = readdir($templates_dir)) !== false)
	{
		if(!is_file('./templates/'.$file) && $file != 'cp' && $file != '.' && $file != '..') //cp is a special template, should not be used here.
		{
			$templates[] = array('id' => $file, 'name' => $file);
		}
	}
	closedir($templates_dir);

	$form = new BitOwl_Form();

	$group = $form->newGroup(language('SYSTEMCONFIG'));
	$group->newWidget(BitOwl_FormWidget::COMBOBOX, language('LANGUAGE'), 'language', $config->setting['system']['language'], $languages);
	$group->newWidget(BitOwl_FormWidget::COMBOBOX, language('DEFAULTTEMPLATE'), 'template', $config->setting['system']['template'], $templates);
	$group->newWidget(BitOwl_FormWidget::TEXT, language('SCRIPTURL'), 'scripturl', $config->setting['system']['scripturl']);
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('ALLOWGUESTREGISTRATIONS'), 'guestregistration', $config->setting['system']['guestregistration']);
	$group->newWidget(BitOwl_FormWidget::TEXT, language('DATEFORMAT'), 'dateformat', $config->setting['system']['dateformat']);

	$group = $form->newGroup(language('UPLOADS'));
	$group->newWidget(BitOwl_FormWidget::TEXT, language('FILESURL'), 'filesurl', $config->setting['system']['files']['url']);
	$group->newWidget(BitOwl_FormWidget::TEXT, language('FILESDIR'), 'filesdir', $config->setting['system']['files']['dir']);

	$group = $form->newGroup(language('USERSCONFIG'));
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('ALLOWNAMECHANGE'), 'allownamechange', $config->setting['system']['allownamechange']);
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('ALLOWREMOTEAVATAR'), 'remoteavatar', $config->setting['system']['remoteavatar']);
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('ALLOWAVATARUPLOAD'), 'avatarupload', $config->setting['system']['avatarupload']);
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('ENABLEBIOS'), 'enablebios', $config->setting['system']['enablebios']);

	$group = $form->newGroup(language('WATERMARK'));
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('ENABLEWATERMARK'), 'enablewatermark', $config->setting['system']['watermark']['enabled']);
	$group->newWidget(BitOwl_FormWidget::UPLOAD, language('UPLOADWATERMARK'), 'watermark');
	$group->newWidget(BitOwl_FormWidget::COMBOBOX, language('WATERMARKPOSITION'), 'watermarkposition', $config->setting['system']['watermark']['position'],
		array(
			array('id' => 0, 'name' => language('ALIGNMENTTOPLEFT')),
			array('id' => 1, 'name' => language('ALIGNMENTTOP')),
			array('id' => 2, 'name' => language('ALIGNMENTTOPRIGHT')),
			array('id' => 3, 'name' => language('ALIGNMENTLEFT')),
			array('id' => 4, 'name' => language('ALIGNMENTCENTER')),
			array('id' => 5, 'name' => language('ALIGNMENTRIGHT')),
			array('id' => 6, 'name' => language('ALIGNMENTBOTTOMLEFT')),
			array('id' => 7, 'name' => language('ALIGNMENTBOTTOM')),
			array('id' => 8, 'name' => language('ALIGNMENTBOTTOMRIGHT'))
		)
	);
	$group->newWidget(BitOwl_FormWidget::TEXT, language('MAXWATERMARKSIZE'), 'watermarksize', $config->setting['system']['watermark']['size']);

	$group = $form->newGroup(language('PERFORMANCE'));
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('CHECKCACHE'), 'checktemplates', $config->setting['system']['performance']['checkcache']);
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('DYNAMICLANGUAGE'), 'dynamiclanguage', $config->setting['system']['performance']['dynamiclanguage']);
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('ENABLESITELOGIN'), 'enablesitelogin', $config->setting['system']['performance']['enablesitelogin']);
	$group->newWidget(BitOwl_FormWidget::LABEL, language('PERFORMANCENOTE'));

	if(file_exists('./includes/recaptchalib.php'))
	{
		$group = $form->newGroup(language('CAPTCHA'));
		$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('ENABLECAPTCHA'), 'enablecaptcha', $config->setting['system']['captcha']['enabled']);
		$group->newWidget(BitOwl_FormWidget::TEXT, language('RECAPTCHAPRIVATE'), 'recaptchaprivate', $config->setting['system']['captcha']['privatekey']);
		$group->newWidget(BitOwl_FormWidget::TEXT, language('RECAPTCHAPUBLIC'), 'recaptchapublic', $config->setting['system']['captcha']['publickey']);
	}

	$form->printForm();
}
?>
