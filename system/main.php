<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl') || !$this_user['perm_system'])
{
	die();
}
if(isset($_GET['refreshinit']))
{
	initCache('.', true);

	$template_engine->variables['destination'] = '?cp=system';
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->variables['message'] = language('M_INITREFRESHED');
	$template_engine->variables['post_fields'] = array();
	$template_engine->template('templates/cp/message_confirm.html');
}
elseif(isset($_GET['cleartemplatecache']))
{
	// Gather template list
	$templates_dir = opendir('./templates/');
	$templates = array();
	while(($file = readdir($templates_dir)) !== false)
	{
		if(!is_file('./templates/'.$file) && $file != '.' && $file != '..')
		{
			$templates[] = './templates/'.$file;
		}
	}
	closedir($templates_dir);
	foreach($templates as $template)
		BitOwl_TemplateEngine::clearCache($template);

	$template_engine->variables['destination'] = '?cp=system';
	$template_engine->variables['message_title'] = language('MESSAGE');
	$template_engine->variables['message'] = language('M_TEMPLATECACHECLEARED');
	$template_engine->variables['post_fields'] = array();
	$template_engine->template('templates/cp/message_confirm.html');
}
else
{
	$form = new BitOwl_Form(0);

	$group = $form->newGroup(language('DATABASE'));
	$group->newWidget(BitOwl_FormWidget::LABEL, language('DATABASEVERSION'), NULL, $config->setting['version']['db']);
	$group->newWidget(BitOwl_FormWidget::LABEL, language('DATABASETYPE'), NULL, $db->getName());
	$group->newWidget(BitOwl_FormWidget::LABEL, language('SIZE'), NULL, readablesize($db->getsize()));

	$group = $form->newGroup(language('CACHE'));
	$group->newWidget(BitOwl_FormWidget::LABEL, '<a href="?cp=system&amp;refreshinit=true">'.language('REFRESHINIT').'</a>');
	$group->newWidget(BitOwl_FormWidget::LABEL, '<a href="?cp=system&amp;cleartemplatecache=true">'.language('CLEARTEMPLATECACHE').'</a>');

	$form->printForm();
}
?>
