<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl'))
{
	die();
}

$form = new BitOwl_Form(0);

$group = $form->newGroup($this_user['username'].'\'s '.language('PROFILE'));
if(!empty($this_user['realname']))
	$group->newWidget(BitOwl_FormWidget::LABEL, language('REALNAME'), NULL, $this_user['realname']);
$group->newWidget(BitOwl_FormWidget::LABEL, language('EMAIL'), NULL, $this_user['email']);
if($this_user['avatar'] !== '0')
	$group->newWidget(BitOwl_FormWidget::LABEL, language('AVATAR'), NULL, '<img src="'.$config->setting['system']['files']['url'].'/download.php?file='.$this_user['avatar'].'" alt="" />');

$form->printForm();
?>
