<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl'))
{
	die();
}

$updateMessage = NULL;
$uploadMessage = NULL;
$usernameMessage = NULL;
$passwordMessage = NULL;
if(isset($_POST['password']))
{
	$updateMessage = language('PROFILEUPDATED');
	if($config->setting['system']['allownamechange'] && $_POST['username'] != $this_user['username']) //can we change our username?
	{
		$change = true;
		if(strtolower($_POST['username']) != strtolower($this_user['username'])) //allow users to change case
		{
			foreach($users as $user) //be sure no user with the same name exists
			{
				if(strtolower($user['username']) == strtolower($_POST['username']))
				{
					$change = false;
				}
			}
		}
		if($change)
			$this_user['username'] = $_POST['username'];
		else
			$usernameMessage = language('E_USERNAMETAKEN');
	}
	if(isset($_POST['updateavatar']))
	{
		$this_user['avatar'] = update_avatar($_POST['avatar'], 'avatarupload', $this_user['username'], $uploadMessage);
		if(empty($this_user['avatar']))
			$this_user['avatar'] = '0';
	}
	if(!empty($_POST['email']))
	{
		$this_user['email'] = $_POST['email'];
	}
	$this_user['publicemail'] = isset($_POST['publicemail']) ? true : false; //no need to validate
	if(!empty($_POST['password']) || !empty($_POST['password_confirm'])) //do we need to change password? if so check for consistency.
	{
		if($_POST['password'] != $_POST['password_confirm'])
			$passwordMessage = language('E_PASSWORDSDONTMATCH');
		else
			$this_user['password'] = md5($_POST['password']);
	}
	foreach($this_user as $key => $value)
	{
		if(strpos($key, 'perm_') === 0)
		{
			unset($this_user[$key]);
		}
	}
	$this_user['realname'] = $_POST['realname'];
	if($config->setting['system']['enablebios'])
		$this_user['bio'] = entities_encode(stripslashes($_POST['bio']));
	$db->query('users', BITOWL_DB_UPDATE, BITOWL_DB_ROW, $this_user);
}

$form = new BitOwl_Form(BitOwl_Form::BTN_EDIT|BitOwl_Form::BTN_RESET);
if(!$usernameMessage && !$passwordMessage && !$uploadMessage)
	$form->addMessage($updateMessage);
else
{
	$form->addMessage($uploadMessage);
	$form->addMessage($usernameMessage);
	$form->addMessage($passwordMessage);
}

makeUserForm($form, $config->setting['system']['allownamechange'], $this_user);
$form->printForm();
?>