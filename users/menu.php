<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

function getFunctions()
{
	global $this_user;

	$functions = array(
		array('name' => language('F_MAIN'), 'link' => 'main'),
		array('name' => language('F_EDITPROFILE'), 'link' => 'edit'), //edit your settings
	);
	if($this_user['perm_users'] == true)
	{
		$functions[] = array('name' => language('F_MANAGEUSERS'), 'link' => 'users');
	}
	return $functions;
}

//pass user's info to template engine
$template_engine->variables['loggedinuser']['username'] = $this_user['username'];
$template_engine->variables['loggedinuser']['avatar'] = $this_user['avatar'];
$template_engine->variables['loggedinuser']['email'] = $this_user['email'];
$template_engine->variables['loggedinuser']['publicemail'] = $this_user['publicemail'];

function makeUserForm(&$form, $canChangeUsername=true, $user=NULL)
{
	global $config;

	$group = $form->newGroup(language('USERNAME').'/'.language('PASSWORD'));
	if(!$canChangeUsername)
		$group->newWidget(BitOwl_FormWidget::LABEL, language('USERNAME'), NULL, $user != NULL ? $user['username'] : '');
	else
		$group->newWidget(BitOwl_FormWidget::TEXT, language('USERNAME'), 'username', $user != NULL ? $user['username'] : '');
	$group->newWidget(BitOwl_FormWidget::PASSWORD, language('PASSWORD'), 'password');
	$group->newWidget(BitOwl_FormWidget::PASSWORD, language('CONFIRMPASSWORD'), 'password_confirm');
	$group->newWidget(BitOwl_FormWidget::LABEL, language('CHANGEPASSWORDNOTE_EDIT'));

	$group = $form->newGroup(language('PROFILE'));
	$group->newWidget(BitOwl_FormWidget::TEXT, language('REALNAME'), 'realname', $user != NULL ? $user['realname'] : '');
	$group->newWidget(BitOwl_FormWidget::TEXT, language('EMAIL'), 'email', $user != NULL ? $user['email'] : '');
	$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('PUBLICEMAIL'), 'publicemail', $user != NULL ? $user['publicemail'] : '');
	if($config->setting['system']['remoteavatar'])
		$group->newWidget(BitOwl_FormWidget::TEXT, language('AVATAR'), 'avatar', $user != NULL && $user['avatar'] !== '0' ? $user['avatar'] : '');
	if($config->setting['system']['avatarupload'])
	{
		$group->newWidget(BitOwl_FormWidget::UPLOAD, language('UPLOADAVATAR'), 'avatarupload');
		$group->newWidget(BitOwl_FormWidget::CHECKBOX, language('UPDATEAVATAR'), 'updateavatar', false);
	}
	if($config->setting['system']['enablebios'])
		$group->newWidget(BitOwl_FormWidget::MULTILINE, language('BIO'), 'bio', $user != NULL ? $user['bio'] : '');
}

function update_avatar($url, $file, $username, &$status) //handel avatar settings
{
	global $config;
	if($config->setting['system']['avatarupload'] && !empty($file['name'])) //upload avatar?
	{
		$uplobj = new BitOwl_Upload($file);
		//get the filename, we will need the extension from the origional file.
		$filename = 'avatar'.substr($uplobj->getName(), strpos($uplobj->getName(), '.'));
		$upload = true;
		switch($uplobj->getType()) //if we have a mime type, check it!
		{
			case 'image/png':
			case 'image/gif':
			case 'image/jpeg':
				$upload = true;
				break;
			default:
				$upload = false;
				break;
		}
		if($upload)
		{
			$uplobj->upload($filename);
			$status = $uplobj->getStatus() == BitOwl_Upload::UPLOAD_TOOBIG ? language('E_FILETOOLARGE') : '';
			return $uplobj->getDestination();
		}
		else
		{
			$status = language('E_INVALIDMIME');
		}
	}
	elseif($config->setting['system']['remoteavatar'] && !empty($_POST['avatar'])) //url
	{
		$status = NULL;
		return $_POST['avatar'];
	}
	return '';
}
?>