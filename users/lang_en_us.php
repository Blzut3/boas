<?php
//*****************************************************************************
//
//	Copyright (C) 2011  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

switch($msg)
{
	case 'C_DELETEUSER': return "Are you sure you want to delete the user $arg1?"; //$arg1 is the name of the user
	case 'CHANGEPASSWORDNOTE': return 'If you do not wish to change your password, leave the fields blank.';
	case 'CHANGEPASSWORDNOTE_EDIT': return 'If you do not wish to change this user\'s password, leave the fields blank.';
	case 'CONFIRMPASSWORD': return 'Confirm Password';
	case 'DELETEUSER': return 'Delete User';
	case 'F_EDITPROFILE': return 'Edit Profile';
	case 'F_MANAGEUSERS': return 'Manage Users';
	case 'NEWUSER': return 'New User';
	case 'NOAVATAR': return 'None';
	case 'PROFILE': return 'Profile';
	case 'PROFILEUPDATED': return 'Profile updated successfully!';
	case 'UPLOADAVATAR': return 'Upload Avatar';
	case 'USERCREATED': return 'User created successfully!';
	case 'USEREDITED': return 'User edited successfully!';
	default: return false;
}
?>
