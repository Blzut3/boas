<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if((!defined("bitowl") || $this_user['perm_users'] != true) || (isset($_GET['user']) && strtolower($_GET['user']) == strtolower($users[0]['username'])))
{
	die();
}

$userMessage = NULL;
$uploadMessage = NULL;
$showTable = true;

function generatePermissionsList($user)
{
	$permissions_list = array();
	foreach($user as $permission => $value) //send list of permissions to template
	{
		if(strpos($permission, 'perm_') === false)
			continue;
		if($value)
			$permissions_list[] = substr($permission, 5);
	}
	return $permissions_list;
}

if(isset($_GET['action']))
{
	if($_GET['action'] == 'new')
	{
		$new_user = NULL;
		if(isset($_POST['username']))
		{
			$userMessage = language('USERCREATED');
			if(empty($_POST['username']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['password_confirm']))
				$userMessage = language('E_REQUIRED');
			elseif($_POST['password'] != $_POST['password_confirm']) //passwords must be the same
				$userMessage = language('E_PASSWORDSDONTMATCH');
			else
			{
				$nametaken = user_exists($_POST['username'], $db);
				if(!$nametaken)
				{
					$new_user['username'] = $_POST['username'];
					$new_user['email'] = $_POST['email'];
					$new_user['password'] = md5($_POST['password']);
					$new_user['publicemail'] = isset($_POST['publicemail']) ? true : false;
					$new_user['permissions'] = array();
					foreach($_POST as $key => $value)
					{
						if(strpos($key, 'perm_') === false)
						{
							continue;
						}
						if($value)
						{
							if($this_user[$key]) //make sure the user has the set permission
							{
								$new_user['permissions'][] = $key;
							}
						}
					}
					$new_user['permissions'] = implode(',', $new_user['permissions']);
					$new_user['avatar'] = update_avatar($_POST['avatar'], 'avatarupload', $_POST['username'], $uploadMessage);
					if(empty($new_user['avatar']))
					{
						$new_user['avatar'] = '0';
					}
					$db->query('users', BITOWL_DB_INSERT, BITOWL_DB_ROW, $new_user);
				}
				else
				{
					$userMessage = language('E_USERNAMETAKEN');
				}
			}
		}

		if(isset($_POST['username']) ^ ($userMessage == NULL && $uploadMessage == NULL))
		{
			$form = new BitOwl_Form();
			$form->addMessage($userMessage);
			$form->addMessage($uploadMessage);

			makeUserForm($form, true, $new_user);
	
			$permissions_list = generatePermissionsList($this_user);
			$group = $form->newGroup(language('PERMISSIONS'));
			foreach($permissions_list as $permission)
			{
				$group->newWidget(BitOwl_FormWidget::CHECKBOX, $permission, 'perm_'.$permission, isset($_POST['perm_'.$permission]) ? $_POST['perm_'.$permission] : false);
			}
	
			$form->printForm();
			$showTable = false;
		}
	}
	elseif($_GET['action'] == 'edit')
	{
		if(isset($_POST['username']))
		{
			if(strtolower($_POST['delete']) == strtolower($this_user['username'])) //There is another function for doing this...
			{
				die();
			}
			if(empty($_POST['username']) || empty($_POST['email']))
				$userMessage = language('E_REQUIRED');
			elseif($_POST['password'] != $_POST['password_confirm']) //passwords must be the same
				$userMessage = language('E_PASSWORDSDONTMATCH');
			else
			{
				
				$nametaken = false; //user_exists() can not be applied here
				$editeduser = false;
				$users = $db->query('users', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('username', $_POST['editing']), BITOWL_DB_WHERE, array('username', $_POST['username'], 'multioperator' => BITOWL_DB_WHERE_OR));
				foreach($users as $user)
				{
					if($user['username'] == $_POST['editing'])
					{
						$editeduser = $user;
					}
					elseif($user['username'] == $_POST['username'])
					{
						$nametaken = true;
						$userMessage = language('E_USERNAMETAKEN');
					}
				}
				if(!$nametaken && $editeduser !== false)
				{
					$editeduser['username'] = $_POST['username'];
					$editeduser['email'] = $_POST['email'];
					if(!empty($_POST['password']))
					{
						$editeduser['password'] = md5($_POST['password']);
					}
					$editeduser['publicemail'] = $_POST['publicemail'] == 'true' ? true : false;
					if(strlen($editeduser['permissions']) == 0)
					{
						$editeduser['permissions'] = array();
					}
					else
					{
						$editeduser['permissions'] = explode(',', $editeduser['permissions']);
					}
					foreach($_POST as $key => $value)
					{
						if(strpos($key, 'perm_') === false)
						{
							continue;
						}
						if(!$value)
						{
							if($this_user[$key]) //make sure the user has the set permission
							{
								for($i = 0;$i < count($editeduser['permissions']);$i++)
								{
									if($editeduser['permissions'][$i] == $key)
									{
										unset($editeduser['permissions'][$i]);
									}
								}
							}
						}
						else
						{
							if($this_user[$key])
							{
								for($i = 0;$i < count($editeduser['permissions']);$i++)
								{
									if($editeduser['permissions'][$i] == $key)
									{
										continue(2);
									}
								}
								$editeduser['permissions'][] = $key;
							}
						}
					}
					$editeduser['permissions'] = implode(',', $editeduser['permissions']);
					if(isset($_POST['updateavatar']))
					{
						$editeduser['avatar'] = update_avatar($_POST['avatar'], 'avatarupload', $_POST['username'], $uploadMessage);
						if(empty($editeduser['avatar']))
						{
							$editeduser['avatar'] = '0';
						}
					}
					$editeduser['realname'] = $_POST['realname'];
					if($config->setting['system']['enablebios'])
						$editeduser['bio'] = entities_encode(stripslashes($_POST['bio']));
					$db->query('users', BITOWL_DB_UPDATE, BITOWL_DB_ROW, $editeduser);
				}
			}
		}

		if(isset($_POST['username']) ^ ($userMessage == NULL && $uploadMessage == NULL))
		{
			if(strtolower($this_user['username']) == strtolower($_GET['user'])) //this would be pointless
				die();
			$user = $db->query('users', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('username', $_GET['user']));
			if(!is_array($user))
				die();
			$user = $user[0];
			$permissions_list = generatePermissionsList($this_user);
			readpermissions($user, $permissions_list);
			foreach($user as $key => $value) //convert perm_* to just *
			{
				if(strpos($key, 'perm_') === 0)
				{
					$user[substr($key, 5)] = $value; //limitations of the template_engine requires this
				}
			}

			$form = new BitOwl_Form(BitOwl_Form::BTN_EDIT|BitOwl_Form::BTN_RESET);
			$form->addMessage($userMessage);
			$form->addMessage($uploadMessage);

			makeUserForm($form, true, $user);
	
			$permissions_list = generatePermissionsList($this_user);
			$group = $form->newGroup(language('PERMISSIONS'));
			foreach($permissions_list as $permission)
			{
				$group->newWidget(BitOwl_FormWidget::CHECKBOX, $permission, 'perm_'.$permission, $user[$permission]);
			}

			$group->newWidget(BitOwl_FormWidget::HIDDEN, NULL, 'editing', $user['username']);
			$form->printForm();
			$showTable = false;
		}
	}
	elseif($_GET['action'] == 'delete')
	{
		if(strtolower($this_user['username']) == strtolower($_GET['user'])) //you can't delete yourself
		{
			die();
		}
		$template_engine->variables['message_title'] = language('DELETEUSER');
		$template_engine->variables['message'] = language('C_DELETEUSER', $_GET['user']);
		$template_engine->variables['post_fields'] = array(
			array('name' => 'delete', 'value' => $_GET['user'])
		);
		$template_engine->variables['destination'] = '?cp=users&amp;func=users';
		$template_engine->template('templates/cp/message_confirm.html');
		$showTable = false;
	}
}

if($showTable)
{
	$pagination = new BitOwl_Pagination(STANDARD_PAGE_SIZE);
	$users = $db->query('users', BITOWL_DB_SELECT, BITOWL_DB_LIMIT, array($pagination->start(), STANDARD_PAGE_SIZE), BITOWL_DB_PAGINATED);
	$pagination->setTotal($db->getTotalResults());

	if(isset($_POST['delete']))
	{
		if(strtolower($_POST['delete']) == strtolower($this_user['username'])) //is the user trying to delete themself?
		{
			die();
		}
		for($i = 0;$i < count($users);++$i)
		{
			if($users[$i]['username'] == $_POST['delete'])
			{
				$db->query('users', BITOWL_DB_DELETE, BITOWL_DB_ROW, $users[$i]);
				unset($users[$i]);
				break;
			}
		}
	}

	$user_list = array();
	foreach($users as &$user)
	{
		$tmp = array(
			$user['username'],
			$user['email'],
			NULL
		);

		$isFounder = strpos($user['permissions'], 'perm_founder') !== false;

		if($user['id'] == $this_user['id'])
			$tmp[2] = '<a href="?cp=users&amp;func=edit">'.language('EDIT').'</a>';
		elseif(!$this_user['perm_founder'] && $isFounder)
			$tmp[2] = NULL;
		else
			$tmp[2] = '<a href="?cp=users&amp;func=users&amp;action=edit&amp;user='.$user['username'].'">'.language('EDIT').'</a> '.
				'<a href="?cp=users&amp;func=users&amp;action=delete&amp;user='.$user['username'].'">'.language('DELETE').'</a>';

		$user_list[] = $tmp;
	}

	$form = new BitOwl_Form(0);

	$group = $form->newGroup(language('F_MANAGEUSERS'));
	$group->newTable(array(language('USERNAME'), language('EMAIL'), language('ACTIONS')), $user_list);
	$group->newWidget(BitOwl_FormWidget::PAGINATION, NULL, NULL, $pagination);
	$group->newWidget(BitOwl_FormWidget::LABEL, '<a href="?cp=users&amp;func=users&amp;action=new">'.language('NEWUSER').'</a>');

	$form->printForm();
}
?>