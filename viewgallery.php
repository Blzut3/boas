<?php
//*****************************************************************************
//
//	Copyright (C) 2010  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(!defined('bitowl')) return; //do not continue if we did not init.
if(!defined('VIEWGALLERY'))
{
	define('VIEWGALLERY', true);

	function showAlbumImage($albumid, $imageid=0)
	{
		global $_bitowl;

		if($albumid <= 0)
			return;

		$thisAlbum = $_bitowl['database']->query('albums', BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('images'), BITOWL_DB_WHERE, array('id', (int) $albumid));
		if(count($thisAlbum) <= 0)
			return;
		$thisAlbum = $thisAlbum[0];

		$thisAlbum['images'] = unserialize($thisAlbum['images']);
		$requestedImage = $thisAlbum['images'][$imageid];
		if($requestedImage == NULL)
			return;
		$requestedImage['file'] = $_bitowl['config']->setting['system']['files']['url'].'/download.php?file='.$requestedImage['file'];

		$trace = $_bitowl['database']->query('albums', BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('id', 'name'), BITOWL_DB_NESTEDSET, BITOWL_DB_SELECTPARENTS, BITOWL_DB_WHERE, array('id', (int) $albumid, 'parent' => true));
		$trace[] = array('id' => 0, 'name' => $requestedImage['title'], 'last' => true);
		$_bitowl['templates']->variables['trace'] = $trace;
		$_bitowl['templates']->variables['image'] = $requestedImage;
		$_bitowl['templates']->variables['nav']['next'] = $imageid < count($thisAlbum['images'])-1 && is_array($thisAlbum['images'][$imageid+1]) ? $imageid+1 : -1;
		$_bitowl['templates']->variables['nav']['prev'] = $imageid >= 1 && is_array($thisAlbum['images'][$imageid-1]) ? $imageid-1 : -1;
		showTemplate('gallery_image.html');
	}

	function showAlbums($id=0)
	{
		global $_bitowl;

		$trace = $_bitowl['database']->query('albums', BITOWL_DB_SELECT, BITOWL_DB_COLUMNLIST, array('id', 'name'), BITOWL_DB_NESTEDSET, BITOWL_DB_SELECTPARENTS, BITOWL_DB_WHERE, array('id', (int) $id, 'parent' => true));
		$trace[count($trace)-1]['last'] = true;
		$albums = $_bitowl['database']->query('albums', BITOWL_DB_SELECT, BITOWL_DB_NESTEDSET, BITOWL_DB_SELIMEDCHILDREN, BITOWL_DB_WHERE, array('id', (int) $id, 'parent' => true));
		if(is_array($albums))
		{
			foreach($albums as &$subalbum)
			{
				$subalbum['thumbnail'] = $_bitowl['config']->setting['system']['files']['url'].'/download.php?file='.$subalbum['thumbnail'];
			}
		}

		if($id != 0) // We don't need to get the root
		{
			$pagination = new BitOwl_Pagination($_bitowl['config']->setting['gallery']['imagesperpage']);
			$thisAlbum = $_bitowl['database']->query('albums', BITOWL_DB_SELECT, BITOWL_DB_WHERE, array('id', (int) $id));
			if(count($thisAlbum) > 0)
			{
				$thisAlbum[0]['images'] = unserialize($thisAlbum[0]['images']);
				if(is_array($thisAlbum[0]['images']))
				{
					// Count before the slice.
					$_bitowl['templates']->variables['numimages'] = count($thisAlbum[0]['images']);
					$pagination->setTotal($_bitowl['templates']->variables['numimages']);
					if($_bitowl['config']->setting['gallery']['imagesperpage'] != 0)
						$thisAlbum[0]['images'] = array_slice($thisAlbum[0]['images'], $pagination->start(), $_bitowl['config']->setting['gallery']['imagesperpage']);

					// We need to generate more information such as the image's id number
					$id = $pagination->start();
					foreach($thisAlbum[0]['images'] as &$image)
					{
						$image['id'] = $id++;
						$image['thumbnail'] = $_bitowl['config']->setting['system']['files']['url'].'/download.php?file='.$image['thumbnail'];
					}
				}
				else
					$_bitowl['templates']->variables['numimages'] = 0;
				$_bitowl['templates']->variables['album'] = $thisAlbum[0];
			}
			$_bitowl['templates']->variables['pagination'] = $pagination;
		}

		$_bitowl['templates']->variables['trace'] = $trace;
		$_bitowl['templates']->variables['subalbums'] = $albums;
		showTemplate('gallery_layout.html');
	}
}
?>
