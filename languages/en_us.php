<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

switch($msg)
{
	//General
	case 'ACTIONS': return 'Actions';
	case 'ADD': return 'Add';
	case 'ADMINLOGIN': return 'Administrator Login';
	case 'ALIGN': return 'Align';
	case 'ALIGNMENT': return 'Alignment';
	case 'ALIGNMENTTOPLEFT': return 'Top Left';
	case 'ALIGNMENTTOP': return 'Top';
	case 'ALIGNMENTTOPRIGHT': return 'Top Right';
	case 'ALIGNMENTLEFT': return 'Left';
	case 'ALIGNMENTCENTER': return 'Center';
	case 'ALIGNMENTRIGHT': return 'Right';
	case 'ALIGNMENTBOTTOMLEFT': return 'Bottom Left';
	case 'ALIGNMENTBOTTOM': return 'Bottom';
	case 'ALIGNMENTBOTTOMRIGHT': return 'Bottom Right';
	case 'ALLOWAVATARUPLOAD': return 'Allow users to upload their avatar.';
	case 'ALLOWCOMMENTS': return 'Allow comments';
	case 'ALLOWNAMECHANGE': return 'Allow username changes.';
	case 'ALLOWREMOTEAVATAR': return 'Allow remote avatars.';
	case 'AUTHOR': return 'Author';
	case 'AVATAR': return 'Avatar';
	case 'BIO': return 'Bio';
	case 'BOAS': return 'BitOwl Application Suite';
	case 'CACHE': return 'Cache';
	case 'CAPTCHA': return 'Captcha';
	case 'COMMENT': return 'Comment';
	case 'COMMENTS': return 'Comments';
	case 'CONFIGSUMMERY': return 'Configuration Summery';
	case 'CONFIGURE': return 'Configure';
	case 'CONTINUE': return 'Continue';
	//CP Names
	case 'CP_USERS': return 'Users';
	case 'CP_SYSTEM': return 'System';
	case 'CREATE': return 'Create';
	case 'DATABASE': return 'Database';
	case 'DATE': return 'Date';
	case 'DAY': return 'Day';
	case 'DEFAULTTHUMBNAILSIZE': return 'Default Thumbnail Size';
	case 'DELETE': return 'Delete';
	case 'DESCRIPTION': return 'Description';
	case 'DISPLAY': return 'Display';
	//Errors
	case 'E_CONNECT': return 'Unable to connect to MySQL database.';
	case 'E_COULDNOTDELETEFILE': return 'File could not be deleted.';
	case 'E_FILETOOLARGE': return 'Uploaded file size too large.';
	case 'E_INJECTION': return 'Possible injection attack detected.';
	case 'E_INVALIDDATE': return 'The date specified was invalid.';
	case 'E_INVALIDCAPTCHA': return 'Incorrect answer for captcha.';
	case 'E_INVALIDEMAIL': return 'A valid E-Mail address is required.';
	case 'E_INVALIDMIME': return 'Uploaded file\'s mime type invailid.';
	case 'E_MISSINGQUERYSECTION': return "A required section of a query is missing ($arg1)."; // $arg1 is extra information.
	case 'E_OPEN': return 'Could not open file; check permissions.';
	case 'E_PASSWORDSDONTMATCH': return 'Passwords do not match.';
	case 'E_QUERY': return 'Database query failed.';
	case 'E_REQUIRED': return 'Not all required fields were filled!';
	case 'E_SELECTDB': return 'Unable to switch to database.';
	case 'E_UPDATE': return 'Unable to update database record.';
	case 'E_UPLOAD': return 'File could not be uploaded.';
	case 'E_USERNAMEINUSE': return 'Someone has registered the name you selected, please choose another one.';
	case 'E_USERNAMETAKEN': return 'Username already in use.';
	case 'EDIT': return 'Edit';
	case 'EMAIL': return 'E-Mail';
	case 'ENABLEBIOS': return 'Enable User Bios';
	case 'ENABLECAPTCHA': return 'Enable Captcha';
	case 'ENABLEWATERMARK': return 'Enable Watermark';
	//Function names
	case 'F_MAIN': return 'Main';
	case 'F_CONFIGURE': return 'Configure';
	case 'FILENAME': return 'Filename';
	case 'FILERELATIVENOTE': return "Filename is relative to \"$arg1\"."; // $arg1 is the path to the script.
	case 'FLATFILE': return 'Flat File';
	case 'HOSTNAME': return 'Host Name';
	case 'ID': return 'ID';
	case 'INSERT': return 'Insert';
	case 'INSTALLEDVERSION': return 'Installed Version';
	case 'IP': return 'IP';
	case 'LANGUAGE': return 'Language';
	case 'LOGIN': return 'Login';
	case 'LOGINAUTO': return 'Log me on automatically.';
	case 'LOGINFAILED': return 'Invailid username and/or password.';
	case 'LOGOUT': return 'Logout';
	//General Messages
	case 'M_SETTINGSAPPLIED': return 'The new settings have been applied.';
	case 'MANAGECOMMENTS': return 'Manage Comments';
	case 'MAXWATERMARKSIZE': return 'Max Watermark Size (%)';
	case 'MEMORY': return 'Memory';
	case 'MESSAGE': return 'Message';
	case 'MONTH': return 'Month';
	case 'MYSQL': return 'MySQL';
	case 'NAME': return 'Name';
	case 'NEW': return 'New';
	case 'NO': return 'No';
	case 'PARENT': return 'Parent';
	case 'PASSWORD': return 'Password';
	case 'PERMISSIONS': return 'Permissions';
	case 'PREVIEW': return 'Preview';
	case 'PORT': return 'Port';
	case 'PUBLICEMAIL': return 'Display E-Mail to Public';
	case 'QUERIES': return 'Queries';
	case 'REALNAME': return 'Real Name';
	case 'REMOTE': return 'Remote';
	case 'REPLY': return 'Reply';
	case 'REPLYTOPOST': return 'Reply to this post';
	case 'REQUIREDNOTE': return '* Represents rquired fields.';
	case 'RESET': return 'Reset';
	case 'SCRIPTURL': return 'Script URL';
	case 'SIZE': return 'Size';
	case 'SUBJECT': return 'Subject';
	case 'SUBMIT': return 'Submit';
	//Template Engine Errors
	case 'T_ARG3NOTNUMBERIC': return "Argument 3 ($arg1) not numberic"; //$arg1 is the value of argument 3
	case 'T_ILLEGALBOOL': return 'Illegal part of boolean expression';
	case 'T_UNCLOSED': return 'Unclosed code block!';
	case 'T_UNKOWN': return 'Unkown statement';
	case 'TIME': return 'Time';
	case 'TITLE': return 'Title';
	case 'TO': return 'To';
	case 'UNKNOWNDB': return 'Unkown Database Type';
	case 'UPDATEAVATAR': return 'Update Avatar';
	case 'UPLOAD': return 'Upload';
	case 'UPLOADS': return 'Uploads';
	case 'USERNAME': return 'Username';
	case 'USERS': return 'Users';
	case 'USERSCONFIG': return 'Users Configureation';
	case 'VERSIONINFORMATION': return 'Version Information';
	case 'WATERMARK': return 'Watermark';
	case 'WATERMARKPOSITION': return 'Watermark Position';
	case 'YEAR': return 'Year';
	case 'YES': return 'Yes';

	// Installer strings
	case 'ADMINISTRATOR': return 'Administrator';
	case 'DATABASETYPE': return 'Database Type';
	case 'I_DATABASESETUP': return 'Setting up the database...';
	case 'I_ADDADMIN': return 'Adding administrative user...';
	case 'I_SAVECONFIG': return 'Saving configuration file...';
	case 'I_DATABASEINSTALLNOTICE': return 'You must only fill out the form for the database type selected. Any other input will be ignored.';
	case 'I_INSTALLSUCCESSFUL': return 'BitOwl Application Suite has been installed.  You may now log in using the user and password you provided during the installation.';
	case 'I_INVALIDADMINFORM': return 'You have entered invalid information into one or more field of the admin user form.';
	case 'I_INVALIDDATABASE': return 'You have entered invalid database information.';
	case 'I_NOINSTALL': return 'Could not find valid BitOwl Application Suite 1.0 install.';
	case 'I_UPGRADEDB': return "Converting database $arg1..."; //$arg1 is the database file.
	case 'I_UPGRADE': return 'Please point the relative path to your existing BitOwl Application Suite 1.0 installation. BitOwl Application Suite 2.0 will convert the installation to the best of it\'s ability filling in resonable defaults where necessary.';
	case 'I_SUCCESS': return 'Installation successful! You may now <a href="./">login</a>.';
	case 'I_INITSETUP': return 'Inital Setup';
	case 'I_DATABASE': return 'Database';
	case 'INSTALL': return 'Install';
	case 'LICENSE': return 'License Agreement';
	case 'PROGRESS': return 'Progress';
	case 'UPGRADE': return 'Upgrade';
	default: return false;
}
?>
