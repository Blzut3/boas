<?php
//*****************************************************************************
//
//	Copyright (C) 2009  Braden "Blzut3" Obrzut <devs@bitowl.com>
//
//	This program is free software; you can redistribute it and/or
//	modify it under the terms of the GNU General Public License
//	as published by the Free Software Foundation; either version 2
//	of the License, or (at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
//	02110-1301, USA.
//
//*****************************************************************************

if(defined('bitowl')) return; //already included apparently...
// Include this file at the top of your website (before anything else).
define('bitowl', true); //used to identify if this was included or not.
$script_path = substr(str_replace('init.php', '', __FILE__),0,-1);
include_once "$script_path/includes/config.php";
$_bitowl['config'] = new BitOwl_Config("$script_path/config.php");
$script_url = $_bitowl['config']->setting['system']['scripturl'];
include_once "$script_path/includes/global_functions.php";
include_once "$script_path/includes/template_engine.php";
include_once "$script_path/includes/database.php";
include_once "$script_path/includes/language.php";
include_once "$script_path/includes/comments.php";
include_once "$script_path/includes/bbcode.php";
include_once "$script_path/includes/captcha.php";
$_bitowl['templates'] = BitOwl_TemplateEngine::getStandardInstance();
if($_bitowl['config']->setting['system']['dbuser'] === false) //we need to get relative to script directory if flatfile
{
	$_bitowl['config']->setting['system']['dbhost'] = $script_path.'/'.$_bitowl['config']->setting['system']['dbhost'];
}
$_bitowl['database'] = BitOwl_Database::createDatabaseObject($_bitowl['config']->setting['system']['dbhost'], $_bitowl['config']->setting['system']['dbuser'], $_bitowl['config']->setting['system']['dbpass'], $_bitowl['config']->setting['system']['dbbase']);

// Some scripts utilize the users database, so query it anyways.
$_bitowl['users'] = $_bitowl['database']->query('users', BITOWL_DB_SELECT);

if($_bitowl['config']->setting['system']['performance']['enablesitelogin'])
{
	//check login
	$loggedinuser = false;
	$_bitowl['templates']->variables['loggingin'] = 'false';
	if(isset($_GET['logout']))
	{
		setcookie('login_username', '', 1);
		setcookie('login_password', '', 1);
		define('USERNAME', '');
		unset($_COOKIE['login_username'], $_COOKIE['login_password']);
		$_bitowl['templates']->variables['loggedinuser'] = 0;
	}
	else
	{
		if(isset($_POST['login_username']) && isset($_POST['login_password']))
		{
			$_COOKIE['login_username'] = $_POST['login_username'];
			$_COOKIE['login_password'] = md5($_POST['login_password']);
			$_bitowl['templates']->variables['loggingin'] = 'true';
		}
		if(isset($_COOKIE['login_username']) && isset($_COOKIE['login_password']))
		{
			foreach($_bitowl['users'] as $_bitowl['user'])
			{
				if($_bitowl['user']['username'] == $_COOKIE['login_username'] && $_bitowl['user']['password'] == $_COOKIE['login_password'])
				{
					$loggedinuser = $_bitowl['user'];
					if($_bitowl['templates']->variables['loggingin'] == 'true')
					{
						if($_POST['login_auto'] == 'on') //31 days
						{
							setcookie('login_username', $_COOKIE['login_username'], time()+2678400);
							setcookie('login_password', $_COOKIE['login_password'], time()+2678400);
						}
						else
						{
							setcookie('login_username', $_COOKIE['login_username']);
							setcookie('login_password', $_COOKIE['login_password']);
						}
					}
				}
			}
		}
		if($loggedinuser !== false)
		{
			define('USERNAME', $loggedinuser['username']);
		}
		else
		{
			define('USERNAME', '');
			if($_bitowl['templates']->variables['loggingin'] == 'true')
			{
				$_bitowl['templates']->variables['loggingin'] = 'failed';
			}
		}
		$_bitowl['templates']->variables['loggedinuser'] = $loggedinuser == false ? 0 : $loggedinuser;
	}
	define('LOGIN_STATUS', $_bitowl['templates']->variables['logginin']); //allow easier access
	//register?
	if($_bitowl['config']->setting['system']['guestregistration'])
	{
		$_bitowl['templates']->variables['registration'] = 'false';
		if(isset($_POST['register_username'], $_POST['register_password'], $_POST['register_password_confirm'], $_POST['register_email']))
		{
			if(empty($_POST['register_username']) || empty($_POST['register_password']) || empty($_POST['register_password_confirm']) || empty($_POST['register_email']))
			{
				$_bitowl['templates']->variables['registration'] = 'empty';
			}
			elseif($_POST['register_password'] != $_POST['register_password_confirm']) //passwords must match
			{
				$_bitowl['templates']->variables['registration'] = 'passwords_dontmatch';
			}
			else
			{
				$_bitowl['inuse'] = false;
				foreach($_bitowl['users'] as $_bitowl['user']) //username in use?
				{
					if($_bitowl['user']['username'] == $_POST['register_username'])
					{
						$_bitowl['inuse'] = true;
					}
				}
				if(!$_bitowl['inuse'])
				{
					$_bitowl['user'] = array(
						'username' => $_POST['register_username'],
						'password' => md5($_POST['register_password']),
						'email' => $_POST['register_email'],
						'avatar' => '0',
						'publicemail' => false,
						'permissions' => ''
					);
					$_bitowl['database']->query('users', BITOWL_DB_INSERT, BITOWL_DB_ROW, $_bitowl['user']);
					$_bitowl['templates']->variables['registration'] = 'true';
				}
				else
				{
					$_bitowl['templates']->variables['registration'] = 'username_inuse';
				}
			}
		}
		//REGISTRATION_STATUS can be true, false, empty, username_inuse, or passwords_dontmatch
		define('REGISTRATION_STATUS', $_bitowl['templates']->variables['registration']);
	}
}
else
	$_bitowl['templates']->variables['loggedinuser'] = 0;
$_bitowl['templates']->variables['query'] = str_replace('&', '&amp;', $_SERVER['QUERY_STRING']);
if(strlen($_bitowl['templates']->variables['query']) != 0)
{
	$_bitowl['templates']->variables['query'] .= '&amp;';
}
if(!file_exists($script_path.'/initcache.php')) //retrieve a list of view(something).php files
{
	initCache($script_path);
}
require_once $script_path.'/initcache.php';
?>